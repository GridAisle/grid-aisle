Template.TokenAddPaymentMethod.onRendered(function() {
    $('input.cc-num').payment('formatCardNumber');
    $('input.cc-exp').payment('formatCardExpiry');
    $('input.cc-cvc').payment('formatCardCVC');
    $('[data-numeric]').payment('restrictNumeric');
});
Template.TokenAddPaymentMethod.events({
    'submit form': function(event, template) {
        event.preventDefault();
        document.getElementById("AddPaymentSubmitButton").disabled = true;
        ccNum = $('.cc-num').val();
        cvc = $('.cc-cvc').val();
        expMo = $('.cc-exp').val().substring(0, 2);
        expYr = $('.cc-exp').val().split("/ ").pop();
        name = $('.cc-name').val();
        zipcode = $('.cc-zip').val();
        Stripe.card.createToken({
            name: name,
            number: ccNum,
            cvc: cvc,
            exp_month: expMo,
            exp_year: expYr,
            address_zip: zipcode
        }, function(status, response) {
            if (response.error) {
                template.$('#error').html(response.error.message);
                document.getElementById("AddPaymentSubmitButton").disabled = false;
            } else {
                Meteor.call('AddPaymentMethod', response, function(error, result) {
                    if (result) {
                        var modal = $('[data-remodal-id=AddNewPaymentMethodModal]').remodal();
                        modal.close();
                    } else {
                        template.$('#error').html(error.reason);
                        document.getElementById("AddPaymentSubmitButton").disabled = false;
                    }
                });
            }
        });
    }
});
