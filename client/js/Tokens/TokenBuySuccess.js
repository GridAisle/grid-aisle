Template.TokenBuySuccess.onCreated(function() {})
Template.TokenBuySuccess.events({
    'click .BuyMoreTokensButton': function() {
        Session.set("TokensSectionExpand", false);
        Session.set("TokenScreenInformation", "TokenBuy");
    }
});
Template.TokenBuySuccess.helpers({
    TokenBuySuccessAmount: function() {
        return Session.get("TokenBuyAmountSuccess");
    },
})
