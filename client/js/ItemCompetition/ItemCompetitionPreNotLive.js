Template.ItemCompetitionPreNotLive.onCreated(function() {

});

Template.ItemCompetitionPreNotLive.helpers({
    ItemCompetition: function() {
        return Template.instance().data;
    },
    ItemCompetitionCountdown: function(Competition) {
        var diff = moment(Competition.hash.Competition.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();

        Meteor.setInterval(function() {
            var diff = moment(Competition.hash.Competition.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    IsUserEntered: function() {
        if (Meteor.user() && Meteor.user().CompetitionsEntered) {
            var self = this;
            var isEntered = _.some(Meteor.user().CompetitionsEntered, function(Competition) {
                return Competition == self._id
            });
            return isEntered;
        }

    }
});
Template.ItemCompetitionPreNotLive.events({
    'click .EnterButton': function(event, template) {
        console.log(this);
        Session.set("CompetitionEntrantInformation", this);
        $('[data-remodal-id=ItemCompetitionEntrantModal]').remodal({
            "closeOnConfirm": false,
            modifier: 'EnterCompetitionModal'
        }).open();
    }
})
