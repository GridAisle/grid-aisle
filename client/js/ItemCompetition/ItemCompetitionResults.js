Template.ItemCompetitionResults.onCreated(function() {
    this.subscribe("ItemCompetitionResult", FlowRouter.getParam("ItemCompetitionId"), this.data.Item._id);
    this.IsLoading = new ReactiveVar(true);
    this.Competition = new ReactiveVar(ItemCompetitions.find({
        _id: FlowRouter.getParam("ItemCompetitionId")
    }).observeChanges({
        removed: function(x) {
            if (FlowRouter.current().params.ItemCompetitionId == FlowRouter.getParam("ItemCompetitionId")) {
                FlowRouter.go("/");
                stop();
            }
        }
    }));
    var self = this;
    Tracker.autorun(function() {
        self.Competition.get();
    });

});
Template.ItemCompetitionResults.onDestroyed(function() {
    this.Competition.get().stop();
})
Template.ItemCompetitionResults.helpers({
    ItemCompetitionResult: function() {
        if (Meteor.user().Cart) {
            var winningObject = {}
            winningObject["WinningInfo"] = [];
            winningObject["DidWin"] = false;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].hasOwnProperty("WinningCompetitionId")) {
                    if (Meteor.user().Cart[i].WinningCompetitionId == FlowRouter.getParam("ItemCompetitionId")) {
                        winningObject["WinningInfo"].push({
                            "Discount": Meteor.user().Cart[i].Discount,
                            "Name": this.Item.Name
                        });
                        winningObject["DidWin"] = true;

                    }
                }
            }
            return winningObject;
        }
    },
    ItemCompetition: function() {
        return this;
    },
    SimilarItemCompetitions: function() {
        var competitionId = this._id
        return ItemCompetitions.find({
            Cancelled: {
                $exists: false
            },
            _id: {
                $ne: {
                    competitionId
                }
            },
            "HasBeenWon": false
        }, {
            sort: {
                "StartTimeOfCompetition": 1
            }
        });
    },
    IsOnUserWatchList: function(Competition_Id) {
        if (Meteor.users.findOne({
                _id: Meteor.userId(),
                'Watchlist': {
                    $in: [Competition_Id.hash.Competition_Id]
                }
            })) {
            return true;
        }
        return false;

    },
    SimilarItemCompetitiveLiveCountdown: function() {
        var diff = moment(this.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        var self = this;
        Meteor.setInterval(function() {
            var diff = moment(self.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    SimilarItemCompetitiveNotLiveCountdown: function() {
        var diff = moment(this.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();

        Meteor.setInterval(function() {
            var diff = moment(self.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    UserSlots: function() {
        var userSlots = Meteor.users.findOne({
            _id: Meteor.userId()
        }, {
            fields: {
                "CompetitionSlots": 1
            }
        });
        return _.filter(userSlots.CompetitionSlots, function(Slots) {
            return (Slots.CompetitionId == FlowRouter.getParam("ItemCompetitionId"));
        });
    },
    Progress: function() {
        return 100 - ((this.Progress % 1) * 100);
    },
    ProgressText: function() {
        return Math.floor(this.Progress * 100) + "%";
    },

});
Template.ItemCompetitionResults.events({
    'click .Watchlist-Star-Add': function(event, template) {
        Meteor.call("AddToWatchlist", this._id);
    },
    'click .Watchlist-Star-Remove': function(event, template) {
        Meteor.call("RemoveFromWatchlist", this._id);
    },
    'click .GoToSimilarItemCompetitionButton': function(event, template) {
        FlowRouter.go("/ItemCompetition/" + this._id);

    }

})
