Template.ItemCompetitionsPage.onCreated(function() {
    this.subscribe("ItemCompetitionsPage");
});
Template.ItemCompetitionsPage.onRendered(function() {

})
Template.ItemCompetitionsPage.onDestroyed(function() {})
Template.ItemCompetitionsPage.helpers({
    IsDisabled: function(Button) {
        if (Button.hash.Button == FlowRouter.getRouteName()) {
            return true;
        }
        return false;
    },
    Notification: function() {
        return Notification.find({});
    }
});
Template.ItemCompetitionsPage.events({
    'click .GoToWatchlistButton': function(event, template) {
        FlowRouter.go("/Watchlist")
    },
    'click .GoToCompetitionsButton': function(event, template) {
        FlowRouter.go("/");
    },
    'click .GoToCompetitionsEnteredButton': function(event, template) {
        FlowRouter.go("/CompetitionsEntered")
    },
    'click .GoToLoginButton': function(event, template) {
        FlowRouter.go("/Login");
    },
    'click .GoToRegisterButton': function(event, template) {
        FlowRouter.go("/Register");
    },
    'click .Notification': function(event, template) {
        FlowRouter.go(this.Url);
    }

})
