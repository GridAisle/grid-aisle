Template.ItemCompetitionGridOuter.onCreated(function() {
    this.MiddleGridButtons = new ReactiveVar(5);
});
Template.ItemCompetitionGridOuter.helpers({
    ItemCompetitionSlots: function() {
        return Template.instance().Grid.get();
    }
});
Template.ItemCompetitionGridOuter.events({
    'click .OuterGridButton': function(event, template) {
        var MiddleGridArray = [];
        var GridRangeCount = 1000;
        var GridNumberButtonCount = 999;
        for (var i = 0; i < 10; i++) {
            MiddleGridArray[i] = {
                GridRange: event.currentTarget.getAttribute('data-gridnumbers') - GridRangeCount,
                GridNumberButton: event.currentTarget.getAttribute('data-gridnumbers') - GridNumberButtonCount + " - " + (event.currentTarget.getAttribute('data-gridnumbers') - GridRangeCount + 100)
            };
            GridRangeCount -= 100;
            GridNumberButtonCount -= 100;
        }
        Session.set("GridSectionMiddleData", MiddleGridArray);
        Session.set("GridSection", "ItemCompetitionGridMiddle");
    }
});
