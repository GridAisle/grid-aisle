Template.ItemCompetition.onCreated(function() {

    this.ItemCompetitionStatus = new ReactiveVar();


    this.subscribe("ItemCompetition", FlowRouter.getParam("ItemCompetitionId"), function() {
        // var UserCompetitionSlotsArray = [];
        // for (var i = 0; i < Meteor.user().CompetitionSlots.length; i++) {
        //     if (Meteor.user().CompetitionSlots[i].CompetitionId == FlowRouter.getParam("ItemCompetitionId")) {
        //         UserCompetitionSlotsArray.push(Meteor.user().CompetitionSlots[i].Slot);
        //     }
        // }
        // self.UserItemCompetitionSlots.set(UserCompetitionSlotsArray);
    });
});
Template.ItemCompetition.onRendered(function() {})
Template.ItemCompetition.helpers({
    ItemCompetition: function() {
        return ItemCompetitions.findOne({
            _id: FlowRouter.getParam("ItemCompetitionId")
        });
    },
    ItemCompetitionStatus: function() {
        if (this.HasBeenWon) {
            if (this.WinnersCalculated) {
                Template.instance().ItemCompetitionStatus.set("ItemCompetitionResults");
            } else {
                Template.instance().ItemCompetitionStatus.set("ItemCompetitionCalculatingResults");
            }
        } else {
            if (this.IsLive) {
                Template.instance().ItemCompetitionStatus.set("ItemCompetitionGrid");
            } else {
                Template.instance().ItemCompetitionStatus.set("ItemCompetitionPreNotLive");
            }
        }
        return Template.instance().ItemCompetitionStatus.get();
    },
    ItemCompetitionStatusData: function() {
        return FlowRouter.getParam("ItemCompetitionId");
    },


});
Template.ItemCompetition.events({
    'click .DecreaseQuantity' (event, template) {
        if (template.Quantity.get() > 1) {
            template.Quantity.set(template.Quantity.get() - 1);
        }
    },
    'click .IncreaseQuantity' (event, template) {
        if (template.Quantity.get() < 10) {
            template.Quantity.set(template.Quantity.get() + 1);
        }

    },
    'click .AddToCartButton' (event, template) {
        Meteor.call('AddToCart', event.currentTarget.getAttribute('data-itemcompetitionitemid'), template.Quantity.get());
        FlowRouter.go('/AddToCartResult');
    },
    'click .GoToSimilarItemCompetitionButton' (event, template) {
        FlowRouter.go("/ItemCompetition/" + event.currentTarget.id);
    }
});
