Template.ItemCompetitionGridInner.onCreated(function() {
    this.Competition = new ReactiveVar();
    var self = this;
    self.autorun(function() {
        self.subscribe("ItemCompetitionInnerSlots", FlowRouter.getParam("ItemCompetitionId"), Session.get("GridSectionInnerData") + 1, Session.get("GridSectionInnerData") + 100);
        self.Competition.set(ItemCompetitions.findOne({
            _id: FlowRouter.getParam("ItemCompetitionId")
        }));
    });
    this.IsUserEntered = new ReactiveVar();
    var self = this;
    Tracker.autorun(function(c) {
        if (Meteor.user() && Meteor.user().CompetitionsEntered) {
            self.IsUserEntered.set(_.some(Meteor.user().CompetitionsEntered, function(Competition) {
                return Competition == FlowRouter.getParam("ItemCompetitionId")
            }));
            c.stop();
        }
    });
});
Template.ItemCompetitionGridInner.onRendered(function() {});
Template.ItemCompetitionGridInner.helpers({
    InnerGridButtons: function() {
        return Template.instance().Competition.get()[((Session.get("GridSectionInnerData") + 1) + "-" + (Session.get("GridSectionInnerData") + 100))];
    },
    SlotUser: function(Slot) {
        if (Slot.hash.Slot.User != null && Slot.hash.Slot.User != Meteor.userId()) {
            if (Slot.hash.Slot.Slot == Template.instance().Competition.get().SlotCount) {
                return "GoldenTaken";
            }
            return "Taken";
        } else if (Slot.hash.Slot.User != null && Slot.hash.Slot.User == Meteor.userId()) {
            if (Slot.hash.Slot.Slot == Template.instance().Competition.get().SlotCount) {
                return "GoldenMine";
            }
            return "Mine";
        }
        if (Slot.hash.Slot.Slot == Template.instance().Competition.get().SlotCount) {
            return "GoldenNeutral";
        }
        return "";
    },
    DisableSlot: function(Slot) {
        if (Meteor.userId() == undefined || Slot.hash.Slot.User != null || !Template.instance().IsUserEntered.get()) {
            return true;
        }
        return false;
    },

});
Template.ItemCompetitionGridInner.events({
    'click .InnerGridButton': function(event, template) {
        if (ItemCompetitions.findOne({
                _id: FlowRouter.getParam("ItemCompetitionId")
            }).SlotPrice > Meteor.user().profile.Tokens) {
            Session.set("ItemCompetitionGridInnerTokenBuyModalTemplate", "ItemCompetitionGridInnerTokenBuyModalPurchaseSection");
            $('[data-remodal-id=SlotBuyTokensModal]').remodal({
                "closeOnConfirm": false
            }).open();
        } else if (Meteor.user().CompetitionSettings.FastSlots) {
            var x = Meteor.call("PlaceSlotForItemCompetition", parseInt(event.currentTarget.getAttribute('data-innergridnumber')), Session.get("GridSectionInnerData") + 100, FlowRouter.getParam("ItemCompetitionId"), function(error, result) {
                if (result) {
                    UserSlots.insert({
                        Slot: result.Slot,
                        Date: new Date(),
                        Price: result.Price
                    });
                }

            });
        } else {
            Session.set("CompetitionSlot", this.Slot)
            $('[data-remodal-id=SlotBuyModal]').remodal({
                "closeOnConfirm": false
            }).open();
        }

    },

});

Template.ItemCompetitionGridInnerSlotBuyModal.helpers({
    ItemCompetitionModalSlot: function() {
        if (Session.get("CompetitionSlot")) {
            return ItemCompetitions.findOne({
                _id: FlowRouter.getParam("ItemCompetitionId")
            })[(Session.get("GridSectionInnerData") + 1) + "-" + (Session.get("GridSectionInnerData") + 100)][Number(Session.get("CompetitionSlot")) - Session.get("GridSectionInnerData") - 1];
        }
    },
    CompetitionPrice: function() {
        return ItemCompetitions.findOne({
            _id: FlowRouter.getParam("ItemCompetitionId")
        }).SlotPrice;
    }
});
Template.ItemCompetitionGridInnerSlotBuyModal.events({
    'click .BuySlotButton': function(event, template) {
        Meteor.call("PlaceSlotForItemCompetition", Session.get("CompetitionSlot"), Session.get("GridSectionInnerData") + 100, FlowRouter.getParam("ItemCompetitionId"), function(error, result) {});
    },
    'click .FastSlots': function(event, template) {
        Meteor.call("FastSlots", function(error, result) {});
    },
    'click .FastSlotsEnabled': function(event, template) {
        Meteor.call("FastSlots", function(error, result) {});
    }
})
