Template.ItemCompetitionGrid.onCreated(function() {
    this.MiddleGridButtons = new ReactiveVar();
    this.InnerGridButtons = new ReactiveVar();
    this.MiddleLeftArrow = new ReactiveVar();
    this.MiddleRightArrow = new ReactiveVar();
    this.InnerLeftArrow = new ReactiveVar();
    this.InnerRightArrow = new ReactiveVar();
    this.Progress = new ReactiveVar();
    this.IsEntered = new ReactiveVar();
    Session.set("GridSection", "ItemCompetitionGridOuter");

    var self = this;
    UserSlots = new Mongo.Collection(null);
    SlotsPlaced = new Mongo.Collection(null);
    var self = this;
    Tracker.autorun(function(c) {
        if (Meteor.user() && Meteor.user().CompetitionsEntered) {
            self.IsEntered.set(_.some(Meteor.user().CompetitionsEntered, function(Competition) {
                return Competition == FlowRouter.getParam("ItemCompetitionId")
            }));
            c.stop();
        }
    });

    Meteor.call("UserCompetitionSlots", FlowRouter.getParam("ItemCompetitionId"), function(error, result) {
        if (result) {
            for (var i = 0; i < result.length; i++) {
                UserSlots.insert({
                    Slot: result[i].Slot,
                    Price: result[i].Price
                })
            }
        }

    });


    serverMessages.listen('ItemCompetition' + FlowRouter.getParam("ItemCompetitionId"), function(Slot, SlotPrice) {
        SlotsPlaced.insert({
            Slot: Slot,
            Price: SlotPrice
        })


    });

});
Template.ItemCompetitionGrid.onRendered(function() {

});
Template.ItemCompetitionGrid.helpers({
    ItemCompetitionGrid: function() {
        return Session.get("GridSection");
    },
    ItemCompetition: function() {
        return ItemCompetitions.findOne({
            _id: FlowRouter.getParam("ItemCompetitionId")
        });
    },
    ZoomOut: function() {
        if (Session.get("GridSection") === "ItemCompetitionGridInner" || Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            return true;
        }
        return false;
    },
    ItemNameSize: function() {
        var fontSize = 375 / this.Item.Name.length;
        if (fontSize > 35) {
            return "";
        }
        return fontSize + "px";
    },
    Arrows: function() {
        if (Session.get("GridSection") === "ItemCompetitionGridInner" || Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            return true;
        }
        return false;
    },
    RightArrow: function() {
        if (Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            if (Session.get("GridSectionMiddleData")[Session.get("GridSectionMiddleData").length - 1].GridRange >= 11900) {
                return false;
            }
            return true;
        } else if (Session.get("GridSection") === "ItemCompetitionGridInner") {
            if (Session.get("GridSectionInnerData") >= 12000) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    },
    LeftArrow: function() {
        if (Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            if (Session.get("GridSectionMiddleData")[0].GridRange <= 100) {
                return false;
            }
            return true;
        } else if (Session.get("GridSection") === "ItemCompetitionGridInner") {
            if (Session.get("GridSectionInnerData") <= 0) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    },
    ItemCompetitionCountdown: function() {
        var diff = moment(this.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        var self = this;
        Meteor.setInterval(function() {
            var diff = moment(self.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    Progress: function(ProgressInfo) {
        //.animate slows the DOM
        return 100 - ((ProgressInfo.hash.ProgressInfo % 1) * 100);
    },
    ProgressValue: function() {
        return Math.round(this.Progress * this.Item.SellingPrice);
    },
    ProgressText: function(ProgressInfo) {
        return Math.round(ProgressInfo.hash.ProgressInfo * 100) + "%";
    },
    ItemCompetitionUserSlots: function() {
        return UserSlots.find({}, {
            sort: {
                _id: -1
            }
        });
    },
    ItemCompetitionPlacedSlots: function() {
        if (Meteor.user() && Template.instance().IsEntered.get()) {
            return SlotsPlaced.find({}, {
                sort: {
                    _id: -1
                },
                limit: 11
            });
        } else {
            return SlotsPlaced.find({}, {
                sort: {
                    _id: -1
                },
                limit: 26
            });
        }

        // return Template.instance().ItemCompetitionPlacedSlots.get();
    },
    IsUserEntered: function() {
        return Template.instance().IsEntered.get();
    }
});
Template.ItemCompetitionGrid.events({
    'click .RightArrow': function(event, template) {
        if (Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            if (Session.get("GridSectionMiddleData")[0].GridRange < 11000) {
                var newMiddleGridArray = [];
                for (var i = 0; i < Session.get("GridSectionMiddleData").length; i++) {
                    newMiddleGridArray[i] = {
                        GridRange: Session.get("GridSectionMiddleData")[i].GridRange + 1000,
                        GridNumberButton: (Session.get("GridSectionMiddleData")[i].GridRange + 1001) + " - " + (Session.get("GridSectionMiddleData")[i].GridRange + 1100)
                    }
                }
                Session.set("GridSectionMiddleData", newMiddleGridArray);
            }
        } else if (Session.get("GridSection") === "ItemCompetitionGridInner") {
            if (Session.get("GridSectionInnerData") < 12000) {
                Session.set("GridSectionInnerData", Session.get("GridSectionInnerData") + 100);


            }
        }
    },
    'click .LeftArrow': function(event, template) {
        if (Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            if (Session.get("GridSectionMiddleData")[0].GridRange > 100) {
                var newMiddleGridArray = [];
                for (var i = 0; i < Session.get("GridSectionMiddleData").length; i++) {
                    newMiddleGridArray[i] = {
                        GridRange: Session.get("GridSectionMiddleData")[i].GridRange - 1000,
                        GridNumberButton: (Session.get("GridSectionMiddleData")[i].GridRange - 999) + " - " + (Session.get("GridSectionMiddleData")[i].GridRange - 900)
                    }
                }
                Session.set("GridSectionMiddleData", newMiddleGridArray);
            }
        } else if (Session.get("GridSection") === "ItemCompetitionGridInner") {
            if (Session.get("GridSectionInnerData") > 0) {
                Session.set("GridSectionInnerData", Session.get("GridSectionInnerData") - 100);

            }
        }
    },
    'click .ZoomOut': function(event, template) {
        if (Session.get("GridSection") === "ItemCompetitionGridMiddle") {
            Session.set("GridSection", "ItemCompetitionGridOuter");
        }
        if (Session.get("GridSection") === "ItemCompetitionGridInner") {
            var gridSectionInnerDataToString = String(Session.get("GridSectionInnerData"));
            var newMiddleGridArray = [];
            if (gridSectionInnerDataToString.length == 3) {
                for (var i = 0; i < 10; i++) {
                    newMiddleGridArray[i] = {
                        GridRange: i * 100,
                        GridNumberButton: ((i * 100) + 1) + "-" + ((i * 100) + 100)
                    }
                }
                Session.set("GridSectionMiddleData", newMiddleGridArray);
            } else if (gridSectionInnerDataToString.length == 4) {
                var subsetGridSectionInnerDataToString = gridSectionInnerDataToString.substring(0, 1);
                for (var i = 0; i < 10; i++) {
                    newMiddleGridArray[i] = {
                        GridRange: (Number(subsetGridSectionInnerDataToString) * 1000) + (i * 100),
                        GridNumberButton: ((Number(subsetGridSectionInnerDataToString) * 1000) + (i * 100) + 1) + " - " + ((Number(subsetGridSectionInnerDataToString) * 1000) + (i * 100) + 100)
                    }
                }
                Session.set("GridSectionMiddleData", newMiddleGridArray);
            } else if (gridSectionInnerDataToString.length == 5) {
                var subsetGridSectionInnerDataToString = gridSectionInnerDataToString.substring(0, 2);
                for (var i = 1; i <= 10; i++) {
                    newMiddleGridArray[i - 1] = {
                        GridRange: (Number(subsetGridSectionInnerDataToString) * 1000) + (i * 100),
                        GridNumberButton: ((Number(subsetGridSectionInnerDataToString) * 1000) + (i * 100) - 99) + " - " + ((Number(subsetGridSectionInnerDataToString) * 1000) + (i * 100))
                    }
                }
                Session.set("GridSectionMiddleData", newMiddleGridArray);
            }
            Session.set("GridSection", "ItemCompetitionGridMiddle");
        }
    },
    'submit .SlotBuyModalForm': function(event, template) {
        event.preventDefault();
        Meteor.call("PlaceSlotForItemCompetition", parseInt(event.currentTarget.getAttribute('data-innergridnumber')), Session.get("GridSectionInnerData"), FlowRouter.getParam("ItemCompetitionId"), function(error, result) {});
    },
    'click .FastSlots': function(event, template) {
        Meteor.call("FastSlots", function(error, result) {});
    },
    'click .ItemCompetitionGridHeaderSlotCount .Value': function(event, template) {
        if (this.SlotCount != 0) {
            if (this.SlotCount % 100 == 0) {
                Session.set("GridSectionInnerData", this.SlotCount - 100);
            } else {
                Session.set("GridSectionInnerData", Math.floor(this.SlotCount / 100) * 100);
            }
            Session.set("GridSection", "ItemCompetitionGridInner");
        }
    }
});
