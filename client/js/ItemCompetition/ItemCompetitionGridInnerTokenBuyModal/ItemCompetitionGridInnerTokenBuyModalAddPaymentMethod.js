Template.ItemCompetitionGridInnerTokenBuyModalAddPaymentMethod.onRendered(function() {
    $('input.cc-num').payment('formatCardNumber');
    $('input.cc-exp').payment('formatCardExpiry');
    $('input.cc-cvc').payment('formatCardCVC');
    $('[data-numeric]').payment('restrictNumeric');
});

Template.ItemCompetitionGridInnerTokenBuyModalAddPaymentMethod.events({
    'submit form': function(event, template) {
        event.preventDefault();
        document.getElementById("AddPaymentSubmitButton").disabled = true;
        ccNum = $('.cc-num').val();
        cvc = $('.cc-cvc').val();
        expMo = $('.cc-exp').val().substring(0, 2);
        expYr = $('.cc-exp').val().split("/ ").pop();
        name = $('.cc-name').val();
        Stripe.card.createToken({
            name: name,
            number: ccNum,
            cvc: cvc,
            exp_month: expMo,
            exp_year: expYr,
        }, function(status, response) {
            if (response.error) {
                template.$('#error').html(response.error.message);
                document.getElementById("AddPaymentSubmitButton").disabled = false;
            } else {
                stripeToken = response.id;
                Meteor.call('AddPaymentMethod', stripeToken, function(error, result) {
                    var modal = $('[data-remodal-id=AddNewPaymentMethodModal]').remodal();
                    modal.close();
                });
            }
        });
        Session.set("ItemCompetitionGridInnerTokenBuyModalTemplate", "ItemCompetitionGridInnerTokenBuyModalPurchaseSection");
    }
});
