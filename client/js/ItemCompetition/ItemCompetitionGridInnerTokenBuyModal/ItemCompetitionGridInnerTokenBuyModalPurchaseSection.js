Template.ItemCompetitionGridInnerTokenBuyModalPurchaseSection.onCreated(function() {
    this.TokenBuyAmount = new ReactiveVar(150);
    this.FreeTokenAmount = new ReactiveVar(0);
    this.TokenCostAmount = new ReactiveVar(this.TokenBuyAmount.get() / 10);
    this.ProceedToPayment = new ReactiveVar(false);
    this.DesiredPaymentInformation = new ReactiveVar();
    this.AlternativePaymentInformation = new ReactiveVar();
    var self = this;
    if(Meteor.user()){
    Meteor.call("RetrievePaymentMethods", function(error, result) {
        if (result.length > 0) {
            Session.set("DesiredPaymentMethod", result[0].id);
            Session.set("DefaultPaymentInformation", result[0]);
            result.splice(0, 1);
            Session.set("AlternativePaymentInformation", result);
            Session.set("NewOrSaved", false);
            // Session.set("TokenScreenInformation", "SavedPaymentInformation");
        } else {
            Session.set("NewOrSaved", true);

            // Session.set("TokenScreenInformation", "NewPaymentInformation");
        }
    });
  }
});

Template.ItemCompetitionGridInnerTokenBuyModalPurchaseSection.onRendered(function() {
    var self = this;
    var x = this.$("#slider").noUiSlider({
        start: self.TokenBuyAmount.get(),
        connect: "lower",
        step: 1,
        range: {
            'min': 150,
            'max': 10000
        },
        format: wNumb({
            decimals: 0
        })
    }).on('slide', function(ev, val) {
        // set real values on 'slide' event
        self.TokenBuyAmount.set(val);
    }).on('change', function(ev, val) {
        // round off values on 'change' event
        self.TokenBuyAmount.set(val);
    });
    var inputFormat = document.getElementById("slider-input");

    inputFormat.addEventListener('keyup', function() {
        if (Number(this.value) > 10000) {
            self.TokenBuyAmount.set(10000);
            x.val(10000);
            this.value = 10000;
        }
        if (150 <= Number(this.value) && Number(this.value) <= 10000) {
            self.TokenBuyAmount.set(this.value);
            x.val(this.value);
        }
    });
    inputFormat.addEventListener('change', function() {
        if (Number(this.value) < 150) {
            this.value = 150;
            self.TokenBuyAmount.set(150);
            x.val(150);
        }
    });

});
Template.ItemCompetitionGridInnerTokenBuyModalPurchaseSection.helpers({
    TokenBuyAmount: function() {
        return Template.instance().TokenBuyAmount.get();
    },
    FreeTokenAmount: function() {
        Template.instance().FreeTokenAmount.set(Math.floor((Template.instance().TokenBuyAmount.get() - 150) / 50));
        return Template.instance().FreeTokenAmount.get();
    },
    TokenCostAmount: function() {
        Template.instance().TokenCostAmount.set(Template.instance().TokenBuyAmount.get() / 10)
        return Template.instance().TokenCostAmount.get();
    },
    NewOrSaved: function() {
        return Session.get("NewOrSaved");
    },
    DesiredPaymentMethod: function(Id) {
        if (Id.hash.Id == Session.get("DesiredPaymentMethod")) {
            return "DesiredPaymentMethod";
        }
        return "";
    },
    DesiredPaymentMethodCheck: function(Id) {
        if (Id.hash.Id == Session.get("DesiredPaymentMethod")) {
            return true;
        }
        return false;
    },
    DefaultPaymentMethod: function() {
        return Session.get("DefaultPaymentInformation");
    },
    AlternativePaymentMethods: function() {
        return Session.get("AlternativePaymentInformation");
    }
});
Template.ItemCompetitionGridInnerTokenBuyModalPurchaseSection.events({
    'click #BuyTokensButton' (event, template) {
        event.preventDefault();
        document.getElementById("BuyTokensButton").disabled = true;
        if (Session.get("NewOrSaved")) {
            ccNum = $('.cc-num').val();
            cvc = $('.cc-cvc').val
            expMo = $('.cc-exp').val().substring(0, 2);
            expYr = $('.cc-exp').val().split("/ ").pop();
            name = $('.cc-name').val();

            Stripe.card.createToken({
                name: name,
                number: ccNum,
                cvc: cvc,
                exp_month: expMo,
                exp_year: expYr,
            }, function(status, response) {
                if (response.error) {
                    template.$('#error').html(response.error.message);
                    document.getElementById("BuyTokensButton").disabled = false;
                } else {
                    Meteor.call('ChargePaymentMethodBuyTokensNew', template.TokenBuyAmount.get(), response, function(error, result) {
                        if (result) {
                            Session.set("TokenScreenInformation", "TokenBuySuccess");
                        } else {
                          template.$('#error').html(error.reason);
                          document.getElementById("BuyTokensButton").disabled = false;
                        }
                    });
                }
            });
        } else {
            Meteor.call("ChargePaymentMethodBuyTokensSaved", template.TokenBuyAmount.get(), Session.get("DesiredPaymentMethod"), function(error, result) {
                if (result) {
                    Session.set("ItemCompetitionGridInnerTokenBuyModalTemplate", "ItemCompetitionGridInnerTokenBuyModalPurchaseSuccess");
                } else {
                    template.$('#error').html(error);
                }
            });
        }

    },
    'click .AddNewPaymentMethodButton': function() {
        Session.set("ItemCompetitionGridInnerTokenBuyModalTemplate", "ItemCompetitionGridInnerTokenBuyModalAddPaymentMethod");
    },
    'click .PaymentDropdownBox': function(event, template) {
        Session.set("DesiredPaymentMethod", this.id);
    },
    'click .ProceedToPaymentSection' (event, template) {
        event.preventDefault();
        Session.set("TokensSectionExpand", true);
        Meteor.setTimeout(function() {
            template.ProceedToPayment.set(true);
        }, 600);
    },
})
