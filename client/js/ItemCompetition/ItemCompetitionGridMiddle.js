Template.ItemCompetitionGridMiddle.onCreated(function() {});
Template.ItemCompetitionGridMiddle.helpers({
    MiddleGridButtons: function() {
        return Session.get("GridSectionMiddleData");
    }
});
Template.ItemCompetitionGridMiddle.events({
    'click .MiddleGridButton': function(event, template) {
        Session.set("GridSectionInnerData", parseInt(event.currentTarget.getAttribute('data-middlegridnumbers')));
        Session.set("GridSection", "ItemCompetitionGridInner");
    }
});
