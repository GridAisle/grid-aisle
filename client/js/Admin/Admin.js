Template.Admin.onCreated(function() {

    this.AdminTemplate = new ReactiveVar();
});
Template.Admin.helpers({
    AdminTemplate: function() {
        return Template.instance().AdminTemplate.get();
    },
    Active: function(Id) {
        if (Id.hash.Id == Template.instance().AdminTemplate.get()) {
            return "active";
        }
    }
});
Template.Admin.events({
    'click #CreateItemCompetition': function(event, template) {
        template.AdminTemplate.set("CreateItemCompetition");
    },
    'click #CreatedItemCompetitions': function(event, template) {
        template.AdminTemplate.set("CreatedItemCompetitions");
    },
    'click #CompletedItemCompetitions': function(event, template) {
        template.AdminTemplate.set("CompletedItemCompetitions");
    },
    'click #UserTickets': function(event, template) {
        template.AdminTemplate.set("UserTickets");
    },
    'click #PlacedOrders': function(event, template) {
        template.AdminTemplate.set("PlacedOrders");
    },
    'click #CompletedOrders': function(event, template) {
        template.AdminTemplate.set("CompletedOrders");
    },
    'click #CreatedItems': function(event, template) {
        template.AdminTemplate.set("CreatedItems")
    },
    'click #CreateItem': function(event, template) {
        template.AdminTemplate.set("CreateItem")
    },
    'click #UserAccounts': function(event, template) {
        template.AdminTemplate.set("UserAccounts");
    },
    'click #ChangeNotification': function(event, template) {
        template.AdminTemplate.set("ChangeNotification");
    },
    'click #ReplacementReturnRequests': function(event, template) {
        template.AdminTemplate.set("ReplacementReturnRequests");
    },
    'click #ReplacementReturnRequestsInformationEmailSent': function(event, template) {
        template.AdminTemplate.set("ReplacementReturnRequestsInformationEmailSent");
    },
    'click #CompletedReplacementReturns': function(event, template) {
        template.AdminTemplate.set("CompletedReplacementReturns");
    },
    'click #FacebookQuest': function(event, template) {
        template.AdminTemplate.set("FacebookQuest");
    },


});

Template.CreateItemCompetition.onCreated(function() {
    this.subscribe("AdminCreateItemCompetition");
    this.SelectedItem = new ReactiveVar();
    this.endTime = new ReactiveVar();
    this.startTime = new ReactiveVar();
    this.CreatedItemCompetitionId = new ReactiveVar();
});
Template.CreateItemCompetition.onRendered(function() {
    this.startTime.set(rome(datetimepickerstarttime, {
        timeInterval: 300,
        min: moment()
    }));
    this.endTime.set(rome(datetimepickerendtime, {
        timeInterval: 300,
        min: moment().add(5, 'minutes')
    }));
})
Template.CreateItemCompetition.helpers({
    Items: function() {
        return Items.find({});
    },
    SelectedItem: function() {
        return Items.findOne({
            _id: Template.instance().SelectedItem.get()
        });
    },
    ReenableButton: function() {
        if (Template.instance().CreatedItemCompetitionId.get()) {
            if (ItemCompetitions.findOne({
                    _id: Template.instance().CreatedItemCompetitionId.get()
                }).CreationComplete) {
                Template.instance().CreatedItemCompetitionId.set(undefined);
                Template.instance().$('#Success').html("Competition Successfully Added!");
                return false;
            } else {
                return true;
            }
        }

    }
});
Template.CreateItemCompetition.events({
    'change #SelectItem': function(event, template) {
        template.SelectedItem.set(event.target.value);
    },
    'submit .CreateCompetitionForm': function(event, template) {
        event.preventDefault();
        template.$('#Success').html("");
        document.getElementById("CreateCompetitionButton").disabled = true;
        var ItemCompetitionObject = {};
        ItemCompetitionObject.StartTimeOfCompetition = template.startTime.get().getDate();
        ItemCompetitionObject.EndTimeOfCompetition = template.endTime.get().getDate();
        ItemCompetitionObject.Item = event.target.SelectedItemId.value;
        Meteor.call("AddItemCompetition", ItemCompetitionObject, function(error, result) {
            if (result) {
                template.CreatedItemCompetitionId.set(result);
                document.getElementById("CreateCompetitionForm").reset();

            }
            if (error) {
                document.getElementById("CreateCompetitionButton").disabled = false;
                document.getElementById("CreateCompetitionForm").reset();
            }
        })
    }
});
Template.CreatedItemCompetitions.onCreated(function() {
    this.subscribe("AdminCreatedItemCompetitions");
    this.DesiredCompetitionRemodalId = new ReactiveVar();
});
Template.CreatedItemCompetitions.onRendered(function() {
    var self = this;
    $(document).on('confirmation', '.remodal', function() {
        Meteor.call("RemoveCompetition", self.DesiredCompetitionRemodalId.get(), function(errror, result) {

        });
    });
});
Template.CreatedItemCompetitions.helpers({
    CreatedItemCompetitions: function() {
        return ItemCompetitions.find({}, {
            sort: {
                "StartTimeOfCompetition": 1
            }
        });
    }
});
Template.CreatedItemCompetitions.events({
    'click .OpenRemoveCompetitionModal': function(event, template) {
        template.DesiredCompetitionRemodalId.set(this._id);
        $('[data-remodal-id=RemoveItemCompetitionModal]').remodal().open();
    }
})
Template.CompletedItemCompetitions.onCreated(function() {
    this.subscribe("AdminCompletedItemCompetitions");
});
Template.CompletedItemCompetitions.helpers({
    CompletedItemCompetitions: function() {
        return CompletedItemCompetitions.find({});
    }
});
Template.UserAccounts.onCreated(function() {
    this.subscribe("AdminUserAccounts");
});
Template.UserAccounts.helpers({
    Accounts: function() {
        return Meteor.users.find();
    }
});
Template.UserAccounts.events({
    'click .UserAccountTitle': function(event, template) {
        document.getElementById(event.currentTarget.getAttribute('data-index')).classList.toggle('active');
    },
    'click .UserAccountContentTokensEditButton': function(event, template) {
        event.preventDefault();
        document.getElementById(event.currentTarget.getAttribute('data-index')).classList.toggle('active');
        document.getElementById(event.currentTarget.getAttribute('data-index') + " TokenEdit").classList.toggle('active');
    },
    'click .UserAccountContentTokensEditFormCancel': function(event, template) {
        event.preventDefault();
        document.getElementById(event.currentTarget.getAttribute('data-index')).classList.toggle('active');
        document.getElementById(event.currentTarget.getAttribute('data-index') + " TokenEdit").classList.toggle('active');
    },
    'submit .UserAccountContentTokensEditForm': function(event, template) {
        event.preventDefault();
        Meteor.call("UpdateTokenAmount", this._id, event.target.TokenEditValue.value, function(error, result) {

        });
        document.getElementById(event.currentTarget.getAttribute('data-index')).classList.toggle('active');
        document.getElementById(event.currentTarget.getAttribute('data-index') + " TokenEdit").classList.toggle('active');
    },

});
Template.UserTickets.onCreated(function() {
    this.subscribe("AdminUserTickets");
});
Template.UserTickets.helpers({
    UserTickets: function() {
        var users = Meteor.users.find({
            Tickets: {
                $elemMatch: {
                    Status: false
                }
            }
        }).fetch()

        var filteredTickets = _.map(users, function(user) {
            return _.filter(user.Tickets, function(Ticket) {
                Ticket.UserId = user._id;
                return Ticket.Status == false
            })
        });
        var flattenedTickets = _(filteredTickets).flatten(true);
        return flattenedTickets;
    },
    DateFormat: function(Date) {
        return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }

});
Template.UserTickets.events({
    'click .UserTicketsTitle': function(event, template) {
        document.getElementById(event.currentTarget.getAttribute('data-index')).classList.toggle('active');
    },
    'submit .ReplyToTicketForm': function(event, template) {
        event.preventDefault();
        document.querySelector(".ReplyButton").disabled = true;
        Meteor.call("RespondToTicket", this.UserId, event.target.ReplyTicketContent.value, function(error, result) {
            document.querySelector(".ReplyToTicketForm").reset();
            document.querySelector(".ReplyButton").disabled = false;
        })
    }
});
Template.PlacedOrders.onCreated(function() {
    this.subscribe("AdminPlacedOrders");

});
Template.PlacedOrders.helpers({
    PlacedOrders: function() {
        import {
            _
        } from 'underscore';
        var users = Meteor.users.find({
            Orders: {
                $elemMatch: {
                    Cancelable: false,
                    HasBeenCompleted: false
                }
            }
        }).fetch()
        var filteredOrders = _.map(users, function(user) {
            return _.filter(user.Orders, function(Order) {
                return Order.Cancelable == false && Order.HasBeenCompleted == false
            })
        });
        var flattenedOrders = _(filteredOrders).flatten(true);
        var groupedOrders = _.groupBy(flattenedOrders, 'OrderId');

        return _.map(groupedOrders, function(Order, key) {
            object = {}
            object["OrderId"] = key;
            object["Orders"] = Order;
            object["ShippingInformation"] = Order[0].ShippingInformation;
            object["OrderPlacedDate"] = Order[0].OrderPlacedDate;
            if (Order[0].TrackingUrl) {
                object["TrackingUrl"] = Order[0].TrackingUrl;
            } else {
                object["TrackingUrl"] = null;
            }
            return object;
        });

    },
    OrderPlacedDateFormat: function(Date) {
        return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }
});
Template.PlacedOrders.events({
    'click .OrderDeliveredButton': function(event, template) {
        Meteor.call("OrderComplete", this.OrderId, function(error, result) {

        });
    },
    'submit #AddTrackingInformationForm': function(event, template) {
        event.preventDefault();
        Meteor.call("AddTrackingUrl", this, event.target.TrackingUrl.value, function(error, result) {

        })
    }
});
// Template.PendingShipmentOrders.onCreated(function() {
//     this.subscribe("AdminPendingShipmentOrders");
// });
// Template.PendingShipmentOrders.helpers({
//     PendingShipmentOrders: function() {
//         return Meteor.users.find({
//             Orders: {
//                 $elemMatch: {
//                     HasBeenShipped: false
//                 }
//             }
//         });
//     },
//     OrderPlacedDateFormat: function(Date) {
//         return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
//     }
// });
// Template.PendingShipmentOrders.events({
//     'submit .ShipOrderForm': function(event, template) {
//         event.preventDefault();
//         Meteor.call("MoveOrderToShipped", event.currentTarget.getAttribute('data-userid'), this._id, function(error, result) {
//
//         });
//     }
// });
Template.CompletedOrders.onCreated(function() {
    this.subscribe("AdminCompletedOrders");
});
Template.CompletedOrders.helpers({
    CompletedOrders: function() {
        import {
            _
        } from 'underscore';
        var users = Meteor.users.find({
            Orders: {
                $elemMatch: {
                    Cancelable: false,
                    HasBeenCompleted: true
                }
            }
        }).fetch()
        var filteredOrders = _.map(users, function(user) {
            return _.filter(user.Orders, function(Order) {
                return Order.Cancelable == false && Order.HasBeenCompleted == true
            })
        });
        var flattenedOrders = _(filteredOrders).flatten(true);
        var groupedOrders = _.groupBy(flattenedOrders, 'OrderId');

        return _.map(groupedOrders, function(Order, key) {
            object = {}
            object["OrderId"] = key;
            object["Orders"] = Order;
            object["ShippingInformation"] = Order[0].ShippingInformation;
            object["OrderPlacedDate"] = Order[0].OrderPlacedDate;
            return object;
        });
    },
    OrderPlacedDateFormat: function(Date) {
        return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }
});
Template.PendingShipmentOrders.onCreated(function() {
    this.subscribe("AdminPendingShipmentOrders");
});
Template.CompletedOrders.onCreated(function() {
    this.subscribe("AdminCompletedOrders");
});
Template.CreateItem.onCreated(function() {
    this.Main_Image = new ReactiveVar();
});
Template.CreateItem.onRendered(function() {
    $('#Description').trumbowyg();
});
Template.CreateItem.events({
    'change #NewItemMainImage': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedMainImage').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);

    },
    'change #NewItemAlternateImage1': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedAlternateImage1').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);

    },
    'change #NewItemAlternateImage2': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedAlternateImage2').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);

    },
    'change #NewItemAlternateImage3': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedAlternateImage3').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);
    },
    'submit #CreateItemForm': function(event, template) {
        event.preventDefault();
        var newItem = {};
        newItem.Name = event.target.Name.value;
        newItem.SellingPrice = event.target.SellingPrice.value;
        newItem.Description = $('#Description').trumbowyg('html');

        var uploader = new Slingshot.Upload("myFileUploads");
        if (document.getElementById('NewItemMainImage').files[0]) {
            uploader.send(document.getElementById('NewItemMainImage').files[0], function(error, downloadUrl) {
                if (error) {
                    // Log service detailed response.
                    alert(error);
                } else {
                    newItem.Main_Image = downloadUrl;
                    if (document.getElementById('NewItemAlternateImage1').files[0]) {
                        uploader.send(document.getElementById('NewItemAlternateImage1').files[0], function(error, downloadUrl) {
                            if (error) {
                                // Log service detailed response.
                                alert(error);
                            } else {
                                newItem.Alternate_Image_1 = downloadUrl;
                                if (document.getElementById('NewItemAlternateImage2').files[0]) {
                                    uploader.send(document.getElementById('NewItemAlternateImage2').files[0], function(error, downloadUrl) {
                                        if (error) {
                                            // Log service detailed response.
                                            alert(error);
                                        } else {
                                            newItem.Alternate_Image_2 = downloadUrl;
                                            if (document.getElementById('NewItemAlternateImage3').files[0]) {
                                                uploader.send(document.getElementById('NewItemAlternateImage3').files[0], function(error, downloadUrl) {
                                                    if (error) {
                                                        // Log service detailed response.
                                                        alert(error);
                                                    } else {
                                                        newItem.Alternate_Image_3 = downloadUrl;
                                                        Meteor.call("CreateNewItem", newItem, function(error, result) {
                                                            if (result) {
                                                                document.getElementById("CreateItemForm").reset();
                                                                template.$('#Success').html("Item Successfully Added!");
                                                                $('#Description').trumbowyg('empty');
                                                                document.getElementById("NewItemMainImage").value = "";
                                                                document.getElementById("UploadedMainImage").src="";
                                                                document.getElementById("NewItemAlternateImage1").value = "";
                                                                document.getElementById("UploadedAlternateImage1").src="";
                                                                document.getElementById("NewItemAlternateImage2").value = "";
                                                                document.getElementById("UploadedAlternateImage2").src="";
                                                                document.getElementById("NewItemAlternateImage3").value = "";
                                                                document.getElementById("UploadedAlternateImage3").src="";

                                                            }
                                                        });
                                                    }
                                                });
                                            } else {

                                                Meteor.call("CreateNewItem", newItem, function(error, result) {
                                                    if (result) {
                                                        document.getElementById("CreateItemForm").reset();
                                                        template.$('#Success').html("Item Successfully Added!");
                                                        $('#Description').trumbowyg('empty');
                                                        document.getElementById("NewItemMainImage").value = "";
                                                        document.getElementById("UploadedMainImage").src="";
                                                        document.getElementById("NewItemAlternateImage1").value = "";
                                                        document.getElementById("UploadedAlternateImage1").src="";
                                                        document.getElementById("NewItemAlternateImage2").value = "";
                                                        document.getElementById("UploadedAlternateImage2").src="";
                                                        document.getElementById("NewItemAlternateImage3").value = "";
                                                        document.getElementById("UploadedAlternateImage3").src="";
                                                    }
                                                });
                                            }
                                        }
                                    });
                                } else {
                                    Meteor.call("CreateNewItem", newItem, function(error, result) {
                                        if (result) {
                                            document.getElementById("CreateItemForm").reset();
                                            template.$('#Success').html("Item Successfully Added!");
                                            $('#Description').trumbowyg('empty');
                                            document.getElementById("NewItemMainImage").value = "";
                                            document.getElementById("UploadedMainImage").src="";
                                            document.getElementById("NewItemAlternateImage1").value = "";
                                            document.getElementById("UploadedAlternateImage1").src="";
                                            document.getElementById("NewItemAlternateImage2").value = "";
                                            document.getElementById("UploadedAlternateImage2").src="";
                                            document.getElementById("NewItemAlternateImage3").value = "";
                                            document.getElementById("UploadedAlternateImage3").src="";
                                        }
                                    });
                                }

                            }
                        });
                    } else {
                        Meteor.call("CreateNewItem", newItem, function(error, result) {
                            if (result) {
                                document.getElementById("CreateItemForm").reset();
                                template.$('#Success').html("Item Successfully Added!");
                                $('#Description').trumbowyg('empty');
                                document.getElementById("NewItemMainImage").value = "";
                                document.getElementById("UploadedMainImage").src="";
                                document.getElementById("NewItemAlternateImage1").value = "";
                                document.getElementById("UploadedAlternateImage1").src="";
                                document.getElementById("NewItemAlternateImage2").value = "";
                                document.getElementById("UploadedAlternateImage2").src="";
                                document.getElementById("NewItemAlternateImage3").value = "";
                                document.getElementById("UploadedAlternateImage3").src="";
                            }
                        });
                    }
                }
            });
        }

    }
});
Template.CreatedItems.onCreated(function() {
    this.subscribe("AdminCreatedItems");
    this.DesiredItemRemodalId = new ReactiveVar();
});
Template.CreatedItems.onRendered(function() {
    var self = this;
    $(document).on('confirmation', '.remodal', function() {
        Meteor.call("RemoveItem", self.DesiredItemRemodalId.get(), function(errror, result) {

        });
    });
});
Template.CreatedItems.helpers({
    CreatedItems: function() {
        return Items.find({});
    }
});
Template.CreatedItems.events({
    'click .CreatedItemTitle': function(event, template) {
        if (document.getElementById(this._id).classList.contains('active')) {
            document.getElementById(this._id).classList.remove('active');

        } else {
            document.getElementById(event.currentTarget.getAttribute('data-index') + " CreatedItemContent").classList.toggle('active');
        }
    },
    'click .CreatedItemMakeUnavailableButton': function(event, template) {
        Meteor.call("MakeCreatedItemUnavailable", this._id, function(error, result) {});
    },
    'click .CreatedItemMakeAvailableButton': function(event, template) {
        Meteor.call("MakeCreatedItemAvailable", this._id, function(error, result) {});
    },
    'click .RemoveItemButton': function(event, template) {
        template.DesiredItemRemodalId.set(this._id);
        $('[data-remodal-id=RemoveCreatedItemModal]').remodal().open();
    },
    'click .EditItemButton': function(event, template) {
        document.getElementById(event.currentTarget.parentNode.id).classList.toggle('active');
        document.getElementById(this._id).classList.toggle('active');
        $('#Description' + this._id).trumbowyg();
        $('#Description' + this._id).trumbowyg('html', this.Description);
    },
    'click .CreatedItemEditSectionCancelChanges': function(event, template) {
        event.preventDefault();
        document.getElementById(event.currentTarget.getAttribute('data-index') + " CreatedItemContent").classList.toggle('active');
        document.getElementById(this._id).classList.toggle('active');
    },
    'change #NewItemMainImage': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedMainImage').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);

    },
    'change #NewItemAlternateImage1': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedAlternateImage1').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);

    },
    'change #NewItemAlternateImage2': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedAlternateImage2').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);

    },
    'change #NewItemAlternateImage3': function(event, template) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#UploadedAlternateImage3').attr('src', e.target.result);
        }
        reader.readAsDataURL(event.target.files[0]);
    },
    'submit .CreatedItemEditForm': function(event, template) {
        var updatedItem = {};
        event.preventDefault();

    }
});
Template.ChangeNotification.onCreated(function() {
    this.subscribe("AdminChangeNotification");
});
Template.ChangeNotification.onRendered(function() {

});
Template.ChangeNotification.helpers({
    Notification: function() {
        return Notification.find({});
    }
});
Template.ChangeNotification.events({

});
Template.ReplacementReturnRequests.onCreated(function() {
    this.subscribe("AdminReplacementReturnRequests")
});

Template.ReplacementReturnRequests.helpers({
    Requests: function() {
        var requests = Meteor.users.find({
            ReplacementsRefunds: {
                $elemMatch: {
                    HasBeenRefundedReplaced: false,
                    InformationEmailSent: false
                }
            }
        }, {
            fields: {
                ReplacementsRefunds: 1
            }
        }).fetch();
        var filteredRequests = _.map(requests, function(user) {
            return _.filter(user.ReplacementsRefunds, function(request) {
                return !request.HasBeenRefundedReplaced && !request.InformationEmailSent;
            })
        });
        var flattenedRequests = _(filteredRequests).flatten(true);
        sortedFlattenedRequests = _.sortBy(flattenedRequests, 'RequestDate')

        return sortedFlattenedRequests;

    },
    RequestDateFormat: function() {
        return moment(this.RequestDate).format("dddd, MMMM Do YYYY, h:mm:ss a");
    },
    User: function() {
        return Meteor.users.findOne({
            ReplacementsRefunds: {
                $elemMatch: {
                    OrderId: this.OrderId,
                    "Item._id": this.IemId,
                    HasBeenRefundedReplaced: false
                }
            }
        }, {
            fields: {
                _id: 1,
                emails: 1
            }
        });
    },
    Item: function() {
        return Items.findOne({
            _id: this.ItemId
        });
    }
});

Template.ReplacementReturnRequests.events({
    'click .InformationEmailSentButton': function(event, template) {
        Meteor.call("ReturnReplacementInformationEmailSent", this, function(error, result) {

        })
    }
})
Template.ReplacementReturnRequestsInformationEmailSent.onCreated(function() {
    this.subscribe("AdminReplacementReturnRequestsInformationEmailSent");
});
Template.ReplacementReturnRequestsInformationEmailSent.helpers({
    Requests: function() {
        var requests = Meteor.users.find({
            ReplacementsRefunds: {
                $elemMatch: {
                    HasBeenRefundedReplaced: false,
                    InformationEmailSent: true
                }
            }
        }, {
            fields: {
                ReplacementsRefunds: 1
            }
        }).fetch();
        var filteredRequests = _.map(requests, function(user) {
            return _.filter(user.ReplacementsRefunds, function(request) {
                return !request.HasBeenRefundedReplaced && request.InformationEmailSent;
            })
        });
        var flattenedRequests = _(filteredRequests).flatten(true);
        sortedFlattenedRequests = _.sortBy(flattenedRequests, 'RequestDate')

        return sortedFlattenedRequests;

    },
    RequestDateFormat: function() {
        return moment(this.RequestDate).format("dddd, MMMM Do YYYY, h:mm:ss a");
    },
    User: function() {
        return Meteor.users.findOne({
            ReplacementsRefunds: {
                $elemMatch: {
                    OrderId: this.OrderId,
                    "Item._id": this.IemId,
                    HasBeenRefundedReplaced: false
                }
            }
        }, {
            fields: {
                _id: 1,
                emails: 1
            }
        });
    },
    Item: function() {
        return Items.findOne({
            _id: this.ItemId
        });
    }
})
Template.ReplacementReturnRequestsInformationEmailSent.events({
    'click .RefundCompleteButton': function(event, template) {
        Meteor.call("CompleteReturn", this, function(error, result) {

        });
    },
    'click .ReplacementCompleteButton': function(event, template) {
        Meteor.call("CompleteReplacement", this, function(error, result) {

        });
    }
})
Template.CompletedReplacementReturns.onCreated(function() {
    this.subscribe("AdminCompletedReplacementReturns")
});

Template.CompletedReplacementReturns.helpers({
    Requests: function() {
        var requests = Meteor.users.find({
            ReplacementsRefunds: {
                $elemMatch: {
                    HasBeenRefundedReplaced: true
                }
            }
        }, {
            fields: {
                ReplacementsRefunds: 1
            }
        }).fetch();
        var filteredRequests = _.map(requests, function(user) {
            return _.filter(user.ReplacementsRefunds, function(request) {
                return request.HasBeenRefundedReplaced;
            })
        });
        var flattenedRequests = _(filteredRequests).flatten(true);
        sortedFlattenedRequests = _.sortBy(flattenedRequests, 'RequestDate')

        return sortedFlattenedRequests;

    },
    RequestDateFormat: function() {
        return moment(this.RequestDate).format("dddd, MMMM Do YYYY, h:mm:ss a");
    },
    User: function() {
        return Meteor.users.findOne({
            ReplacementsRefunds: {
                $elemMatch: {
                    OrderId: this.OrderId,
                    "Item._id": this.IemId,
                    HasBeenRefundedReplaced: true
                }
            }
        }, {
            fields: {
                _id: 1
            }
        })._id;
    },
    Item: function() {
        return Items.findOne({
            _id: this.ItemId
        });
    }
})
Template.FacebookQuest.onCreated(function() {
    this.subscribe("AdminFacebookQuest");
})
Template.FacebookQuest.helpers({
    Users: function() {
        return Meteor.users.find({
            roles: {
                $in: ["User"]
            },
            "Quests.FacebookQuest.Submitted": true,
            "Quests.FacebookQuest.Active": true
        });
    }
})
Template.FacebookQuest.events({
    "submit #AcceptQuestForm": function(event, template) {
        event.preventDefault();
        if (event.target.friendcount.value) {
            Meteor.call("AcceptFacebookQuest", this._id, event.target.friendcount.value, function(error, result) {})
        }

    },
    'click .RejectQuest': function(event, template) {
        Meteor.call("RejectFacebookQuest", function(error, result) {})
    }
})
