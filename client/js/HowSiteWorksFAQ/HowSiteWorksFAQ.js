Template.HowSiteWorksFAQ.helpers({
    Active: function(Title) {
        if (Title.hash.Title == FlowRouter.getRouteName()) {
            return "Active";
        }
    }
})
Template.HowSiteWorksFAQ.events({
    'click .HowSiteWorks': function() {
        if (FlowRouter.getRouteName() != "HowSiteWorks") {
            FlowRouter.go("/HowSiteWorks")
        }
    },
    'click .FAQ': function() {
        if (FlowRouter.getRouteName != "FAQ") {
            FlowRouter.go("/FAQ")
        }
    }
})
