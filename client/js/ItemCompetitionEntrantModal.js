Template.ItemCompetitionEntrantModal.onCreated(function() {
    Session.set("ItemCompetitionEntrantModalTemplate", "RequestEntrance");
});
Template.ItemCompetitionEntrantModal.helpers({
    ItemCompetitionEntrantModalTemplate: function() {
        return Session.get("ItemCompetitionEntrantModalTemplate");
    }
})
Template.RequestEntrance.helpers({
    CompetitionInformation: function() {
        return Session.get("CompetitionEntrantInformation");
    }
})
Template.RequestEntrance.events({
    'click #PurchaseEntrance': function(event, template) {
        document.getElementById("PurchaseEntrance").disabled = true;
        Meteor.call("CompetitionPurchaseEntrance", Session.get("CompetitionEntrantInformation")._id, function(error, result) {
            if (error) {
                document.getElementById("PurchaseEntrance").disabled = false;
                template.$('#error').html(error.reason);
            }
            if (result) {
                console.log(result);
                Session.set("ItemCompetitionEntrantModalTemplate", "EntranceGranted")
            }
        })
    }
})
Template.EntranceGranted.helpers({
    CompetitionInformation: function() {
        return Session.get("CompetitionEntrantInformation");
    }
})

Template.EntranceGranted.events({
    'click .PrepareButton': function(event, template) {
        var inst = $('[data-remodal-id=ItemCompetitionEntrantModal]').remodal();
        inst.close();
        FlowRouter.go("/ItemCompetition/" + Session.get("CompetitionEntrantInformation")._id);
    }
})
