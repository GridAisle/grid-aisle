Template.PaymentInformation.onCreated(function() {
    this.TokenBuyAmount = new ReactiveVar(Session.get("PaymentInformationCost"));
    this.FreeTokenAmount = new ReactiveVar(0);
    this.TokenCostAmount = new ReactiveVar(this.TokenBuyAmount.get() / 10);
    this.ProceedToPayment = new ReactiveVar(false);
    this.DesiredPaymentInformation = new ReactiveVar();
    this.PaymentInformation = new ReactiveVar();
    this.NewOrSaved = new ReactiveVar();
    this.ProceedToPayment = new ReactiveVar(false);
    this.RetrievePaymentMethodsReady = new ReactiveVar(false);
    var self = this;
    Meteor.call("RetrievePaymentMethods", function(error, result) {
        if (result.length > 0) {
            self.DesiredPaymentInformation.set(result[0].id);
            self.PaymentInformation.set(result);
            self.RetrievePaymentMethodsReady.set(true);
            self.NewOrSaved.set(false);
        } else {
            self.RetrievePaymentMethodsReady.set(true);
            self.NewOrSaved.set(true);

        }
    });
});
Template.PaymentInformation.onRendered(function() {
    var self = this;
    Tracker.autorun(function() {
        if (Session.get("PaymentInformationCost") != undefined) {
            self.TokenBuyAmount.set(Session.get("PaymentInformationCost"));
            var x = this.$("#slider").noUiSlider({
                start: Session.get("PaymentInformationCost"),
                connect: "lower",
                step: 1,
                range: {
                    'min': Session.get("PaymentInformationCost"),
                    'max': Session.get("PaymentInformationCost") + 9850
                },
                format: wNumb({
                    decimals: 0
                })
            }).on('slide', function(ev, val) {
                // set real values on 'slide' event
                self.TokenBuyAmount.set(val);
            }).on('change', function(ev, val) {
                // round off values on 'change' event
                self.TokenBuyAmount.set(val);
            });
            var inputFormat = document.getElementById("slider-input");

            inputFormat.addEventListener('keyup', function() {
                if (Number(this.value) > Session.get("PaymentInformationCost") + 9850) {
                    self.TokenBuyAmount.set(Session.get("PaymentInformationCost") + 9850);
                    x.val(Session.get("PaymentInformationCost") + 9850);
                    this.value = Session.get("PaymentInformationCost") + 9850;
                }
                if (Session.get("PaymentInformationCost") <= Number(this.value) && Number(this.value) <= Session.get("PaymentInformationCost") + 9850) {
                    self.TokenBuyAmount.set(this.value);
                    x.val(this.value);
                }
            });
            inputFormat.addEventListener('change', function() {
                if (Number(this.value) < Session.get("PaymentInformationCost")) {
                    this.value = Session.get("PaymentInformationCost");
                    self.TokenBuyAmount.set(Session.get("PaymentInformationCost"));
                    x.val(Session.get("PaymentInformationCost"));
                }
            });
        }
    });


});
Template.PaymentInformation.helpers({
    IsSufficientTokenCount: function() {
        return Session.get("PaymentInformationCost");
    },
    TokenBuyAmount: function() {
        return Template.instance().TokenBuyAmount.get();
    },
    FreeTokenAmount: function() {
        Template.instance().FreeTokenAmount.set(Math.floor((Template.instance().TokenBuyAmount.get() - Session.get("PaymentInformationCost")) / 50));
        return Template.instance().FreeTokenAmount.get();
    },
    TokenCostAmount: function() {
        Template.instance().TokenCostAmount.set(Template.instance().TokenBuyAmount.get() / 10)
        return Template.instance().TokenCostAmount.get();
    },
    NewOrSaved: function() {
        return Template.instance().NewOrSaved.get();
    },
    PaymentInformation: function() {
        return Template.instance().PaymentInformation.get();
    },
    DesiredPaymentMethod: function(Id) {
        if (Id.hash.Id == Template.instance().DesiredPaymentInformation.get()) {
            return "DesiredPaymentMethod";
        }
        return "";
    },
    RetrievePaymentMethodsReady: function() {
        return Template.instance().RetrievePaymentMethodsReady.get();
    },
    DesiredPaymentMethodCheck: function(Id) {
        if (Id.hash.Id == Template.instance().DesiredPaymentInformation.get()) {
            return true;
        }
        return false;
    },
});
Template.PaymentInformation.events({
    'submit .BuyTokensProceed': function(event, template) {
        event.preventDefault();
        document.getElementById("ProceedToPaymentSectionButton").disabled = true;
        if (template.NewOrSaved.get()) {
            ccNum = $('.cc-num').val();
            cvc = $('.cc-cvc').val
            expMo = $('.cc-exp').val().substring(0, 2);
            expYr = $('.cc-exp').val().split("/ ").pop();
            name = $('.cc-name').val();
            zipcode = $('.cc-zip').val();

            Stripe.card.createToken({
                name: name,
                number: ccNum,
                cvc: cvc,
                exp_month: expMo,
                exp_year: expYr,
                address_zip: zipcode
            }, function(status, response) {
                if (response.error) {
                    template.$('#errorNew').html(response.error.message);
                    document.getElementById("ProceedToPaymentSectionButton").disabled = false;
                } else {
                    Meteor.call('ChargePaymentMethodBuyTokensNew', template.TokenBuyAmount.get(), response, function(error, result) {
                        if (result) {
                            FlowRouter.go("ShippingInformation");
                        } else {
                            document.getElementById("ProceedToPaymentSectionButton").disabled = false;
                            template.$('#errorNew').html(error.reason);
                        }
                    });

                }
            });
        } else {
            Meteor.call("ChargePaymentMethodBuyTokensSaved", template.TokenBuyAmount.get(), template.DesiredPaymentInformation.get(), function(error, result) {
                if (result) {
                    FlowRouter.go("ShippingInformation");
                } else {
                    document.getElementById("ProceedToPaymentSectionButton").disabled = false;
                    template.$('#errorSaved').html(error.reason);
                }
            });
        }
    },
    'click .PaymentDropdownBox': function(event, template) {
        template.DesiredPaymentInformation.set(this.id);
    },
    'click .AddNewPaymentMethodButton': function(event, template) {
        $('[data-remodal-id=AddNewPaymentMethodModal]').remodal({
            modifier: 'AddNewPaymentMethodModal'
        }).open();
    }
});
