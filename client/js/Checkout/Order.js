Template.Order.onCreated(function() {
    Tracker.autorun(function() {
        if (Meteor.user()) {
            if (Session.get("DesiredShippingAddress") == null) {
                var AlternativeShippingAddresses = [];
                for (var i = 0; i < Meteor.user().Addresses.length; i++) {
                    if (Meteor.user().Addresses[i].IsDefault == true) {
                        Session.set("DesiredShippingAddress", Meteor.user().Addresses[i]);
                    } else {
                        AlternativeShippingAddresses.push(Meteor.user().Addresses[i]);
                    }
                }
                Session.set("AlternativeShippingAddresses", AlternativeShippingAddresses);
            }
        }
    });


});
Template.Order.helpers({
    DesiredShippingInformation: function() {
        return Session.get("DesiredShippingAddress");
    },
    AlternativeShippingInformation: function() {
        return Session.get("AlternativeShippingAddresses");
    },
    OrderTotal: function() {
        if (Meteor.user()) {
            var orderTotal = 0;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].CartOrReserve) {
                    orderTotal += (Meteor.user().Cart[i].Item.SellingPrice - Meteor.user().Cart[i].Discount) * Meteor.user().Cart[i].Quantity;
                }
            }
            return orderTotal;
        }
    }

});
Template.Order.events({
    'click .PlaceOrderButton': function() {
        document.getElementById("PlaceOrderButton").disabled = true;
        Meteor.call('PlaceOrder', Session.get("DesiredShippingAddress"), function(error, result) {
            if (result) {
                var Title = '<i class="fa fa-check" aria-hidden="true"></i>';
                var Message = '<div class="Top">Thank you, your order has been placed!</div><div class="Bottom"><div class="OrderInformation"><div>Order Number: <span>' + result[0]._id + '</span></div><a href="/UserProfile/OrderHistory">Review or edit your order <i class="fa fa-caret-right" aria-hidden="true"></i></a></div><div class="ShippingInformation"><div><strong><u>Shipping Information</u></strong></div><div><div>' + result[0].ShippingInformation.FirstName + ' ' + result[0].ShippingInformation.LastName + '</div><div>' + result[0].ShippingInformation.Address + '</div><div class="CityStateZip"><div>' + result[0].ShippingInformation.City + ',</div><div>' + result[0].ShippingInformation.State + '</div><div> ' + result[0].ShippingInformation.Zipcode + '</div></div><div>' + result[0].ShippingInformation.PhoneNumber + '</div></div>';
                Notifications.success(Title, Message, {
                    clickBodyToClose: false
                });
                FlowRouter.go('/');

            } else {
                document.getElementById("PlaceOrderButton").disabled = false;
            }

        });
    },
    'click .ShippingInformationDropdownDropdownBox': function(event, template) {
        var newDesiredShippingAddress = Session.get("AlternativeShippingAddresses")[event.currentTarget.id];
        var tempAlternativeShippingAddresses = Session.get("AlternativeShippingAddresses");
        var oldDesiredShippingAddress = Session.get("DesiredShippingAddress");
        tempAlternativeShippingAddresses.splice(event.currentTarget.id, 1);
        tempAlternativeShippingAddresses.push(oldDesiredShippingAddress);
        Session.set("AlternativeShippingAddresses", tempAlternativeShippingAddresses);
        Session.set("DesiredShippingAddress", newDesiredShippingAddress);
        Dropdowns.hide("OrderAddressDropdown");
    },
    'click .AddNewShippingInformationButton': function() {
        $('[data-remodal-id=AddNewShippingInformationModal]').remodal().open();
    }
});
