Template.Checkout.onCreated(function() {
    this.subscribe("UserProfileCheckout");
});
Template.Checkout.helpers({
    BreadCrumb: function() {
        return Session.get("CheckoutBreadcrumb");
    },
    ShippingInformationTabHighlight: function() {
        if (Session.get("CheckoutBreadcrumb") == "ShippingInformation" || Session.get("CheckoutBreadcrumb") == "Order") {
            return "activebreadcrumb";
        }
    },
    OrderTabHighlight: function() {
        if (Session.get("CheckoutBreadcrumb") == "Order") {
            return "activebreadcrumb";
        }
    },
    CartInformation: function() {
        if (Meteor.user()) {
            return Meteor.user().Cart;
        }
    },
    CheckoutData: function() {
        return Meteor.user();
    },
    ItemTotal:function(CartItem){
      return (CartItem.hash.CartItem.Item.SellingPrice*CartItem.hash.CartItem.Quantity)-CartItem.hash.CartItem.Discount;
    },
    ItemNameFormatSize:function(){
      var fontSize = 700 / this.Item.Name.length;
      if (fontSize > 25) {
          return "";
      }
      return fontSize + "px";
    }
});
