Template.Item.onCreated(function() {
    this.subscribe("Item", FlowRouter.getParam("ItemId"));
    this.Quantity = new ReactiveVar(1);
    this.SelectedImage = new ReactiveVar("Main_Image");
    this.ItemId = new ReactiveVar(FlowRouter.getParam("ItemId"));

});
Template.Item.onRendered(function() {

})
Template.Item.helpers({
    Item: function() {
        return Items.findOne({
            _id: FlowRouter.getParam("ItemId")
        })
    },
    Quantity: function() {
        return Template.instance().Quantity.get();
    },
    SimilarItemCompetitions: function() {
        return ItemCompetitions.find({}, {
            sort: {
                "StartTimeOfCompetition": 1
            }
        });

    },
    ItemNameSize: function() {
        var fontSize = 2500 / this.Name.length;
        if (fontSize > 40) {
            return "";
        }
        return fontSize + "px";
    },
    IsOnUserWatchList: function(Competition_Id) {
        if (Meteor.users.findOne({
                _id: Meteor.userId(),
                'Watchlist': {
                    $in: [Competition_Id.hash.Competition_Id]
                }
            })) {
            return true;
        }
        return false;

    },
    ItemCompetitiveLiveCountdown: function(Competition) {
        var diff = moment(Competition.hash.Competition.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        var self = this;
        Meteor.setInterval(function() {
            var diff = moment(Competition.hash.Competition.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    ItemCompetitiveNotLiveCountdown: function(Competition) {
        var diff = moment(Competition.hash.Competition.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();

        Meteor.setInterval(function() {
            var diff = moment(Competition.hash.Competition.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    Progress: function(CompetitionProgress) {
        return 100 - ((CompetitionProgress.hash.CompetitionProgress % 1) * 100);
    },
    ProgressText: function(Progress) {
        return Math.floor(Progress.hash.Progress * 100) + "%";
    },
    SelectedImage: function(ImageName) {
        if (ImageName.hash.ImageName == Template.instance().SelectedImage.get()) {
            return "Selected";
        }
        return "";
    },
    IsUserEntered: function() {
        if (Meteor.user() && Meteor.user().CompetitionsEntered) {
            var self = this;
            var isEntered = _.some(Meteor.user().CompetitionsEntered, function(Competition) {
                return Competition == self._id
            });
            return isEntered;
        }

    }
});
Template.Item.events({
    'click .DecreaseQuantity' (event, template) {
        if (template.Quantity.get() > 1) {
            template.Quantity.set(template.Quantity.get() - 1);
        }
    },
    'click .IncreaseQuantity' (event, template) {
        if (template.Quantity.get() < 10) {
            template.Quantity.set(template.Quantity.get() + 1);
        }

    },
    'click .AddToCartButton' (event, template) {
        Meteor.call('AddToCart', FlowRouter.getParam("ItemId"), template.Quantity.get(), function(error, result) {

            Session.set("AddToCartItemId", template.ItemId.get());
        });
        FlowRouter.go('/AddToCartResult');
    },
    'click .Watchlist-Star-Add': function(event, template) {
        Meteor.call("AddToWatchlist", this._id);
    },
    'click .Watchlist-Star-Remove': function(event, template) {
        Meteor.call("RemoveFromWatchlist", this._id);
    },
    'mouseenter .MainImageSidebar': function(event, template) {
        template.SelectedImage.set(event.currentTarget.name);
        document.getElementById("DefaultPicture").src = event.currentTarget.currentSrc;
    },
    'mouseenter .AlternateImage1Sidebar': function(event, template) {
        template.SelectedImage.set(event.currentTarget.name);
        document.getElementById("DefaultPicture").src = event.currentTarget.currentSrc;
    },
    'mouseenter .AlternateImage2Sidebar': function(event, template) {
        template.SelectedImage.set(event.currentTarget.name);
        document.getElementById("DefaultPicture").src = event.currentTarget.currentSrc;
    },
    'mouseenter .AlternateImage3Sidebar': function(event, template) {
        template.SelectedImage.set(event.currentTarget.name);
        document.getElementById("DefaultPicture").src = event.currentTarget.currentSrc;
    },
    'click .EnterCompetitionButton': function(event, template) {
        Session.set("CompetitionEntrantInformation", this);
        $('[data-remodal-id=ItemCompetitionEntrantModal]').remodal({
            "closeOnConfirm": false,
            modifier: 'EnterCompetitionModal'
        }).open();
    }
});
