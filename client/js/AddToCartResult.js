Template.AddToCartResult.onCreated(function() {
    this.subscribe("UserProfileAddToCartResult");
});

Template.AddToCartResult.helpers({
    AddedCartItem: function() {
        if (Meteor.user() && Meteor.user().Cart) {
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].WonFromCompetition == false && Meteor.user().Cart[i].Item._id == Session.get("AddToCartItemId")) {
                    return Meteor.user().Cart[i];
                }
            }

        }
    },
    AddedCartItemTotalCost:function(Item){
      return Item.hash.Item.Item.SellingPrice*Item.hash.Item.Quantity;
    },
    ItemFormatSize:function(){
      var fontSize = 550 / this.Name.length;
      if (fontSize > 40) {
          return "";
      }
      return fontSize + "px";
    }

})
