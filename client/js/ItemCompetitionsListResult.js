Template.ItemCompetitionsListResult.onCreated(function() {
    this.subscribe("ItemCompetitionsListResult");
});
Template.ItemCompetitionsListResult.helpers({
    ItemCompetitionResult: function() {
        if (Meteor.user() && Meteor.user().Cart) {
            var winningObject = {}
            winningObject["WinningInfo"] = [];
            winningObject["DidWin"] = false;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].hasOwnProperty("WinningCompetitionId")) {
                    if (Meteor.user().Cart[i].WinningCompetitionId == this._id) {
                        winningObject["WinningInfo"].push({
                            "Discount": Meteor.user().Cart[i].Discount,
                            "Name": this.Item.Name
                        });
                        winningObject["DidWin"] = true;

                    }
                }
            }
            return winningObject;
        }
    },
    ItemCompetition: function() {
        return ItemCompetitions.findOne({
            _id: this._id
        }, {
            fields: {
                "SlotCount": 1,
                "Item.Name": 1
            }
        })
    }
})
