Template.ItemCompetitionsListWatchList.onCreated(function() {
    this.SkipAmount = new ReactiveVar(0);
    var self = this;
    Tracker.autorun(function() {
        self.subscribe("ItemCompetitionUserWatchlist", self.SkipAmount.get());
    });
});
Template.ItemCompetitionsListWatchList.helpers({
    ItemCompetitions: function() {
        return ItemCompetitions.find({
            _id: {
                $in: Meteor.user().Watchlist
            },
            Cancelled: {
                $exists: false
            }
        }, {
            sort: {
                "StartTimeOfCompetition": 1
            }
        });

    },
    ItemCompetitiveLiveCountdown: function() {
        var diff = moment(this.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        var self = this;
        Meteor.setInterval(function() {
            var diff = moment(self.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    ItemCompetitiveNotLiveCountdown: function() {
        var diff = moment(this.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();

        Meteor.setInterval(function() {
            var diff = moment(self.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    IsOnUserWatchList: function(Competition_Id) {
        if (Meteor.users.findOne({
                _id: Meteor.userId(),
                'Watchlist': {
                    $in: [Competition_Id.hash.Competition_Id]
                }
            })) {
            return true;
        }
        return false;
    },
    Progress: function() {
        return 100 - ((this.Progress % 1) * 100);
    },
    ProgressText: function() {
        return Math.floor(this.Progress * 100) + "%";
    },
    NextButton: function() {
        if (ItemCompetitions.find().count() == 20) {
            return true
        }
        return false;
    },
    PreviousButton: function() {
        if (FlowRouter.getParam("Number") === undefined) {
            return false;
        }
        return true;
    },
    IsCompetitionLive: function(Competition) {
        if (Competition.hash.Competition.IsLive || Competition.hash.Competition.HasBeenWon) {
            return "";
        }
        return "NotLive";

    },
    IsUserEntered: function() {
        if (Meteor.user() && Meteor.user().CompetitionsEntered) {
            var self = this;
            var isEntered = _.some(Meteor.user().CompetitionsEntered, function(Competition) {
                return Competition == self._id
            });
            return isEntered;
        }

    },
    ItemNameSize: function(Name) {
        var fontSize = 950 / Name.hash.Name.length;
        if (fontSize > 20) {
            return "";
        }
        return fontSize + "px";
    },
});
Template.ItemCompetitionsListWatchList.events({
    'click .Watchlist-X-Remove': function(event, template) {
        Meteor.call("RemoveFromWatchlist", this._id);
    },
    'click .CompetitionButton': function(event, template) {
        FlowRouter.go('/ItemCompetition/' + this._id);
    },
    'click .NextPageButton': function(event, template) {
        if (FlowRouter.getParam("Number") === undefined) {
            template.SkipAmount.set(20);
            FlowRouter.go("/Watchlist/Page/2");
            window.scrollTo(0, 0);

        } else {
            var page = parseInt(FlowRouter.getParam("Number")) + 1;
            template.SkipAmount.set(page * 20);
            FlowRouter.go("/Watchlist/Page/" + page);
            window.scrollTo(0, 0);

        }
    },
    'click .PreviousPageButton': function(event, template) {
        if (Number(FlowRouter.getParam("Number")) === 2) {
            template.SkipAmount.set(0);
            FlowRouter.go("/Watchlist");
            window.scrollTo(0, 0);
        } else {
            var page = parseInt(FlowRouter.getParam("Number")) - 1;
            template.SkipAmount.set(page * 20);
            FlowRouter.go("/Watchlist/Page/" + page);
            window.scrollTo(0, 0);

        }
    },
    'click .EnterCompetitionButton': function(event, template) {
        Session.set("CompetitionEntrantInformation", this);
        $('[data-remodal-id=ItemCompetitionEntrantModal]').remodal({
            "closeOnConfirm": false,
            modifier: 'EnterCompetitionModal'
        }).open();
    }
});
