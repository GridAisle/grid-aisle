Template.Blog.onCreated(function() {
    this.subscribe("Blog");
});
Template.Blog.helpers({
    Blogs: function() {
        return Blog.find({}, {
            sort: {
                "Date": 1
            }
        });
    },
    DateFormat: function(Date) {
        return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }
});
