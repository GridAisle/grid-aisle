Template.Profile.onCreated(function() {
    this.subscribe("UserProfileProfile");

    this.DefaultPaymentMethod = new ReactiveVar();
    this.AlternativePaymentMethods = new ReactiveVar();
    Meteor.call("RetrievePaymentMethods", function(error, result) {
        Session.set("DefaultPaymentMethod", result[0]);
        result.splice(0, 1);
        Session.set("AlternativePaymentMethods", result);
    });

});
Template.Profile.onRendered(function() {
    $(document).on('closing', '.ProfileAddPaymentMethod', function() {
        Meteor.call("RetrievePaymentMethods", function(error, result) {
            Session.set("DefaultPaymentMethod", result[0]);
            result.splice(0, 1);
            Session.set("AlternativePaymentMethods", result);
        });
    });
    $(document).on('closed', '.AddNewAddressModal', function() {
        document.getElementById("ShippingInformationForm").reset();
    });
    $(document).on('closed', '.ProfileAddPaymentMethod', function() {
        document.getElementById("AddPaymentMethodForm").reset();
    });



})
Template.Profile.helpers({
    DefaultPaymentMethod: function() {
        return Session.get("DefaultPaymentMethod");
    },
    AlternativePaymentMethods: function() {
        if (Session.get("AlternativePaymentMethods").length >= 1) {
            return Session.get("AlternativePaymentMethods");
        }
    },
    Addresses: function() {
        if (Meteor.user()) {
            return Meteor.user().Addresses;
        }
    },
    AddressesLength: function() {
        if (Meteor.user()) {
            if (Meteor.user().Addresses) {
                return Meteor.user().Addresses.length;
            }
        }
    },
    DateJoined:function(Date){
      return moment(Date).format('MMMM M, YYYY');
    }
});
Template.Profile.events({
    'click .AddNewAddressButton': function() {
        $('[data-remodal-id=AddNewAddressModal]').remodal({
            closeOnConfirm: false,
            modifier: 'AddNewAddressModal'
        }).open();
    },
    'click .AddNewPaymentMethodModalButton': function() {
        $('[data-remodal-id=AddNewPaymentMethodModal]').remodal({
            closeOnConfirm: false,
            modifier: 'AddNewPaymentMethodModal'
        }).open();
    },
    'click .AddNewPaymentMethodButtonBig': function() {
        $('[data-remodal-id=AddNewPaymentMethodModal]').remodal({
            closeOnConfirm: false,
            modifier: 'AddNewPaymentMethodModal'
        }).open();
    },
    'click .AddNewAddressButtonBig': function() {
        $('[data-remodal-id=AddNewAddressModal]').remodal({
            closeOnConfirm: false,
            modifier: 'AddNewAddressModal'
        }).open();
    },
    'click .AddNewAddress': function() {
        Meteor.call("AddShippingInformation", function(error, result) {

        });
    },
    'click .PaymentDropdownBoxSetDefaultLink': function(event, template) {
        Meteor.call("ChangeDefaultPayment", Session.get("AlternativePaymentMethods")[event.currentTarget.id], function(error, result) {
            if (result) {
                Session.set("DefaultPaymentMethod", result[0]);
                result.splice(0, 1);
                Session.set("AlternativePaymentMethods", result);
            }
        });
        Dropdowns.hide("DefaultPaymentDropdown");
    },
    'click .AddressDropdownBoxSetDefaultLink': function(event, template) {
        Meteor.call("ChangeDefaultAddress", this._id, function(error, result) {
        });
        Dropdowns.hide("DefaultAddressDropdown");
    },
    'click .Remove-Address': function(event, template) {
        Meteor.call("RemoveAddress", this._id);
    },
    'click .Remove-PaymentMethod': function(event, template) {
        Meteor.call("RemovePaymentMethod", this, function(error, result) {
            if (result) {
                Session.set("DefaultPaymentMethod", result[0]);
                result.splice(0, 1);
                Session.set("AlternativePaymentMethods", result);
            }
        });
    },
    'click .FastSlots': function(event, template) {
        Meteor.call("FastSlots");
    },
    'click .NotificationEmails': function(event, template) {
        Meteor.call("NotificationEmails");
    },
    'click .CountdownSound': function(event, template) {
        Meteor.call("CountdownSound");
    }
});
