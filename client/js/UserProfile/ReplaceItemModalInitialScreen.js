Template.ReplaceItemModalInitialScreen.onCreated(function() {

});

Template.ReplaceItemModalInitialScreen.onRendered(function() {})

Template.ReplaceItemModalInitialScreen.helpers({
    ReplaceOrderInformation: function() {
        if (Session.get("ReplaceOrderInformation")) {
            return Session.get("ReplaceOrderInformation").Orders;
        }

    },
    OrderId: function() {
        return Session.get("ReplaceOrderInformation").OrderId;
    },
    Quantity: function() {
        return this.Quantity;
    },
    IsDecreaseDisabled: function(currentQuantity) {
        if (parseInt(currentQuantity.hash.currentQuantity) <= 1) {
            return "disabled";
        }
    },
    ReplacementStatus: function() {
        if (Session.get("ReplaceOrderInformation") && Meteor.user()) {
            var self = this;
            return _.filter(Meteor.user().ReplacementsRefunds, function(ReplacementRefund) {
                return (ReplacementRefund.OrderId == self.OrderId && ReplacementRefund.ItemId == self.Item._id && !ReplacementRefund.HasBeenRefundedReplaced);
            })[0];
        }
    },
});
Template.ReplaceItemModalInitialScreen.events({
    'click .DecreaseQuantity': function(event, template) {
        event.preventDefault();
        var currentQuantity = parseInt($("#" + this.Item._id + " .QuantityValue").val());
        if (currentQuantity > 1) {
            currentQuantity -= 1;
            $("#" + this.Item._id + " .QuantityValue").val(currentQuantity);
        }
        if (currentQuantity < this.Quantity) {
            $("#" + this.Item._id + ' .IncreaseQuantity').prop('disabled', false);
        }
        if (currentQuantity <= 1) {
            $("#" + this.Item._id + ' .DecreaseQuantity').prop('disabled', true);
        }
    },
    'click .IncreaseQuantity': function(event, template) {
        event.preventDefault();
        var currentQuantity = parseInt($("#" + this.Item._id + " .QuantityValue").val());
        if (currentQuantity < this.Quantity) {
            currentQuantity += 1;
            $("#" + this.Item._id + " .QuantityValue").val(currentQuantity);
        }
        if (currentQuantity > 1) {
            $("#" + this.Item._id + ' .DecreaseQuantity').prop('disabled', false);
        }
        if (currentQuantity >= this.Quantity) {
            $("#" + this.Item._id + ' .IncreaseQuantity').prop('disabled', true);
        }
    },
})
