Template.Quests.onCreated(function() {
    this.subscribe("UserProfileQuests");
    this.TwitterQuestTemplate = new ReactiveVar();
    this.FacebookQuestTemplate = new ReactiveVar();
    var self = this;
    Meteor.call("QuestStatus", function(error, result) {
        if (error) {

        } else {
            if (result.TwitterQuest.Active) {
                Session.set("TwitterQuestTemplate", "TwitterQuestActive")
                self.TwitterQuestTemplate.set("TwitterQuestActive");
            } else {
                Session.set("TwitterQuestTemplate", "TwitterQuestInactive")
            }
            if (result.FacebookQuest.Active) {
                if (result.FacebookQuest.Submitted) {
                    Session.set("FacebookQuestTemplate", "FacebookQuestSuccess")
                } else {
                    Session.set("FacebookQuestTemplate", "FacebookQuestActive")
                }

            } else {
                Session.set("FacebookQuestTemplate", "FacebookQuestInactive")
            }
        }
    });

});
Template.Quests.onRendered(function() {
    var clipboard = new Clipboard('.ReferralCopyButton');
})
Template.Quests.helpers({
    Quests: function() {
        return Meteor.users.findOne({
            _id: Meteor.userId()
        }, {
            fields: {
                "Quests": 1
            }
        })
    },

    TwitterQuestTemplate: function() {
        return Session.get("TwitterQuestTemplate");

    },
    FacebookQuestTemplate: function() {
        return Session.get("FacebookQuestTemplate");

    }
});
Template.Quests.events({

})
Template.TwitterQuestActive.events({
    'submit #TwitterQuestForm': function(event, template) {
        event.preventDefault();
        document.getElementById("TwitterSubmitButton").disabled = true;
        Meteor.call("TwitterQuest", event.target.TwitterUsername.value, function(error, result) {
            if (error) {
                template.$('#error').html(error.reason);
                document.getElementById("TwitterSubmitButton").disabled = false;
            } else {
                Session.set("TwitterTokenQuestRewardAmount", result);
                Session.set("TwitterQuestTemplate", "TwitterQuestSuccess");
            }


        })
    }
})
Template.FacebookQuestActive.events({
    'submit #FacebookQuestForm': function(event, template) {
        event.preventDefault();
        if (event.target.FacebookUsername.value) {
            Meteor.call("FacebookQuest", event.target.FacebookUsername.value, function(error, result) {
                if (result) {
                    Session.set("FacebookQuestTemplate", "FacebookQuestSuccess");
                }
                if (error) {
                    template.$('#error').html(error.reason);
                }


            })
        }
    }
})
Template.TwitterQuestSuccess.onCreated(function() {
    Meteor.setTimeout(function() {
        Session.set("TwitterQuestTemplate", "TwitterQuestInactive");
    }, 10000);
})
Template.TwitterQuestSuccess.helpers({
    TwitterTokenQuestRewardAmount: function() {
        return Session.get("TwitterTokenQuestRewardAmount");
    }
})
Template.FacebookQuestInactive.helpers({
    Quests: function() {
        return Meteor.users.findOne({
            _id: Meteor.userId()
        }, {
            fields: {
                "Quests": 1
            }
        })
    },
    QuestRenewalDate: function(Date) {
        var diff = moment(Date.hash.Date).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        Meteor.setInterval(function() {
            var diff = moment(Date.hash.Date).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
})
Template.TwitterQuestInactive.helpers({
    Quests: function() {
        return Meteor.users.findOne({
            _id: Meteor.userId()
        }, {
            fields: {
                "Quests": 1
            }
        })
    },
    QuestRenewalDate: function(Date) {
        var diff = moment(Date.hash.Date).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        Meteor.setInterval(function() {
            var diff = moment(Date.hash.Date).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
})
