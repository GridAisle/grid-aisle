Template.OrderHistory.onCreated(function() {
    PostSubs.subscribe("UserProfileOrders");
});
Template.OrderHistory.helpers({
    OrderHistoryInformation: function() {
        if (Meteor.user() && Meteor.user().Orders) {
            var x = _.groupBy(Meteor.user().Orders.reverse(), 'OrderId');
            return _.map(x, function(Order, key) {
                var object = {};
                object["Orders"] = Order;
                object["OrderId"] = key;
                object["ShippingInformation"] = Order[0].ShippingInformation;
                object["ShippingStatus"] = Order[0].ShippingStatus;
                object["Cancelable"] = Order[0].Cancelable;
                object["HasBeenCompleted"] = Order[0].HasBeenCompleted;
                object["OrderPlacedDate"] = Order[0].OrderPlacedDate;
                if (Order[0].TrackingUrl) {
                    object["TrackingUrl"] = Order[0].TrackingUrl;
                } else {
                    object["TrackingUrl"] = null;
                }
                return object;
            });

            // return _.groupBy(Meteor.user().Orders.reverse(), 'OrderId');
        }
    },
    OrderPlacedDateFormat: function(Date) {
        return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    },
    Total: function() {
        orderTotal = 0;
        for (var i = 0; i < this.Orders.length; i++) {
            orderTotal += this.Orders[i].Item.SellingPrice * this.Orders[i].Quantity;
        }
        return orderTotal;
    },
    HasPendingReplacement: function() {
        var self = this;
        return _.filter(Meteor.user().Replacements, function(Replacement) {
            return (Replacement.ReplacementOrderId == Template.parentData()._id && Replacement.ReplacementItemId == self._id);
        });
    }
});
Template.OrderHistory.events({
    'click .ReplaceItemButton': function(event, template) {
        Session.set("ReplaceOrderInformation", this);
        $('[data-remodal-id=ReplaceItemModal]').remodal().open();
    },
    'click .CancelItemButton': function(event, template) {
        Meteor.call("CancelItem", this, function(error, result) {

        });
    },
    'click .TrackOrderButton': function(event, template) {
        window.open(this.TrackingUrl, '_blank');
    }

});
