Template.ProfileAddPaymentMethod.onRendered(function() {
    $('input.cc-num').payment('formatCardNumber');
    $('input.cc-exp').payment('formatCardExpiry');
    $('input.cc-cvc').payment('formatCardCVC');
    $('[data-numeric]').payment('restrictNumeric');
});
Template.ProfileAddPaymentMethod.events({
    'submit form': function(event, template) {
        event.preventDefault();
        document.getElementById("AddPaymentSubmitButton").disabled = true;
        ccNum = $('.cc-num').val();
        cvc = $('.cc-cvc').val();
        expMo = $('.cc-exp').val().substring(0, 2);
        expYr = $('.cc-exp').val().split("/ ").pop();
        name = $('.cc-name').val();
        zipcode = $('.cc-zip').val();
        Stripe.card.createToken({
            name: name,
            number: ccNum,
            cvc: cvc,
            exp_month: expMo,
            exp_year: expYr,
            address_zip: zipcode
        }, function(status, response) {
            console.log(response);
            if (response.error) {
                template.$('#error').html(response.error.message);
                document.getElementById("AddPaymentSubmitButton").disabled = false;
            } else {
                Meteor.call('AddPaymentMethod', response, function(error, result) {
                    if (error) {
                        template.$('#error').html(error.reason);
                        document.getElementById("AddPaymentSubmitButton").disabled = false;
                    } else {
                        var modal = $('[data-remodal-id=AddNewPaymentMethodModal]').remodal();
                        modal.close();
                    }

                });
            }
        });
    }
});
