Template.UserProfile.onCreated(function(){



})
Template.UserProfile.helpers({
  Tab:function(){
    return FlowRouter.getRouteName();
  }
});
Template.UserProfile.events({
    'click .ProfileTab': function() {
        FlowRouter.go('/UserProfile/Profile');
    },
    'click .OrderHistoryTab': function() {
        FlowRouter.go('/UserProfile/OrderHistory');
    },
    'click .QuestsTab': function() {
        FlowRouter.go('/UserProfile/Quests');
    }

});
