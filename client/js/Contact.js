Template.Contact.onCreated(function() {
    this.subscribe("Contact");
});
Template.Contact.helpers({
    UserTickets: function() {
      if(Meteor.user()){
        var Tickets = Meteor.users.findOne({_id:Meteor.userId()});
        return _.sortBy(Tickets.Tickets,function(Ticket){
          return -Ticket.createdAt;
        })
      }
    },
    DateFormat:function(Date){
      return moment(Date.hash.Date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    }
});
Template.Contact.events({
    'submit .TicketForm': function(event, template) {
        event.preventDefault();
        Meteor.call("CreateTicket", event.target.TicketText.value, function(error, result) {
            event.target.TicketText.value = "";
        });
    }
});
