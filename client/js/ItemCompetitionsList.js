Template.ItemCompetitionsList.onCreated(function() {
    this.ItemCompetitionGrid = new ReactiveVar(null);
    this.ItemCompetitionGridData = new ReactiveVar(null);
    this.SkipAmount = new ReactiveVar(0);
    // Enable infinite scrolling on this template
    // this.infiniteScroll({
    //     perPage: 1, // How many results to load "per page"
    //     query: {}, // The query to use as the selector in our collection.find() query
    //
    //     subManager: new SubsManager(), // (optional, experimental) A meteorhacks:subs-manager to set the subscription on
    //     // Useful when you want the data to persist after this template
    //     // is destroyed.
    //     collection: 'ItemCompetitions', // The name of the collection to use for counting results
    //     publication: 'ItemCompetitions' // (optional) The name of the publication to subscribe.
    //         // Defaults to {collection}Infinite
    // });
    var self = this;
    Tracker.autorun(function() {
        self.subscribe("ItemCompetitions", self.SkipAmount.get());
    });

});
Template.ItemCompetitionsList.onRendered(function() {

});
Template.ItemCompetitionsList.helpers({
    ItemCompetitions: function() {
        return ItemCompetitions.find({
            Cancelled: {
                $exists: false
            }
        }, {
            sort: {
                StartTimeOfCompetition: 1
            },
        });
    },
    OrderPlaced: function() {
        return Session.get("OrderConfirmationData");
    },
    ItemCompetitiveLiveCountdown: function() {
        var diff = moment(this.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();
        var self = this;
        Meteor.setInterval(function() {
            var diff = moment(self.EndTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    ItemCompetitiveNotLiveCountdown: function() {
        var diff = moment(this.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
        var _dep = new Deps.Dependency();
        _dep.depend();

        Meteor.setInterval(function() {
            var diff = moment(self.StartTimeOfCompetition).valueOf() - moment().utc().valueOf();
            _dep.changed();
        }, 1000)

        return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
            forceLength: true,
            trim: false
        });
    },
    Progress: function() {
        return 100 - ((this.Progress % 1) * 100);
    },
    ProgressText: function() {
        return Math.floor(this.Progress * 100) + "%";
    },
    IsOnUserWatchList: function(Competition_Id) {
        if (Meteor.users.findOne({
                _id: Meteor.userId(),
                'Watchlist': {
                    $in: [Competition_Id.hash.Competition_Id]
                }
            })) {
            return true;
        }
        return false;

    },
    IsCompetitionLive: function(Competition) {
        if (Competition.hash.Competition.IsLive || Competition.hash.Competition.HasBeenWon) {
            return "";
        }
        return "NotLive";

    },
    NextButton: function() {
        if (ItemCompetitions.find().count() == 20) {
            return true
        }
        return false;
    },
    PreviousButton: function() {
        if (FlowRouter.getParam("Number") === undefined) {
            return false;
        }
        return true;
    },
    IsUserEntered: function() {
        if (Meteor.user() && Meteor.user().CompetitionsEntered) {
            var self = this;
            var isEntered = _.some(Meteor.user().CompetitionsEntered, function(Competition) {
                return Competition == self._id
            });
            return isEntered;
        }

    },
    ItemNameSize: function(Name) {
        var fontSize = 950 / Name.hash.Name.length;
        if (fontSize > 20) {
            return "";
        }
        return fontSize + "px";
    },
});
Template.ItemCompetitionsList.events({
    'click .CompetitionButton': function(event, template) {
        FlowRouter.go('/ItemCompetition/' + this._id);
    },
    'click .Watchlist-Star-Add': function(event, template) {
        Meteor.call("AddToWatchlist", this._id);
    },
    'click .Watchlist-Star-Remove': function(event, template) {
        Meteor.call("RemoveFromWatchlist", this._id);
    },
    'click .NextPageButton': function(event, template) {
        if (FlowRouter.getParam("Number") === undefined) {
            template.SkipAmount.set(20);
            FlowRouter.go("/Page/2");
            window.scrollTo(0, 0);

        } else {
            var page = parseInt(FlowRouter.getParam("Number")) + 1;
            template.SkipAmount.set(page * 20);
            FlowRouter.go("/Page/" + page);
            window.scrollTo(0, 0);

        }
    },
    'click .PreviousPageButton': function(event, template) {
        if (Number(FlowRouter.getParam("Number")) === 2) {
            template.SkipAmount.set(0);
            FlowRouter.go("/");
            window.scrollTo(0, 0);
        } else {
            var page = parseInt(FlowRouter.getParam("Number")) - 1;
            template.SkipAmount.set(page * 20);
            FlowRouter.go("/Page/" + page);
            window.scrollTo(0, 0);

        }
    },
    'click .EnterCompetitionButton': function(event, template) {
        Session.set("CompetitionEntrantInformation", this);
        $('[data-remodal-id=ItemCompetitionEntrantModal]').remodal({
            "closeOnConfirm": false,
            modifier: 'EnterCompetitionModal'
        }).open();
    }

});
