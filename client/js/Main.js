Template.Main.onCreated(function() {
    // this.subscribe("Main");
    this.CartAmount = new ReactiveVar();
    var self = this;
    if (Meteor.user()) {
        Meteor.call("UserCartAmount", function(error, result) {
            if (result) {
                self.CartAmount.set(result);
            }
        })
    }

});

Template.Main.onRendered(function() {

});

Template.Main.helpers({
    // UsersOnline: function() {
    //     return Meteor.users.find({
    //         "status.online": true
    //     }).count();
    // },
    Competition: function() {
        if (FlowRouter.getParam("ItemCompetitionId")) {
            return true
        }
        return false;
    },
    CartAmount: function() {
        if (Meteor.user() && Meteor.user().Cart) {
            if (Meteor.user().Cart.length == 0) {
                return "";
            }
            Template.instance().CartAmount.set(Meteor.user().Cart.length);
            return Template.instance().CartAmount.get();
        }
        return Template.instance().CartAmount.get();
    }
})
Template.Main.events({
    'click .LogoutButton' () {
        AccountsTemplates.logout();
    },
    'click .brand' () {
        if (FlowRouter.getParam("Number")) {
            // BlazeLayout.reset();
            FlowRouter.go("/");
        } else {
            FlowRouter.go("/");
        }


    }
});
