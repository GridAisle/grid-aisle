Template.Cart.onCreated(function() {
    PostSubs.subscribe("UserProfileCart");
});
Template.Cart.helpers({
    CartInformation: function() {
        if (Meteor.user()) {
            return Meteor.user().Cart;
        }
    },
    Subtotal: function() {
        if (Meteor.user()) {
            var Subtotal = 0;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].CartOrReserve) {
                    Subtotal += (Meteor.user().Cart[i].Item.SellingPrice - Meteor.user().Cart[i].Discount) * Meteor.user().Cart[i].Quantity;
                }
            }
            return Subtotal;
        }
    },
    MerchandiseCount: function() {
        if (Meteor.user()) {
            var count = 0;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].CartOrReserve) {
                    count += 1;
                }
            }
            return count;
        }
    },
    IsWonCompetition: function(IsItemWonFromCompetition) {
        if (IsItemWonFromCompetition.hash.IsItemWonFromCompetition) {
            return "WonItem";
        }
        return "CartItem";
    },
    CheckoutButtonDisabled: function() {
        if (Meteor.user()) {
            var count = 0;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].CartOrReserve) {
                    count += 1;
                }
            }
            if (count > 0) {
                return false;
            }
            return true;
        }
    },
    ItemTotal: function() {
        return this.Item.SellingPrice - this.Discount;
    },
    WonFromCompetitionItemCountdown: function(CompetitionTimer) {
      var diff = CompetitionTimer.hash.CompetitionTimer - moment();
      var _dep = new Deps.Dependency();
      _dep.depend();

      Meteor.setInterval(function() {
          var diff = CompetitionTimer.hash.CompetitionTimer - moment();
          _dep.changed();
      }, 1000)

      return moment.duration(diff, "milliseconds").format("HH:mm:ss", {
          forceLength: true,
          trim: false
      });
}
});
Template.Cart.events({
    'click .ProceedToCheckoutButton' () {
        Meteor.call("CheckoutLocation", function(error, result) {
            Session.set("CheckoutBreadcrumb", result.CheckoutSession);
            FlowRouter.go(result.Url);
        });
    },
    'click .RemoveFromCart' () {
        Meteor.call('RemoveFromCart', this._id);
    },
    'click .DecreaseQuantity' (event, template) {
        if (this.Quantity > 1) {
            Meteor.call("ChangeCartQuantity", this._id, this.Quantity - 1);
        }
    },
    'click .IncreaseQuantity' (event, template) {
        Meteor.call("ChangeCartQuantity", this._id, this.Quantity + 1);
    },
    'click .MoveToReserve': function(event, template) {
        Meteor.call("MoveCartItemToReserve", this._id);
    },
    'click .MoveToCart': function(event, template) {
        Meteor.call("MoveCartItemToCart", this._id);
    }
});
