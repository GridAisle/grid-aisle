var myLogoutFunc = function() {
    FlowRouter.go('/');
}
AccountsTemplates.configure({
    sendVerificationEmail: true,
    privacyUrl: '/Privacy',
    termsUrl: '/Terms',
    showPlaceholders: false,
    showForgotPasswordLink: true,
    homeRoutePath: '/',
    defaultLayout: 'Account',
    defaultLayoutRegions: {},
    defaultContentRegion: 'content',
    enablePasswordChange: true,
    continuousValidation: false,
    negativeFeedback: false,
    negativeValidation: true,
    positiveValidation: true,
    positiveFeedback: true,
    showValidating: true,
    showResendVerificationEmailLink: true,
    onLogoutHook: myLogoutFunc
});
AccountsTemplates.configure({
    texts: {
        button: {
            signIn: "Log In"
        },
        signInLink_link: "Log In",
    }
});

var email = AccountsTemplates.removeField('email');
var password = AccountsTemplates.removeField('password');

AccountsTemplates.addFields([{
    _id: 'FirstName',
    type: 'text',
    displayName: 'First Name',
    required: true
}, {
    _id: 'LastName',
    type: 'text',
    displayName: 'Last Name',
    required: true
},{
  _id:'Reg_Code',
  type:'hidden',
  required:false,
}
]);
AccountsTemplates.addField(email);
AccountsTemplates.addField(password);

AccountsTemplates.configureRoute('signIn', {
    path: '/Login'
});
AccountsTemplates.configureRoute('signUp', {
    path: '/Register*'
});
AccountsTemplates.configureRoute('resetPwd', {
    path: '/ResetPassword'
});
AccountsTemplates.configureRoute('forgotPwd', {
    path: '/ForgotPassword'
});
AccountsTemplates.configureRoute('changePwd', {
    path: '/ChangePassword'
});
AccountsTemplates.configureRoute('verifyEmail', {
    path: '/Verify-Email'
});
AccountsTemplates.configureRoute('resendVerificationEmail', {
    path: '/Resend-Verification-Email'
});
