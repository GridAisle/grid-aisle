Meteor.startup(function() {
    SimpleSchema.debug = true;
});
AutoForm.hooks({
    InsertItemCompetition: {
        before: {
            method: function(doc) {
                var ItemForCompetition = Items.findOne({
                    _id: doc.Item
                });
                doc.Item = ItemForCompetition;
                return doc;
            }
        }
    },
    ShippingInformationForm: {
        before: {
            method: function(doc) {
                doc._id = new Meteor.Collection.ObjectID().valueOf();
                return doc
            }
        },
        onSuccess: function(method, result) {
            if (FlowRouter.getRouteName() == "Profile") {
                var inst = $('[data-remodal-id=AddNewAddressModal]').remodal();
                inst.close();
            }
            else if(FlowRouter.getRouteName()=="Order"){
              var AlternativeShippingAddresses = Session.get("AlternativeShippingAddresses");
              AlternativeShippingAddresses.push(Session.get("DesiredShippingAddress"));
              Session.set("AlternativeShippingAddresses",AlternativeShippingAddresses);
              Session.set("DesiredShippingAddress",result);
              var inst = $('[data-remodal-id=AddNewShippingInformationModal]').remodal();
              inst.close();
            }
            else{
              FlowRouter.go("/Checkout/Order");
            }
        }
    }

});
