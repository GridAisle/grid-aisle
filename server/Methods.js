import {
    Meteor
} from 'meteor/meteor';
import {
    DDPRateLimiter
} from 'meteor/ddp-rate-limiter';
import {
    _
} from 'meteor/underscore';
Meteor.startup(function() {
    const LISTS_METHODS = _.pluck([
        'UserCompetitionSlots',
        'CheckoutLocation',
        'AddToWatchlist',
        'FastSlots',
        'RemoveFromWatchlist',
        'ReplaceItemRequest',
        'ChangeDefaultAddress',
        'ChangeDefaultPayment',
        'AddToCart',
        'ChangeCartQuantity',
        'MoveCartItemToReserve',
        'MoveCartItemToCart',
        'RemoveFromCart',
        'AddShippingInformation',
        'CancelItem',
        'PlaceOrder',
        'AddPaymentMethod',
        'RemoveAddress',
        'RemovePaymentMethod',
        'ChargePaymentMethodBuyTokensNew',
        'ChargePaymentMethodBuyTokensSaved',
        'RetrievePaymentMethods',
        'CreateTicket',
        'QuestStatus',
        'FacebookQuest',
        'TwitterQuest',
        'PlaceSlotForItemCompetition',
        'UserCartAmount',
        'CompetitionPurchaseEntrance'
    ], 'UserMethods');

});
if (Meteor.isServer) {
    // Only allow 5 list operations per connection per second
    DDPRateLimiter.addRule({
        UserMethods(UserMethods) {
            return _.contains(LISTS_METHODS, UserMethods);
        },

        // Rate limit per connection ID
        connectionId() {
            return true;
        }
    }, 5, 1000);
}
Meteor.methods({
    UserCartAmount: function() {
        if (Meteor.userId()) {
            return Meteor.user().Cart.length;
        }
    },
    UserCompetitionSlots: function(CompetitionId) {
        try {
            if (this.userId) {
                var pipeline = [{
                    $match: {
                        _id: this.userId,
                        "CompetitionSlots.CompetitionId": CompetitionId
                    }
                }, {
                    $project: {
                        CompetitionSlots: {
                            $filter: {
                                input: "$CompetitionSlots",
                                as: "slot",
                                cond: {
                                    $eq: ["$$slot.CompetitionId", CompetitionId]
                                }
                            }
                        }
                    }
                }]
                var result = Meteor.users.aggregate(pipeline);
                return result[0].CompetitionSlots;
            }
        } catch (e) {

        }
    },
    CheckoutLocation: function() {
        var checkoutLocationObject = {};
        if (Meteor.userId()) {

            var OrderTotal = 0;
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                OrderTotal += Meteor.user().Cart[i].Item.SellingPrice * Meteor.user().Cart[i].Quantity;
            }
            if (OrderTotal > Meteor.user().profile.Tokens) {
                if (Meteor.user().Addresses.length > 0) {
                    checkoutLocationObject["Url"] = "/Checkout/PaymentInformation";
                    checkoutLocationObject["CheckoutSession"] = "PaymentInformationHasShipping";
                    checkoutLocationObject["PaymentInformationCost"] = OrderTotal - Meteor.user().profile.Tokens > 150 ? OrderTotal - Meteor.user().profile.Tokens : 150;
                    return checkoutLocationObject;
                }
                checkoutLocationObject["Url"] = "/Checkout/PaymentInformation";
                checkoutLocationObject["CheckoutSession"] = "PaymentInformation";
                checkoutLocationObject["PaymentInformationCost"] = OrderTotal - Meteor.user().profile.Tokens > 150 ? OrderTotal - Meteor.user().profile.Tokens : 150;
                return checkoutLocationObject;
            } else if (Meteor.user().Addresses.length > 0) {
                checkoutLocationObject["Url"] = "/Checkout/Order";
                checkoutLocationObject["CheckoutSession"] = "Order";
                return checkoutLocationObject;
            } else {
                checkoutLocationObject["Url"] = "/Checkout/ShippingInformation";
                checkoutLocationObject["CheckoutSession"] = "ShippingInformation";
                return checkoutLocationObject;
            }
        } else {
            checkoutLocationObject["Url"] = "/";
            return checkoutLocationObject;
        }
    },
    AddToWatchlist: function(CompetitionId) {
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $addToSet: {
                    "Watchlist": CompetitionId
                }
            });
        }
    },

    FastSlots: function() {
        if (this.userId) {
            Meteor.users.update({
                _id: this.userId
            }, {
                $set: {
                    "CompetitionSettings.FastSlots": !Meteor.user().CompetitionSettings.FastSlots
                }
            });
        }
    },
    RemoveFromWatchlist: function(CompetitionId) {
        if (this.userId) {
            convertedId = CompetitionId.toString();
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $pull: {
                    "Watchlist": convertedId
                }
            });
        }
    },
    ReplaceItemRequest: function(Request) {
        if (this.userId) {
            Future = Npm.require('fibers/future');
            var myFuture = new Future();
            ReplacementRefundSchema.clean(Request);
            if (Request) {
                Quantity = parseInt(Request.Quantity);
                Meteor.users.update({
                        _id: Meteor.userId(),
                        Orders: {
                            $elemMatch: {
                                OrderId: Request.OrderId,
                                "Item._id": Request.ItemId,
                                "Item.SellingPrice": Request.Price,
                                OrderPlacedDate: {
                                    $gte: moment().subtract(32, 'days').toDate()
                                },
                                Quantity: {
                                    $gte: Request.Quantity
                                }
                            }
                        },
                        ReplacementsRefunds: {
                            $not: {
                                $elemMatch: {
                                    OrderId: Request.OrderId,
                                    ItemId: Request.ItemId,
                                    Quantity: Request.Quantity,
                                    HasBeenRefundedReplaced: false
                                }
                            }
                        }
                    }, {
                        $push: {
                            ReplacementsRefunds: Request

                        }
                    },
                    function(error, result) {
                        if (result == 1) {
                            var item = Items.findOne({
                                _id: Request.ItemId
                            }, {
                                fields: {
                                    "Main_Image": 1,
                                    "Name": 1
                                }
                            });
                            if (Request.RefundOrReplace) {
                                SSR.compileTemplate('OrderRefundConfirmationEmail', Assets.getText('OrderRefundConfirmationEmail.html'));
                                var emailData = {
                                    FirstName: Meteor.user().profile.FirstName,
                                    LastName: Meteor.user().profile.LastName,
                                    Item: item,
                                    Quantity: Request.Quantity
                                };
                                Email.send({
                                    to: Meteor.user().emails[0].address,
                                    from: "support@spargain.com",
                                    subject: "Your Spargain Refund Request",
                                    html: SSR.render('OrderRefundConfirmationEmail', emailData)
                                });
                            } else {
                                SSR.compileTemplate('OrderReplacementConfirmationEmail', Assets.getText('OrderRefundConfirmationEmail.html'));
                                var emailData = {
                                    FirstName: Meteor.user().profile.FirstName,
                                    LastName: Meteor.user().profile.LastName,
                                    Item: item,
                                    Quantity: Request.Quantity
                                };
                                Email.send({
                                    to: Meteor.user().emails[0].address,
                                    from: "support@spargain.com",
                                    subject: "Your Spargain Replacement Request",
                                    html: SSR.render('OrderRefundConfirmationEmail', emailData)
                                });
                            }

                            myFuture.return("Success");

                        } else {
                            // console.log(error);
                            myFuture.return("Replacement Not Processed");
                        }
                    });


            } else {
                myFuture.return("Replacement Not Processed");
            }

            return myFuture.wait();
        }
    },
    ChangeDefaultAddress: function(DesiredDefaultAddressId) {
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId(),
                "Addresses.IsDefault": true
            }, {
                $set: {
                    "Addresses.$.IsDefault": false
                }
            });
            Meteor.users.update({
                _id: Meteor.userId(),
                "Addresses._id": DesiredDefaultAddressId
            }, {
                $set: {
                    "Addresses.$.IsDefault": true
                }
            });
            return Meteor.user().Addresses;
        }
    },
    ChangeDefaultPayment: function(DesiredDefaultPayment) {
        if (this.userId) {
            Stripe = StripeSync(Meteor.settings.private.stripe.testSecretKey);
            var updatedCustomer = Stripe.customers.update(Meteor.user().Customer, {
                default_source: DesiredDefaultPayment.id
            });
            return updatedCustomer.sources.data;
        }
    },
    AddToCart: function(Id, Quantity) {
        if (this.userId) {
            try {
                if (Quantity > 10) {
                    Quantity = 10;
                }
                var Item = Items.findOne({
                    _id: Id
                });
                if (Meteor.users.findOne({
                        _id: Meteor.userId(),
                        $and: [{
                            "Cart.WonFromCompetition": false,
                            "Cart.Item._id": Id
                        }]
                    })) {
                    Meteor.users.update({
                        _id: Meteor.userId(),
                        "Cart.WonFromCompetition": false,
                        "Cart.Item._id": Id
                    }, {
                        $set: {
                            "Cart.$.Quantity": Quantity
                        }
                    });
                } else {
                    Meteor.users.update({
                        _id: Meteor.userId()
                    }, {
                        $push: {
                            "Cart": {
                                _id: new Meteor.Collection.ObjectID().valueOf(),
                                Item,
                                Quantity,
                                WonFromCompetition: false,
                                Discount: 0,
                                CartOrReserve: true
                            }
                        }
                    });
                }
            } catch (e) {
                console.log(e);
            }
        }
    },
    ChangeCartQuantity: function(Id, Quantity) {
        if (this.userId) {
            if (Quantity > 10) {
                Quantity = 10;
            }
            if (Quantity >= 1) {
                Meteor.users.update({
                    _id: Meteor.userId(),
                    Cart: {
                        $elemMatch: {
                            _id: Id,
                            WonFromCompetition: false
                        }
                    }
                }, {
                    $set: {
                        "Cart.$.Quantity": Quantity
                    }
                });
            }
        }
    },
    MoveCartItemToReserve: function(CartItemId) {
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId(),
                "Cart._id": CartItemId
            }, {
                $set: {
                    "Cart.$.CartOrReserve": false
                }
            })
        }
    },
    MoveCartItemToCart: function(CartItemId) {
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId(),
                "Cart._id": CartItemId
            }, {
                $set: {
                    "Cart.$.CartOrReserve": true
                }
            })
        }
    },
    RemoveFromCart: function(Id) {
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $pull: {
                    "Cart": {
                        "_id": Id
                    }
                }
            });
        }
    },
    AddShippingInformation: function(ShippingInformation) {
        if (this.userId) {
            Future = Npm.require('fibers/future');
            var myFuture = new Future();
            ShippingInformation._id = Random.id();
            if (Meteor.user().Addresses.length > 0) {
                ShippingInformation.IsDefault = false;
            } else {
                ShippingInformation.IsDefault = true;
            }
            AddressSchema.clean(ShippingInformation);
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $push: {
                    "Addresses": {
                        "_id": ShippingInformation._id,
                        "FirstName": ShippingInformation.FirstName,
                        "LastName": ShippingInformation.LastName,
                        "Address": ShippingInformation.Address,
                        "ApartmentFloorUnit": ShippingInformation.ApartmentFloorUnit,
                        "City": ShippingInformation.City,
                        "State": ShippingInformation.State,
                        "Zipcode": ShippingInformation.Zipcode,
                        "PhoneNumber": ShippingInformation.PhoneNumber,
                        "IsDefault": ShippingInformation.IsDefault,
                    }
                }
            }, function(error, result) {
                if (result == 1) {
                    myFuture.return(ShippingInformation);
                }

            });
            return myFuture.wait();
        }
    },

    CancelItem: function(Order) {
        console.log(Order);
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId(),
                Orders: {
                    $elemMatch: {
                        _id: Order._id,
                        Cancelable: true,
                        "Item.SellingPrice": Order.Item.SellingPrice,
                        WonFromCompetition: false
                    }
                }
            }, {
                $pull: {
                    Orders: {
                        _id: Order._id
                    }
                },
                $inc: {
                    "profile.Tokens": Order.Item.SellingPrice
                }
            }, {
                multi: true
            })
        }
    },

    PlaceOrder: function(DesiredShippingAddress) {
        if (this.userId) {
            Future = Npm.require('fibers/future');
            var myFuture = new Future();
            var Order = {};
            var orderId = new Meteor.Collection.ObjectID()._str;
            var orderPlacedDate = new Date();
            var orderTotal = 0;
            var orderItems = [];
            for (var i = 0; i < Meteor.user().Cart.length; i++) {
                if (Meteor.user().Cart[i].CartOrReserve) {
                    orderTotal += (Meteor.user().Cart[i].Item.SellingPrice - Meteor.user().Cart[i].Discount) * Meteor.user().Cart[i].Quantity;
                    Order._id = new Meteor.Collection.ObjectID()._str;
                    Order.OrderId = orderId;
                    Order.HasBeenCompleted = false;
                    Order.Cancelable = true;
                    Order.ShippingStatus = "Processing";
                    Order.ShippingInformation = DesiredShippingAddress;
                    Order.OrderPlacedDate = orderPlacedDate;
                    Order.Item = Meteor.user().Cart[i].Item;
                    Order.Quantity = Meteor.user().Cart[i].Quantity;
                    Order.WinningCompetitionId = Meteor.user().Cart[i].WinningCompetitionId;
                    Order.WonFromCompetition = Meteor.user().Cart[i].WonFromCompetition;
                    Order.Discount = Meteor.user().Cart[i].Discount;
                    orderItems.push(Order);
                    Order = {};
                }
            }
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $push: {
                    "Orders": {
                        $each: orderItems
                    }
                },
                $pull: {
                    "Cart": {
                        "CartOrReserve": true
                    },
                },
                $inc: {
                    "profile.Tokens": -orderTotal
                }
            }, function(error, result) {
                if (result) {
                    var job = new Job(OrderData, 'MakeOrderNonCancelable', {
                        UserId: Meteor.userId(),
                        OrderId: orderId
                    }).delay(1800000).save();
                    SSR.compileTemplate('OrderSuccessEmailText', Assets.getText('OrderSuccessEmail.html'));
                    var emailData = {
                        FirstName: Meteor.user().profile.FirstName,
                        LastName: Meteor.user().profile.LastName,
                        ArrivalDate: moment().add(3, 'days').format('dddd, MMMM Do YYYY'),
                        OrderTotal: orderTotal,
                        ShippingInformation: orderItems[0].ShippingInformation,
                        Items: orderItems,
                    };
                    Email.send({
                        to: Meteor.user().emails[0].address,
                        from: "Spargain <support@spargain.com>",
                        subject: "Your Spargain Order",
                        html: SSR.render('OrderSuccessEmailText', emailData)
                    });
                    myFuture.return(orderItems);

                }
            });
            return myFuture.wait();
        }

    },
    AddPaymentMethod: function(Response) {
        if (this.userId) {
            if (Response.card.address_zip_check != "unchecked") {
                throw new Meteor.Error(500, "No Zipcode was provided");
            }
            Stripe = StripeSync(Meteor.settings.private.stripe.testSecretKey);
            if (Meteor.user().Customer == null) {
                var custCreate = Stripe.customers.create({
                    description: "Add card to account.",
                    source: Response.id
                });
                console.log(custCreate);
                if (custCreate.error) {
                    throw new Meteor.error(500, custCreate.error.message);
                } else {
                    Meteor.users.update(Meteor.userId(), {
                        $set: {
                            "Customer": custCreate.id
                        }
                    });
                    return custCreate.sources.data[0];
                }
            } else {
                var addCard = Stripe.customers.createSource(Meteor.user().Customer, {
                    source: Response.id
                });
                if (addCard.error) {
                    throw new Meteor.Error(500, "stripe-error", addCard.error.message);
                } else {
                    return addCard;
                }
            }
        }
    },
    RemoveAddress: function(DesiredRemovedAddressId) {
        if (this.userId) {
            Meteor.users.update({
                _id: Meteor.userId()
            }, {
                $pull: {
                    "Addresses": {
                        "_id": DesiredRemovedAddressId
                    }
                }
            });
        }
    },
    RemovePaymentMethod: function(Card) {
        if (this.userId) {
            console.log(Card.id);
            console.log(Meteor.user().Customer);
            Stripe = StripeSync(Meteor.settings.private.stripe.testSecretKey);
            Stripe.customers.deleteCard(Meteor.user().Customer, Card.id);
            if (Meteor.user().Customer != null) {
                var cardList = Stripe.customers.listCards(Meteor.user().Customer);
                return cardList.data;
            } else {
                return [];
            }
        }
    },
    ChargePaymentMethodBuyTokensNew: function(TokenCount, Response) {
        try {
            if (this.userId) {
                if (isNaN(TokenCount) || TokenCount < 150 || TokenCount > 10000) {
                    throw new Meteor.error(500, "Token amount is invalid");
                }
                if (Response.card.address_zip_check != "unchecked") {
                    throw new Meteor.Error(500, "No Zipcode was provided");
                }
                TokenCount = Math.floor(TokenCount);
                var Stripe = StripeSync(Meteor.settings.private.stripe.testSecretKey);
                if (Meteor.user().Customer == null) {
                    var custCreate = Stripe.customers.create({
                        description: "Add card to account.",
                        source: Response.id
                    });
                    if (custCreate.error) {
                        throw new Meteor.error(500, "stripe-error", custCreate.error.message);
                    } else {
                        var tokenCharge = Stripe.charges.create({
                            amount: TokenCount * 10,
                            currency: "usd",
                            source: custCreate.sources.data[0].id,
                            description: "Bought " + TokenCount + " tokens.",
                            customer: custCreate.id
                        });
                        var ReferralTokenAmountPreFree = TokenCount;
                        TokenCount += Math.floor((TokenCount - 150) / 50);
                        if (tokenCharge.paid) {
                            if (Meteor.user().Referral && Meteor.user().Referral.Referrer) {
                                Meteor.users.update({
                                    _id: Meteor.userId()
                                }, {
                                    $inc: {
                                        "profile.Tokens": TokenCount,
                                        "Referral.TokenBoughtAmount": ReferralTokenAmountPreFree
                                    },
                                    $set: {
                                        "Customer": tokenCharge.customer
                                    }
                                }, function(error, result) {
                                    if (result) {
                                        Meteor.users.update({
                                            _id: Meteor.user().Referral.Referrer
                                        }, {
                                            $inc: {
                                                "profile.Tokens": Math.round(ReferralTokenAmountPreFree * .003)
                                            }
                                        })
                                    }
                                });

                            } else {
                                Meteor.users.update({
                                    _id: Meteor.userId()
                                }, {
                                    $inc: {
                                        "profile.Tokens": TokenCount
                                    },
                                    $set: {
                                        "Customer": tokenCharge.customer
                                    }
                                })
                            }
                            return tokenCharge.status;
                        } else {
                            throw new Meteor.error(500, "Charge was not processed");
                        }
                    }
                } else {
                    throw new Meteor.error(500, "Charge was not processed");
                }
            }
        } catch (e) {
            throw new Meteor.Error(500, e.message);
        }

    },
    ChargePaymentMethodBuyTokensSaved: function(TokenCount, CardId) {
        try {
            if (this.userId) {
                if (isNaN(TokenCount) || TokenCount < 150 || TokenCount > 10000) {
                    throw new Meteor.error(500, "Token amount is invalid");
                }
                if (Meteor.user().Customer == null) {
                    throw new Meteor.error(500, "This is not a valid card");
                }
                TokenCount = Math.floor(TokenCount);
                var Stripe = StripeSync(Meteor.settings.private.stripe.testSecretKey);
                var tokenCharge = Stripe.charges.create({
                    amount: TokenCount * 10,
                    currency: "usd",
                    source: CardId,
                    description: "Buying " + TokenCount + " tokens.",
                    customer: Meteor.user().Customer
                });
                var ReferralTokenAmountPreFree = TokenCount;
                TokenCount += Math.floor((TokenCount - 150) / 50);
                if (tokenCharge.paid) {
                    if (Meteor.user().Referral && Meteor.user().Referral.Referrer) {
                        Meteor.users.update({
                            _id: Meteor.userId()
                        }, {
                            $inc: {
                                "profile.Tokens": TokenCount,
                                "Referral.TokenBoughtAmount": ReferralTokenAmountPreFree
                            },
                        }, function(error, result) {
                            if (result) {
                                Meteor.users.update({
                                    _id: Meteor.user().Referral.Referrer
                                }, {
                                    $inc: {
                                        "profile.Tokens": Math.round(ReferralTokenAmountPreFree * .003)
                                    }
                                })
                            }
                        });

                    } else {
                        Meteor.users.update({
                            _id: Meteor.userId()
                        }, {
                            $inc: {
                                "profile.Tokens": TokenCount
                            }
                        })
                    }
                    return tokenCharge.status;
                }
            }
        } catch (e) {
            console.log(e);
            throw new Meteor.Error(500, "Charge was not processed");
        }
    },
    RetrievePaymentMethods: function() {
        if (this.userId) {
            try {
                if (Meteor.user()) {
                    if (Meteor.user().Customer != null) {
                        Stripe = StripeSync(Meteor.settings.private.stripe.testSecretKey);
                        var cardList = Stripe.customers.listCards(Meteor.user().Customer);
                        return cardList.data;
                    } else {
                        return [];
                    }
                }
            } catch (e) {
                console.log(e);
            }
        }
    },
    CreateTicket: function(RequestTicket) {
        if (this.userId) {
            if (RequestTicket.length > 0) {


                Meteor.users.update({
                    _id: Meteor.userId()
                }, {
                    $push: {
                        "Tickets": {
                            Ticket: RequestTicket,
                            Status: false,
                            Creator: Meteor.user().profile.FirstName + " " + Meteor.user().profile.LastName,
                            IsUser: true,
                            createdAt: new Date()
                        }
                    }
                });
            }
        }
    },
    QuestStatus: function() {
        if (this.userId) {
            return Meteor.user().Quests;
        }
    },
    FacebookQuest: function(Username) {
        if (this.userId) {
            if (Username) {
                Future = Npm.require('fibers/future');
                var myFuture = new Future();
                Meteor.users.update({
                    _id: this.userId,
                    "Quests.FacebookQuest.Active": true,
                    "Quests.FacebookQuest.Submitted": false
                }, {
                    $set: {
                        "Quests.FacebookQuest.Submitted": true,
                        "Quests.FacebookQuest.Username": Username
                    }
                }, function(error, result) {
                    if (result) {
                        myFuture.return("Success");
                    } else {
                        throw new Meteor.Error(500, "Quest could not be completed");
                    }
                })
                return myFuture.wait();
            }
        }
    },
    TwitterQuest: function(Id) {
        if (this.userId) {
            try {
                if (!Id) {
                    throw new Meteor.Error(500, "Not a valid username");
                } else {
                    var TwitterId = Id;
                }
                if (Meteor.user().Quests.TwitterQuest.Active) {

                    var options = {
                        consumer_key: Meteor.settings.Twitter.ConsumerKey,
                        consumer_secret: Meteor.settings.Twitter.ConsumerSecretKey,
                        access_token_key: Meteor.settings.Twitter.AccessToken,
                        access_token_secret: Meteor.settings.Twitter.AccessTokenSecret
                    }
                    var client = new Twitter(options);

                    var params = {
                        screen_name: TwitterId,
                        count: 10,
                        exclude_replies: true,
                        include_rts: false
                    };
                    var isFollowing = client.get('friendships/show', {
                        source_screen_name: TwitterId,
                        target_screen_name: 'Spargain'
                    })
                    if (!isFollowing.relationship.source.following) {
                        throw new Meteor.Error(500, "You must be following @Spargain");
                    }
                    var twitterResponse = client.get('statuses/user_timeline', params);
                    if (twitterResponse.error) {
                        throw new Meteor.Error(500, twitterResponse.error.message);
                    } else {
                        var dateWeekConstraint = moment().subtract(7, 'days');
                        var dateMinuteConstraint = moment().subtract(10, 'minutes');
                        for (var i = 0; i < twitterResponse.length; i++) {
                            if (!twitterResponse[i].user.protected) {
                                if (moment(twitterResponse[i].created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').isBetween(dateWeekConstraint, dateMinuteConstraint)) {
                                    for (var j = 0; j < twitterResponse[i].entities.hashtags.length; j++) {
                                        if (twitterResponse[i].entities.hashtags[j].text.toLowerCase() == "spargain") {
                                            if ((Math.floor(twitterResponse[i].user.followers_count * 100) / 100) < 100) {
                                                throw new Meteor.Error(500, "You need at least 100 followers.");
                                            } else if (100 <= (Math.floor(twitterResponse[i].user.followers_count * 100) / 100) <= 1000000) {
                                                var tokenIncreaseAmount = (Math.floor(twitterResponse[i].user.followers_count * 100) / 100) / 100;
                                                var newActiveDate = new Date();
                                                newActiveDate.setDate(newActiveDate.getDate() + 7);
                                                Meteor.users.update({
                                                    _id: Meteor.userId()
                                                }, {
                                                    $inc: {
                                                        "profile.Tokens": tokenIncreaseAmount
                                                    },
                                                    $set: {
                                                        "Quests.TwitterQuest.Active": false,
                                                        "Quests.TwitterQuest.ActiveRenewalDate": newActiveDate
                                                    }
                                                })
                                                var twitterQuestRenewalJob = new Job(QuestData, 'RenewTwitterQuest', {
                                                    UserId: Meteor.userId()
                                                }).delay(604800000).save();
                                                return tokenIncreaseAmount;

                                            } else if ((Math.floor(twitterResponse[i].user.followers_count * 100) / 100) > 1000000) {
                                                var newActiveDate = moment().add(7, 'days');
                                                Meteor.users.update({
                                                    _id: Meteor.userId()
                                                }, {
                                                    $inc: {
                                                        "profile.Tokens": 10000
                                                    },
                                                    $set: {
                                                        "Quests.TwitterQuest.Active": false,
                                                        "Quests.TwitterQuest.ActiveRenewalDate": newActiveDate
                                                    }
                                                });
                                                var twitterQuestRenewalJob = new Job(QuestData, 'RenewTwitterQuest', {
                                                    UserId: Meteor.userId()
                                                }).delay(604800000).save();
                                                return tokenIncreaseAmount;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        throw new Meteor.Error("No Hashtag", "No recent tweets include #Spargain");
                    }


                }


            } catch (e) {
                if (e.reason != undefined) {
                    throw new Meteor.Error(500, e.reason);
                }
                console.log(e);
                throw new Meteor.Error(500, "Quest could not be completed");
            }
        }
    },
    CompetitionPurchaseEntrance: function(CompetitionId) {
        if (this.userId) {
            var user = Meteor.users.findOne({
                _id: Meteor.userId(),
                "profile.Tokens": {
                    $gte: 5
                },
                CompetitionsEntered: {
                    $nin: [CompetitionId]
                }
            })
            if (user) {
                var itemCompetition = ItemCompetitions.findOne({
                    _id: CompetitionId,
                    IsLive: false,
                    HasBeenWon: false
                }, {
                    fields: {
                        "Item.SellingPrice": 1,
                    }
                });
                var itemCompetitionNewContestant = ItemCompetitions.update({
                    _id: CompetitionId,
                    IsLive: false,
                    HasBeenWon: false
                }, {
                    $inc: {
                        EntrantCount: 1,
                        Progress: 5 / itemCompetition.Item.SellingPrice
                    }
                })
                if (itemCompetitionNewContestant == 1) {
                    var contestant = Meteor.users.update({
                        _id: Meteor.userId(),
                        "profile.Tokens": {
                            $gte: 5
                        },
                        CompetitionsEntered: {
                            $nin: [CompetitionId]
                        }
                    }, {
                        $addToSet: {
                            CompetitionsEntered: CompetitionId
                        },
                        $inc: {
                            "profile.Tokens": -5
                        }
                    })
                    if (contestant == 1) {
                        return "Success"
                    } else {
                        console.log(contestant);
                        throw new Meteor.Error(500, "Cannot Add To Competition");
                    }
                } else {
                    throw new Meteor.Error(500, "Cannot Add To Competition");
                }
            } else {
                console.log("Hi");
                throw new Meteor.Error(500, "Insufficient Tokens Or Already Entered");
            }
        }
        throw new Meteor.Error(500, "Not Logged In");
    },
    PlaceSlotForItemCompetition: function(Slot, UpperSlotRange, CompetitionId) {
        this.unblock();
        try {
            Future = Npm.require('fibers/future');
            var myFuture = new Future();

            var user = Meteor.users.findOne({
                _id: Meteor.userId(),
                CompetitionsEntered: CompetitionId
            }, {
                fields: {
                    "profile.Tokens": 1
                }
            })
            if (user) {
                var itemCompetition = ItemCompetitions.findOne({
                    _id: CompetitionId,
                    IsLive: true,
                    HasBeenWon: false
                }, {
                    fields: {
                        SlotPrice: 1,
                        "Item.SellingPrice": 1,
                        SlotCount: 1,
                        EndTimeOfCompetition: 1,
                        Progress: 1,
                        StartTimeOfCompetition: 1
                    }
                });
                findObject = {};
                findObject["SlotPrice"] = {
                    $lte: user.profile.Tokens
                };
                findObject["_id"] = CompetitionId;
                findObject[(UpperSlotRange - 99) + "-" + UpperSlotRange] = {
                    $elemMatch: {
                        "Slot": Slot,
                        "User": null
                    }
                };
                updateObject = {};
                updateObject[(UpperSlotRange - 99) + "-" + UpperSlotRange + ".$.User"] = Meteor.userId();
                var accurate = (moment() - itemCompetition.StartTimeOfCompetition) / (itemCompetition.EndTimeOfCompetition - itemCompetition.StartTimeOfCompetition);
                // updateObject["SlotPrice"] = Math.ceil(Math.pow((itemCompetition.Progress * itemCompetition.Item.SellingPrice),(itemCompetition.SlotCount/12000)*accurate*2));
                updateObject["SlotPrice"] = 1 + Math.floor(((itemCompetition.Progress * itemCompetition.Item.SellingPrice) / (12000 - itemCompetition.SlotCount)) * accurate * 125);
                // console.log(Math.pow(((itemCompetition.Progress * itemCompetition.Item.SellingPrice)/(itemCompetition.SlotCount/12000)),accurate));
                // console.log(((itemCompetition.Progress * itemCompetition.Item.SellingPrice) / (12000 - itemCompetition.SlotCount)) * accurate*125);
                ItemCompetitions.update(findObject, {
                    $set: updateObject,
                    $inc: {
                        "SlotCount": 1,
                        "Progress": itemCompetition.SlotPrice / itemCompetition.Item.SellingPrice,
                    }

                }, function(error, result) {
                    if (result) {
                        Meteor.users.update({
                            _id: Meteor.userId()
                        }, {
                            $inc: {
                                "profile.Tokens": -itemCompetition.SlotPrice
                            },
                            $push: {
                                "CompetitionSlots": {
                                    "CompetitionId": CompetitionId,
                                    "Slot": Slot,
                                    "Price": itemCompetition.SlotPrice
                                }
                            }
                        });
                        serverMessages.notify('ItemCompetition' + CompetitionId, Slot, Math.floor(itemCompetition.SlotPrice));
                        myFuture.return({
                            "Slot": Slot,
                            "Price": itemCompetition.SlotPrice
                        })
                    }
                });
            } else {
                throw new Meteor.Error(500, "Not entered in Competition");
            }
            return myFuture.wait();
        } catch (e) {
            console.log(e);
        }
    }
});
