import {
    Meteor
} from 'meteor/meteor';
import {
    DDPRateLimiter
} from 'meteor/ddp-rate-limiter';
import {
    _
} from 'meteor/underscore';
Meteor.startup(function() {
    const LISTS_PUBLICATIONS = _.pluck([
        'Main',
        'UserProfile',
        'UserProfileProfile',
        "UserProfileOrders",
        "Blog",
        "UserProfileQuests",
        "UserProfileCart",
        "UserProfileAddToCartResult",
        "UserProfileCheckout",
        "ItemCompetitionsPage",
        "ItemCompetitions",
        "ItemCompetitionsListResult",
        "Item",
        "ItemCompetitionInnerSlots",
        "ItemCompetition",
        "ItemCompetitionResult",
        'Items',
        "ItemCompetitionUserWatchlist",
        "Contact",
        "AdminChangeNotification",
        "AdminCompletedItemCompetitions",
        "AdminCreateItemCompetition",
        "AdminCreatedItemCompetitions",
        "AdminUserTickets",
        "AdminUserAccounts",
        "AdminPlacedOrders",
        "AdminPendingShipmentOrders",
        "AdminCompletedOrders",
        "AdminCreatedItems",
        "AdminReplacementReturnRequests",
        "AdminCompletedReplacementReturns",
        "AdminFacebookQuest"
    ], 'Publications');

});
if (Meteor.isServer) {
    // Only allow 5 list operations per connection per second
    DDPRateLimiter.addRule({
        Publications(Publications) {
            return _.contains(LISTS_PUBLICATIONS, Publications);
        },

        // Rate limit per connection ID
        connectionId() {
            return true;
        }
    }, 5, 1000);
}
Meteor.publish("Main", function() {
    // return Meteor.users.find({
    //     "status.online": true
    // }, {
    //     fields: {
    //         "status.online": 1,
    //     }
    // })

})
Meteor.publish("UserProfile", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "roles": 1,
            "profile": 1,
            "Cart._id": 1
        }
    })
});
Meteor.publish("UserProfileProfile", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Addresses": 1,
            "CompetitionSettings": 1,
            "CompetitionInformation": 1,
            "createdAt": 1
        }
    })
});
Meteor.publish("Blog", function() {
    return Blog.find({});
})
Meteor.publish("UserProfileOrders", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Orders.HasBeenCompleted": 1,
            "Orders.ShippingStatus": 1,
            "Orders.ShippingInformation": 1,
            "Orders.WonFromCompetition": 1,
            "Orders.TrackingUrl": 1,
            "Orders.Quantity": 1,
            "Orders.Item": 1,
            "Orders.OrderPlacedDate": 1,
            "Orders._id": 1,
            "Orders.OrderId": 1,
            "Orders.Cancelable": 1,
            "ReplacementsRefunds.OrderId": 1,
            "ReplacementsRefunds.ItemId": 1,
            "ReplacementsRefunds.Quantity": 1,
            "ReplacementsRefunds.RequestDate": 1,
            "ReplacementsRefunds.HasBeenRefundedReplaced": 1,
            "ReplacementsRefunds.RefundOrReplace": 1,

        }
    })
});
Meteor.publish("UserProfileQuests", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Quests": 1
        }
    });
});
Meteor.publish("UserProfileCart", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Cart": 1
        }
    })
});
Meteor.publish("UserProfileAddToCartResult", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Cart.WonFromCompetition": 1,
            "Cart.Item": 1,
            "Cart.Quantity": 1
        }
    })
});
Meteor.publish("UserProfileCheckout", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Cart": 1,
            "Addresses": 1
        }
    })
});
Meteor.publish("ItemCompetitionsPage", function() {
    return Notification.find({});
});
Meteor.publish("ItemCompetitions", function(skipAmount) {
    return [
        ItemCompetitions.find({
            Cancelled: {
                $exists: false
            }
        }, {
            limit: 20,
            skip: skipAmount,
            fields: {
                "StartTimeOfCompetition": 1,
                "EndTimeOfCompetition": 1,
                "Item.Main_Image": 1,
                "Item.Name": 1,
                "Item._id": 1,
                "IsLive": 1,
                "HasBeenWon": 1,
                "SlotPrice": 1,
                "EntrantCount": 1,
                "SlotCount": 1,
                "Progress": 1,
                "WinnersCalculated": 1,
                "Cancelled": 1
            },
            sort: {
                StartTimeOfCompetition: 1
            },
        }),
        Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                "Watchlist": 1,
                "CompetitionsEntered": 1
            }
        })
    ];
});
Meteor.publish("ItemCompetitionsListResult", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Cart.WinningCompetitionId": 1,
            "Cart.Discount": 1

        }
    });
});
Meteor.publish("Item", function(Id) {
    return [
        Items.find({
            _id: Id
        }),
        ItemCompetitions.find({
            "Item._id": Id,
            "HasBeenWon": false
        }, {
            sort: {
                "StartTimeOfCompetition": 1
            },

            fields: {
                "StartTimeOfCompetition": 1,
                "EndTimeOfCompetition": 1,
                "Item.Main_Image": 1,
                "Item.Name": 1,
                "Item._id": 1,
                "IsLive": 1,
                "HasBeenWon": 1,
                "SlotPrice": 1,
                "SlotCount": 1,
                "Progress": 1,
                "WinnersCalculated": 1,
                "Winners": 1,
                "Cancelled": 1,
                "EntrantCount": 1
            },
            limit: 4
        }),
        Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                "Watchlist": 1,
                "CompetitionsEntered": 1
            }
        })
    ];
});
Meteor.publish("ItemCompetitionInnerSlots", function(CompetitionId, itemCompetitionLowerBound, itemCompetitionUpperBound) {
    var slotRange = {};
    slotRange[itemCompetitionLowerBound + "-" + itemCompetitionUpperBound + ".Slot"] = 1;
    slotRange[itemCompetitionLowerBound + "-" + itemCompetitionUpperBound + ".User"] = 1;
    slotRange["SlotPrice"] = 1;
    return ItemCompetitions.find({
        _id: CompetitionId
    }, {
        fields: slotRange
    });
    // return ItemCompetitionsSlots.matching("vA5vfvDjJbkDFe3JW*");

});

Meteor.publish("ItemCompetition", function(Id) {
    return [ItemCompetitions.find({
            _id: Id
        }, {
            fields: {
                "StartTimeOfCompetition": 1,
                "EndTimeOfCompetition": 1,
                "Item.Name": 1,
                "Item.Main_Image": 1,
                "Item.SellingPrice": 1,
                "Item._id": 1,
                "IsLive": 1,
                "HasBeenWon": 1,
                "SlotPrice": 1,
                "SlotCount": 1,
                "Progress": 1,
                "WinnersCalculated": 1,
                "Winners": 1,
                "Cancelled": 1,
                "EntrantCount": 1
            }
        }),
        Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                "CompetitionsEntered": 1,
                "CompetitionSettings.FastSlots": 1,
            }
        })
    ]
});
Meteor.publish("ItemCompetitionResult", function(Id, ItemId) {
    return [ItemCompetitions.find({
            _id: {
                $ne: {
                    Id
                }
            },
            "Item._id": ItemId,
            HasBeenWon: false,
            CreationComplete: true
        }, {
            sort: {
                "StartTimeOfCompetition": 1
            },
            limit: 3,
            fields: {
                "StartTimeOfCompetition": 1,
                "EndTimeOfCompetition": 1,
                "Item.Name": 1,
                "Item.Main_Image": 1,
                "SlotCount": 1,
                "Progress": 1,
                "NumberOfWinners": 1,
                "HasBeenWon": 1,
                "IsLive": 1,
                "SlotPrice": 1
            }
        }),
        Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                "Cart": 1,
                "CompetitionSlots": 1,
                "Watchlist": 1
            }
        })
    ]
})

Meteor.publish("Items", function() {
    return Items.find({});
});

Meteor.publish("ItemCompetitionUserWatchlist", function(skipAmount) {
    if (this.userId) {
        var user = Meteor.users.findOne({
            _id: this.userId
        }, {
            fields: {
                "Watchlist": 1
            }
        });

        return [ItemCompetitions.find({
            _id: {
                $in: user.Watchlist
            }
        }, {
            limit: 20,
            skip: skipAmount,
            fields: {
                "StartTimeOfCompetition": 1,
                "EndTimeOfCompetition": 1,
                "Item.Main_Image": 1,
                "Item.Name": 1,
                "Item._id": 1,
                "IsLive": 1,
                "HasBeenWon": 1,
                "SlotPrice": 1,
                "SlotCount": 1,
                "Progress": 1,
                "WinnersCalculated": 1,
                "Winners": 1,
                "Cancelled": 1,
                "EntrantCount": 1
            },
            sort: {
                StartTimeOfCompetition: 1
            }

        }), Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                "Watchlist": 1,
                "CompetitionsEntered": 1
            }
        })]
    }
});
Meteor.publish("ItemCompetitionUserEntered", function(skipAmount) {
    if (this.userId) {
        var user = Meteor.users.findOne({
            _id: this.userId
        }, {
            fields: {
                "CompetitionsEntered": 1
            }
        });

        return [ItemCompetitions.find({
            _id: {
                $in: user.CompetitionsEntered
            }
        }, {
            limit: 20,
            skip: skipAmount,
            fields: {
                "StartTimeOfCompetition": 1,
                "EndTimeOfCompetition": 1,
                "Item.Main_Image": 1,
                "Item.Name": 1,
                "Item._id": 1,
                "IsLive": 1,
                "HasBeenWon": 1,
                "SlotPrice": 1,
                "SlotCount": 1,
                "Progress": 1,
                "WinnersCalculated": 1,
                "Winners": 1,
                "Cancelled": 1,
                "EntrantCount": 1
            },
            sort: {
                StartTimeOfCompetition: 1
            }

        }), Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                "Watchlist": 1,
                "CompetitionsEntered": 1
            }
        })]
    }
});
Meteor.publish("Contact", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            "Tickets": 1
        },
        sort: {
            "Tickets.createdAt": 1
        },
    });
})

Meteor.publish("AdminChangeNotification", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Notification.find({});
    }
});
Meteor.publish("AdminCompletedItemCompetitions", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return CompletedItemCompetitions.find({});
    }
});
Meteor.publish("AdminCreateItemCompetition", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return [Items.find({
            "Available": true
        }), ItemCompetitions.find({

        }, {
            fields: {
                "CreationComplete": 1
            }
        })]
    }
})
Meteor.publish("AdminCreatedItemCompetitions", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return ItemCompetitions.find({});
    }
});
Meteor.publish("AdminUserTickets", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Meteor.users.find({
            roles: {
                $in: ["User"]
            },
            "Tickets.Status": false
        }, {
            fields: {
                "Tickets": 1,
                "profile": 1
            }
        });

    }
});
Meteor.publish("AdminUserAccounts", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Meteor.users.find({
            roles: {
                $in: ["User"]
            },
        }, {
            fields: {}
        });
    }
});
Meteor.publish("AdminPlacedOrders", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Meteor.users.find({
            roles: {
                $in: ["User"]
            },
            "Orders.Cancelable": false
        }, {
            fields: {
                "Orders": 1
            }
        })
    }
});
Meteor.publish("AdminCompletedOrders", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Meteor.users.find({
            roles: {
                $in: ["User"]
            },
            "Orders.HasBeenCompleted": true,
            "Orders.Cancelable": false
        }, {
            fields: {
                "Orders": 1
            }
        })
    }
});
Meteor.publish("AdminCreatedItems", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Items.find({});
    }
});
Meteor.publish("AdminReplacementReturnRequests", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return [Meteor.users.find({
                roles: {
                    $in: ["User"]
                },
                ReplacementsRefunds: {
                    $elemMatch: {
                        HasBeenRefundedReplaced: false,
                        InformationEmailSent: false
                    }
                }
            }, {
                fields: {
                    ReplacementsRefunds: 1,
                    emails: 1
                }
            }),
            Items.find({}, {
                fields: {
                    Name: 1,
                    SellingPrice: 1,
                    Main_Image: 1
                }
            })
        ];
    }
});
Meteor.publish("AdminReplacementReturnRequestsInformationEmailSent", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return [Meteor.users.find({
                roles: {
                    $in: ["User"]
                },
                ReplacementsRefunds: {
                    $elemMatch: {
                        HasBeenRefundedReplaced: false,
                        InformationEmailSent: true
                    }
                }
            }, {
                fields: {
                    ReplacementsRefunds: 1,
                    emails: 1
                }
            }),
            Items.find({}, {
                fields: {
                    Name: 1,
                    SellingPrice: 1,
                    Main_Image: 1
                }
            })
        ];
    }
})
Meteor.publish("AdminCompletedReplacementReturns", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return [Meteor.users.find({
                roles: {
                    $in: ["User"]
                },
                ReplacementsRefunds: {
                    $elemMatch: {
                        HasBeenRefundedReplaced: true
                    }
                }
            }, {
                fields: {
                    ReplacementsRefunds: 1
                }
            }),
            Items.find({}, {
                fields: {
                    Name: 1,
                    SellingPrice: 1,
                    Main_Image: 1
                }
            })
        ];
    }
})
Meteor.publish("AdminFacebookQuest", function() {
    if (Roles.userIsInRole(this.userId, 'Admin')) {
        return Meteor.users.find({
            roles: {
                $in: ["User"]
            },
            "Quests.FacebookQuest.Submitted": true,
            "Quests.FacebookQuest.Active": true

        }, {
            fields: {
                roles: 1,
                Quests: 1
            }
        });
    }
})
