Meteor.startup(function() {
    SyncedCron.start();
    SyncedCron.config({
      collectionName: 'cronHistory',
      utc:true
    })
});
