Meteor.startup(function() {
    Meteor.users._ensureIndex({
        "Watchlist": 1
    });
    Meteor.users._ensureIndex({
        "CompetitionSlots.CompetitionId": 1
    });
    Meteor.users._ensureIndex({
        "Cart._id": 1
    });
    Meteor.users._ensureIndex({
        "Orders._id": 1
    });
    ItemCompetitionData._ensureIndex({
      created:1},{expireAfterSeconds:86400
    })
    ItemCompetitionData._createCappedCollection(536870912,5000);
    ItemCompetitionWinnersExpiration._ensureIndex({
      created:1},{expireAfterSeconds:86400
    })
    ItemCompetitionUserDataClean._ensureIndex({
      created:1},{expireAfterSeconds:86400
    })
    ItemData._ensureIndex({
      created:1},{expireAfterSeconds:86400
    })
    OrderData._ensureIndex({
      created:1},{expireAfterSeconds:86400
    })
    QuestsData._ensureIndex({
      created:1},{expireAfterSeconds:2629746
    })
});
