import {
    Meteor
} from 'meteor/meteor';
import {
    DDPRateLimiter
} from 'meteor/ddp-rate-limiter';
import {
    _
} from 'meteor/underscore';
Meteor.startup(function() {
    const LISTS_METHODS = _.pluck([
        'CompleteReturn',
        'CompleteReplacement',
        'AddNotification',
        'OrderComplete',
        'MakeCreatedItemAvailable',
        'MakeCreatedItemUnavailable',
        'RespondToTicket',
        'RemoveCompetition',
        'RemoveItem',
        'UpdateTokenAmount',
        'AddItemCompetition',
        'CreateNewItem',
        'UpdateItem',
        'AddTrackingUrl',
        'ReturnReplacementInformationEmailSent',
        'AcceptFacebookQuest',
        'RejectFacebookQuest',
        'AdminCreateCompetitionSyncedCron'
    ], 'AdminMethods');

});
if (Meteor.isServer) {
    // Only allow 5 list operations per connection per second
    DDPRateLimiter.addRule({
        AdminMethods(AdminMethods) {
            return _.contains(LISTS_METHODS, AdminMethods);
        },

        // Rate limit per connection ID
        connectionId() {
            return true;
        }
    }, 5, 1000);
}
Meteor.methods({
    AddTrackingUrl: function(OrderInformation, TrackingUrl) {
        try {
            if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
                Meteor.users.find({
                    Orders: {
                        $elemMatch: {
                            OrderId: OrderInformation.OrderId,
                            Cancelable: false,
                            HasBeenCompleted: false,
                        }
                    }
                }, {
                    fields: {
                        "profile.FirstName": 1,
                        "profile.LastName": 1,
                        emails: 1,
                        Orders: 1
                    }
                }).forEach(function(doc) {
                    doc.Orders.forEach(function(Order) {
                        if (Order.OrderId == OrderInformation.OrderId && !Order.Cancelled && !Order.HasBeenCompleted) {
                            Meteor.users.update({
                                Orders: {
                                    $elemMatch: {
                                        _id: Order._id,
                                        Cancelable: false,
                                        HasBeenCompleted: false
                                    }
                                }
                            }, {
                                $set: {
                                    "Orders.$.TrackingUrl": TrackingUrl,
                                    "Orders.$.ShippingStatus": "Shipped"
                                }
                            })
                        }
                    })
                    SSR.compileTemplate('OrderTrackingEmail', Assets.getText('OrderTrackingInformation.html'));
                    var emailData = {
                        OrderId: OrderInformation.OrderId,
                        OrderPlacedDate: moment(OrderInformation.OrderPlacedDate).format("dddd, MMMM Do YYYY, h:mm:ss a"),
                        FirstName: doc.profile.FirstName,
                        LastName: doc.profile.LastName,
                        TrackingUrl: TrackingUrl,
                        ShippingInformation: OrderInformation.ShippingInformation,
                        Items: OrderInformation.Orders,
                    };
                    Email.send({
                        to: doc.emails[0].address,
                        from: "Spargain <support@spargain.com>",
                        subject: "Your Spargain Order Tracking Information",
                        html: SSR.render('OrderTrackingEmail', emailData)
                    });
                });



            }
        } catch (e) {
            console.log(e);
        }
    },
    ReturnReplacementInformationEmailSent: function(Request) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Meteor.users.update({
                ReplacementsRefunds: {
                    $elemMatch: {
                        OrderId: Request.OrderId,
                        ItemId: Request.ItemId,
                        HasBeenRefundedReplaced: false,
                        InformationEmailSent: false,
                        Quantity: Request.Quantity,
                        Price: Request.Price
                    }
                }
            }, {
                $set: {
                    "ReplacementsRefunds.$.InformationEmailSent": true
                }
            })
        }
    },
    CompleteReturn: function(Request) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            if (Request.Quantity == Request.ItemQuantity) {
                Meteor.users.update({
                    Orders: {
                        $elemMatch: {
                            Quantity: {
                                $gte: Request.Quantity
                            },
                            "Item._id": Request.ItemId,
                            "Item.SellingPrice": Request.Price
                        }
                    },
                    ReplacementsRefunds: {
                        $elemMatch: {
                            OrderId: Request.OrderId,
                            ItemId: Request.ItemId,
                            HasBeenRefundedReplaced: false,
                            Quantity: Request.Quantity,
                            Price: Request.Price
                        }
                    }
                }, {
                    $pull: {
                        Orders: {
                            OrderId: Request.OrderId,
                            Quantity: Request.Quantity,
                            "Item._id": Request.ItemId,
                            "Item.SellingPrice": Request.Price,
                        }
                    },
                    $inc: {
                        "profile.Tokens": Request.Price * Request.Quantity
                    },
                    $set: {
                        "ReplacementsRefunds.$.HasBeenRefundedReplaced": true,
                    }
                })
            } else {
                Meteor.users.update({
                    Orders: {
                        $elemMatch: {
                            OrderId: Request.OrderId,
                            Quantity: {
                                $gte: Request.Quantity
                            },
                            "Item._id": Request.ItemId,
                            "Item.SellingPrice": Request.Price
                        }
                    },
                    ReplacementsRefunds: {
                        $elemMatch: {
                            OrderId: Request.OrderId,
                            ItemId: Request.ItemId,
                            HasBeenRefundedReplaced: false,
                            Quantity: Request.Quantity,
                            Price: Request.Price
                        }
                    }
                }, {
                    $set: {
                        "ReplacementsRefunds.$.HasBeenRefundedReplaced": true,
                    }
                })
                Meteor.users.update({
                    ReplacementsRefunds: {
                        $elemMatch: {
                            OrderId: Request.OrderId,
                            ItemId: Request.ItemId,
                            HasBeenRefundedReplaced: true,
                            Quantity: Request.Quantity,
                            Price: Request.Price
                        }
                    },
                    Orders: {
                        $elemMatch: {
                            OrderId: Request.OrderId,
                            Quantity: {
                                $gte: Request.Quantity
                            },
                            "Item._id": Request.ItemId,
                            "Item.SellingPrice": Request.Price
                        }
                    }
                }, {
                    $inc: {
                        "Orders.$.Quantity": -Request.Quantity,
                        "profile.Tokens": Request.Price * Request.Quantity
                    }
                })


            }

        }
    },
    CompleteReplacement: function(Request) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Meteor.users.update({
                Orders: {
                    $elemMatch: {
                        OrderId: Request.OrderId,
                        Quantity: {
                            $gte: Request.Quantity
                        },
                        "Item._id": Request.ItemId,
                        "Item.SellingPrice": Request.Price
                    }
                },
                ReplacementsRefunds: {
                    $elemMatch: {
                        OrderId: Request.OrderId,
                        ItemId: Request.ItemId,
                        HasBeenRefundedReplaced: false,
                        Quantity: Request.Quantity,
                        Price: Request.Price
                    }
                }
            }, {
                $set: {
                    "ReplacementsRefunds.$.HasBeenRefundedReplaced": true,
                }
            })
        }
    },
    AddNotification: function(NotificationInformation) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            if (Notification.find().count() > 0) {
                Notification.remove({});
                Notification.insert(
                    NotificationInformation
                )
            } else {
                Notification.insert(
                    NotificationInformation
                )
            }

        }
    },
    OrderComplete: function(OrderId) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Meteor.users.find({
                Orders: {
                    $elemMatch: {
                        OrderId: OrderId,
                        Cancelable: false,
                        HasBeenCompleted: false,
                    }
                }
            }, {
                fields: {
                    Orders: 1
                }
            }).forEach(function(doc) {
                doc.Orders.forEach(function(Order) {
                    if (Order.OrderId == OrderId && !Order.Cancelled && !Order.HasBeenCompleted) {
                        Meteor.users.update({
                            Orders: {
                                $elemMatch: {
                                    _id: Order._id,
                                    Cancelable: false,
                                    HasBeenCompleted: false
                                }
                            }
                        }, {
                            $set: {
                                "Orders.$.HasBeenCompleted": true,
                                "Orders.$.ShippingStatus": "Delivered"
                            }
                        })
                    }
                })

            })
        }
    },
    MakeCreatedItemAvailable: function(Id) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Items.update({
                _id: Id,
                "Available": false
            }, {
                $set: {
                    "Available": true
                }
            });
        }
    },
    MakeCreatedItemUnavailable: function(Id) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            var job = new Job(ItemData, 'RemoveItemFromUserCarts', {
                Id: Id
            }).save();
            Items.update({
                _id: Id,
                "Available": true
            }, {
                $set: {
                    "Available": false
                }
            });
        }
    },
    RespondToTicket: function(UserId, TicketContent) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Meteor.users.update({
                _id: UserId,
                Tickets: {
                    $elemMatch: {
                        Status: false
                    }
                }
            }, {
                $push: {
                    Tickets: {
                        Ticket: TicketContent,
                        Status: true,
                        Creator: 'Spargain Support',
                        IsUser: false,
                        createdAt: new Date()
                    }
                }
            }, function(error, result) {
                if (result == 1) {
                    var Tickets = Meteor.users.findOne({
                        _id: UserId
                    });
                    for (var i = 0; i < Tickets.Tickets.length; i++) {
                        Tickets.Tickets[i].Status = true;
                    }
                    Meteor.users.update({
                        _id: UserId,
                        Tickets: {
                            $elemMatch: {
                                Status: false
                            }
                        }
                    }, {
                        $set: {
                            'Tickets': Tickets.Tickets
                        },
                    })
                }
            });


        }

    },
    RemoveCompetition: function(Id) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            ItemCompetitions.remove({
                _id: Id
            });
        }

    },
    RemoveItem: function(Id) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Items.remove({
                _id: Id
            })
        }
    },
    UpdateTokenAmount: function(UserId, NewTokenAmount) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Meteor.users.update({
                _id: UserId
            }, {
                $set: {
                    "profile.Tokens": NewTokenAmount
                }
            }, function(error, result) {
                if (result == 1) {
                    return "Success";
                }
            });
        }

    },
    AddItemCompetition: function(NewItemCompetition) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            //need to know the timezone for proper fix
            // ItemCompetitionSchema.clean(NewItemCompetition);
            if (NewItemCompetition.StartTimeOfCompetition && NewItemCompetition.EndTimeOfCompetition && NewItemCompetition.Item) {
                Future = Npm.require('fibers/future');
                var myFuture = new Future();

                NewItemCompetition.StartTimeOfCompetition = new Date(NewItemCompetition.StartTimeOfCompetition).toGMTString();
                NewItemCompetition.EndTimeOfCompetition = new Date(NewItemCompetition.EndTimeOfCompetition).toGMTString();
                // ItemCompetitionSchema.clean(NewItemCompetition);
                NewItemCompetition.CreationComplete = false;
                NewItemCompetition.SlotPrice = 1;
                NewItemCompetition.SlotCount = 0;
                NewItemCompetition.Progress = 0;
                NewItemCompetition.HasBeenWon = false;
                NewItemCompetition.IsLive = false;
                NewItemCompetition.EntrantCount = 0;
                NewItemCompetition.WinnersCalculated = false;
                NewItemCompetition._id = Random.id();
                NewItemCompetition.Item = Items.findOne({
                    _id: NewItemCompetition.Item
                });
                var job = new Job(ItemCompetitionData, 'CreateItemCompetitionsJobs', {
                    ItemCompetitionData: NewItemCompetition
                }).save();
                myFuture.return(NewItemCompetition._id);
                return myFuture.wait();
            }
        }
    },
    CreateNewItem: function(Item) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Items.insert({
                "Name": Item.Name,
                "SellingPrice": Item.SellingPrice,
                "Description": Item.Description,
                "Main_Image": Item.Main_Image,
                "Alternate_Image_1": Item.Alternate_Image_1,
                "Alternate_Image_2": Item.Alternate_Image_2,
                "Alternate_Image_3": Item.Alternate_Image_3,
                "Alternate_Image_4": Item.Alternate_Image_4
            });
            return true;
        }
    },
    UpdateItem: function(Item) {
        if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
            Items.update({
                _id: Item._id
            }, {
                "Name": Item.Name,
                "SellingPrice": Item.SellingPrice,
                "Description": Item.Description,
                "Main_Image": Item.Main_Image,
                "Alternate_Image_1": Item.Alternate_Image_1,
                "Alternate_Image_2": Item.Alternate_Image_2,
                "Alternate_Image_3": Item.Alternate_Image_3,
                "Alternate_Image_4": Item.Alternate_Image_4
            });
        }
    },
    AcceptFacebookQuest: function(UserId, FriendCount) {
        try {
            if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
                var tokenQuestAmount = (Math.floor(parseInt(FriendCount) / 100) * 100) / 100;
                var newActiveDate = new Date();
                newActiveDate.setDate(newActiveDate.getDate() + 7);
                Meteor.users.update({
                    _id: UserId,
                    "Quests.FacebookQuest.Active": true,
                    "Quests.FacebookQuest.Submitted": true
                }, {
                    $set: {
                        "Quests.FacebookQuest.Active": false,
                        "Quests.FacebookQuest.Submitted": false,
                        "Quests.FacebookQuest.PreviousTokenReward": tokenQuestAmount,
                        "Quests.FacebookQuest.ActiveRenewalDate": newActiveDate
                    },
                    $inc: {
                        "profile.Tokens": tokenQuestAmount
                    }
                }, function(error, result) {
                    if (result) {
                        var job = new Job(QuestsData, 'RenewFacebookQuest', {
                            UserId: UserId
                        }).delay(newActiveDate.getTime()).save();
                    }
                })
            }
        } catch (e) {
            console.log(e);
        }

    },
    RejectFacebookQuest: function(UserId) {
        try {
            if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
                Meteor.users.update({
                    _id: UserId,
                    "Quests.FacebookQuest.Active": true,
                    "Quests.FacebookQuest.Submitted": true
                }, {
                    $set: {
                        "Quests.FacebookQuest.Active": true,
                        "Quests.FacebookQuest.Submitted": true,
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    },
    AdminCreateCompetitionSyncedCron: function(Competition) {
        try {
            ItemCompetitionSchema.clean(Competition);
            if (Roles.userIsInRole(Meteor.userId(), 'Worker')) {
                SyncedCron.add({
                    name: 'ItemCompetition Start ' + Competition._id,
                    schedule: function(parser) {
                        return parser.recur().on(Competition.StartTimeOfCompetition).fullDate();
                    },
                    job: function() {
                        console.log("Competition Starts " + new Date());
                        ItemCompetitions.update({
                            _id: Competition._id
                        }, {
                            $set: {
                                IsLive: true
                            }
                        });
                        SyncedCron.remove('ItemCompetition Start ' + Competition._id);
                        SyncedCron.add({
                            name: 'ItemCompetition End ' + Competition._id,
                            schedule: function(parser) {
                                return parser.recur().on(Competition.EndTimeOfCompetition).fullDate();
                            },
                            job: function() {
                                console.log("Competition Ends " + new Date());
                                ItemCompetitions.update({
                                    _id: Competition._id
                                }, {
                                    $set: {
                                        IsLive: false,
                                        HasBeenWon: true
                                    }
                                });
                                var completedItemCompetition = ItemCompetitions.findOne({
                                    _id: Competition._id
                                });
                                var resultArray = [];
                                var lowerBound = -99;
                                var upperBound = 0;
                                for (var i = 0; i < 120; i++) {
                                    lowerBound += 100;
                                    upperBound += 100;
                                    Array.prototype.push.apply(resultArray, completedItemCompetition[lowerBound + "-" + upperBound]);
                                }
                                var competitionPotAmount = Math.ceil(completedItemCompetition.Item.SellingPrice * completedItemCompetition.Progress);
                                console.log(competitionPotAmount);
                                var winners = [];
                                var competitionWinningExpirationDate = new Date();
                                competitionWinningExpirationDate.setDate(competitionWinningExpirationDate.getDate() + 3);
                                if (competitionPotAmount > 0) {
                                    if (resultArray[completedItemCompetition.SlotCount - 1].User != null) {

                                        if (competitionPotAmount >= completedItemCompetition.Item.SellingPrice) {
                                            winners.push({
                                                UserId: resultArray[completedItemCompetition.SlotCount - 1].User,
                                                Discount: completedItemCompetition.Item.SellingPrice,
                                                Slot: resultArray[completedItemCompetition.SlotCount - 1].Slot
                                            });
                                            competitionPotAmount -= completedItemCompetition.Item.SellingPrice;
                                        } else {
                                            winners.push({
                                                UserId: resultArray[completedItemCompetition.SlotCount - 1].User,
                                                Discount: competitionPotAmount,
                                                Slot: resultArray[completedItemCompetition.SlotCount - 1].Slot
                                            });
                                            competitionPotAmount = 0;
                                        }
                                    }
                                    var slotCountExtenderPositive = completedItemCompetition.SlotCount - 1;
                                    var slotCountExtenderNegative = completedItemCompetition.SlotCount - 1;
                                    while (competitionPotAmount > 0) {
                                        if (slotCountExtenderPositive < resultArray.length - 1) {
                                            slotCountExtenderPositive += 1;
                                        } else {
                                            if (slotCountExtenderNegative == 0) {
                                                break;
                                            } else {
                                                while (competitionPotAmount > 0) {
                                                    console.log("negative");
                                                    if (slotCountExtenderNegative > 0) {
                                                        slotCountExtenderNegative -= 1;
                                                    } else {
                                                        break;
                                                    }
                                                    if (resultArray[slotCountExtenderNegative].User != null) {
                                                        if (competitionPotAmount >= completedItemCompetition.Item.SellingPrice) {
                                                            winners.push({
                                                                UserId: resultArray[slotCountExtenderNegative].User,
                                                                Discount: completedItemCompetition.Item.SellingPrice,
                                                                Slot: resultArray[slotCountExtenderNegative].Slot
                                                            });
                                                            competitionPotAmount -= completedItemCompetition.Item.SellingPrice;
                                                        } else {
                                                            winners.push({
                                                                UserId: resultArray[slotCountExtenderNegative].User,
                                                                Discount: competitionPotAmount,
                                                                Slot: resultArray[slotCountExtenderNegative].Slot
                                                            });
                                                            competitionPotAmount = 0;
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        if (slotCountExtenderNegative > 0) {
                                            slotCountExtenderNegative -= 1;
                                        } else {
                                            if (slotCountExtenderPositive == resultArray.length - 1) {
                                                break;
                                            } else {
                                                while (competitionPotAmount > 0) {

                                                    if (slotCountExtenderPositive < resultArray.length - 1) {
                                                        slotCountExtenderPositive += 1;
                                                    } else {
                                                        break;
                                                    }
                                                    if (resultArray[slotCountExtenderPositive].User != null) {
                                                        if (competitionPotAmount >= completedItemCompetition.Item.SellingPrice) {
                                                            winners.push({
                                                                UserId: resultArray[slotCountExtenderPositive].User,
                                                                Discount: Competition.Item.SellingPrice,
                                                                Slot: resultArray[slotCountExtenderPositive].Slot
                                                            });
                                                            competitionPotAmount -= completedItemCompetition.Item.SellingPrice;
                                                        } else {
                                                            winners.push({
                                                                UserId: resultArray[slotCountExtenderPositive].User,
                                                                Discount: competitionPotAmount,
                                                                Slot: resultArray[slotCountExtenderPositive].Slot
                                                            });
                                                            competitionPotAmount = 0;
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        console.log(competitionPotAmount);

                                        if (resultArray[slotCountExtenderPositive].User != null && resultArray[slotCountExtenderNegative].User != null) {
                                            if (competitionPotAmount >= 2 * Competition.Item.SellingPrice) {
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderPositive].User,
                                                    Discount: Competition.Item.SellingPrice,
                                                    Slot: resultArray[slotCountExtenderPositive].Slot
                                                });
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderNegative].User,
                                                    Discount: Competition.Item.SellingPrice,
                                                    Slot: resultArray[slotCountExtenderNegative].Slot
                                                });
                                                competitionPotAmount -= 2 * Competition.Item.SellingPrice;
                                            } else {
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderPositive].User,
                                                    Discount: Math.floor(competitionPotAmount / 2),
                                                    Slot: resultArray[slotCountExtenderPositive].Slot
                                                });
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderNegative].User,
                                                    Discount: Math.floor(competitionPotAmount / 2),
                                                    Slot: resultArray[slotCountExtenderNegative].Slot
                                                });
                                                competitionPotAmount = 0;
                                            }
                                        } else if (resultArray[slotCountExtenderPositive].User != null) {
                                            if (competitionPotAmount >= completedItemCompetition.Item.SellingPrice) {
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderPositive].User,
                                                    Discount: Competition.Item.SellingPrice,
                                                    Slot: resultArray[slotCountExtenderPositive].Slot
                                                });
                                                competitionPotAmount -= completedItemCompetition.Item.SellingPrice;
                                            } else {
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderPositive].User,
                                                    Discount: competitionPotAmount,
                                                    Slot: resultArray[slotCountExtenderPositive].Slot
                                                });
                                                competitionPotAmount = 0;
                                            }
                                        } else if (resultArray[slotCountExtenderNegative].User != null) {
                                            if (competitionPotAmount >= completedItemCompetition.Item.SellingPrice) {
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderNegative].User,
                                                    Discount: Competition.Item.SellingPrice,
                                                    Slot: resultArray[slotCountExtenderNegative].Slot
                                                });
                                                competitionPotAmount -= completedItemCompetition.Item.SellingPrice;
                                            } else {
                                                winners.push({
                                                    UserId: resultArray[slotCountExtenderNegative].User,
                                                    Discount: competitionPotAmount,
                                                    Slot: resultArray[slotCountExtenderNegative].Slot
                                                });
                                                competitionPotAmount = 0;
                                            }
                                        }
                                    }

                                    console.log(winners);
                                    for (var i = 0; i < winners.length; i++) {
                                        var competitionWinningsObject = {};
                                        competitionWinningsObject["_id"] = new Meteor.Collection.ObjectID().valueOf();
                                        competitionWinningsObject["Item"] = completedItemCompetition.Item;
                                        competitionWinningsObject["Quantity"] = 1;
                                        competitionWinningsObject["WinningCompetitionId"] = completedItemCompetition._id;
                                        competitionWinningsObject["WonFromCompetition"] = true;
                                        competitionWinningsObject["WonFromCompetitionExpirationDate"] = competitionWinningExpirationDate;
                                        competitionWinningsObject["Discount"] = winners[i].Discount;
                                        competitionWinningsObject["CartOrReserve"] = true;

                                        Meteor.users.update({
                                            _id: winners[i].UserId
                                        }, {
                                            $push: {
                                                "Cart": competitionWinningsObject
                                            },
                                            $inc: {
                                                "CompetitionInformation.CompetitionWins": 1,
                                                "CompetitionInformation.CompetitionWinningsAmount": winners[i].Discount
                                            }
                                        });
                                    }
                                }
                                SyncedCron.remove('ItemCompetition End ' + Competition._id);
                                SyncedCron.add({
                                    name: 'ItemCompetition Winners Announced' + Competition._id,
                                    schedule: function(parser) {
                                        var runCompletedItemCompetitionDate = new Date();
                                        runCompletedItemCompetitionDate.setSeconds(runCompletedItemCompetitionDate.getSeconds() + 10);
                                        return parser.recur().on(runCompletedItemCompetitionDate).fullDate();
                                    },
                                    job: function() {
                                        ItemCompetitions.update({
                                            _id: completedItemCompetition._id
                                        }, {
                                            $set: {
                                                "WinnersCalculated": true,
                                                "Winners": winners,
                                                "NumberOfWinners": Math.ceil(completedItemCompetition.Progress / completedItemCompetition.Item.SellingPrice)
                                            }
                                        });
                                        var removeItemFromCompetitionTimerCountdownJob = new Job(ItemCompetitionData, 'RemoveItemFromCompetitionTimerCountdown', {
                                            CompetitionId: Competition._id
                                        }).delay(competitionWinningExpirationDate.getTime()).save();
                                        var removeItemCompetitionFromWatchlistAndCollectionJob = new Job(ItemCompetitionData, 'RemoveItemCompetitionFromWatchlistCompetitionSlotsCollection', {
                                            ItemCompetitionId: completedItemCompetition._id,
                                            Winners: winners
                                        }).delay(10000).save();

                                    }
                                });
                            }
                        });
                    }
                });
            }
        } catch (e) {
            console.log(e);
        }
    }
});
