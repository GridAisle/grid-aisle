Meteor.startup(function() {
    Accounts.urls.resetPassword = function(token) {
            return Meteor.absoluteUrl('Reset-Password/' + token);
        },
        Accounts.urls.verifyEmail = function(token) {
            return Meteor.absoluteUrl('Verify-Email/' + token);
        }
});
var postSignUp = function(userId, info) {
    Roles.addUsersToRoles(userId, "User");
}
AccountsTemplates.configure({
    postSignUpHook: postSignUp,
    sendVerificationEmail: true,
    enablePasswordChange: true,
    enforceEmailVerification: true,
    showResendVerificationEmailLink: true
});
AccountsTemplates.configure({
    texts: {
        info: {
            signUpVerifyEmail: "Successful Registration! Please check your email and follow the instructions.",
            verificationEmailSent: "A new email has been sent to you. If the email doesn't show up in your inbox, be sure to check your spam folder.",
        },
        errors: {
            verifyEmailFirst: "A verification email has been sent. Check the email and follow the link! If the email doesn't show up in your inbox, be sure to check your spam folder.",
        }
    }
});
Accounts.onCreateUser(function(options, user) {
    user.profile = {
        FirstName: options.profile.FirstName,
        LastName: options.profile.LastName
    }
    if (options.profile.Reg_Code) {
        var referrer = Meteor.users.findOne({
            _id: options.profile.Reg_Code
        }, {
            fields: {
                _id: 1
            }
        });
        if (referrer) {
            var Referral = {};
            Referral.Referrer = options.profile.Reg_Code;
            user.Referral = Referral;
        }
    }
    return user
});
Accounts.emailTemplates.siteName = "Spargain";
Accounts.emailTemplates.from = "Spargain <support@spargain.com>";

Accounts.emailTemplates.verifyEmail = {
    subject() {
        return "Spargain Account Verification Request";
    },
    text(user, url) {
        let emailAddress = user.emails[0].address,
            urlWithoutHash = url.replace('#/', ''),
            userFirstName = user.profile.FirstName
        emailBody = `Hello ${userFirstName},\n\nThank you for creating an account at Spargain. To verify your account please visit the following:\n\n${urlWithoutHash}\n\nThank you,\n\nSpargain`;

        return emailBody;
    }
};
