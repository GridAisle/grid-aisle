Items = new Mongo.Collection('Items');
ItemSchema = new SimpleSchema({
    _id: {
        type: String
    },
    Name: {
        type: String
    },
    SellingPrice: {
        type: Number
    },
    Description: {
        type: String
    },
    Available:{
      type:Boolean,
      defaultValue:true
    },
    Main_Image: {
        type: String
    },
    Alternate_Image_1: {
        type: String,
        optional:true
    },
    Alternate_Image_2: {
        type: String,
        optional:true
    },
    Alternate_Image_3: {
        type: String,
        optional:true
    }
});
Items.attachSchema(ItemSchema);
