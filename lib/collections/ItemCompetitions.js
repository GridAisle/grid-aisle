ItemCompetitionExampleData = new Mongo.Collection('ItemCompetitionExampleData');

WinnersSchema = new SimpleSchema({
    UserId: {
        type: String
    },
    Discount: {
        type: Number
    },
    Slot: {
        type: Number
    }
});
SlotSchema = new SimpleSchema({
    Slot: {
        type: Number,
        optional: true
    },
    User: {
        type: String,
        optional: true,
        defaultValue: null
    }
});
ItemCompetitions = new Mongo.Collection('ItemCompetitions');

serverMessages = new ServerMessages();

ItemCompetitionSchema = new SimpleSchema({
    _id: {
        type: String,
        optional: true,
        autoform: {
            omit: true
        }
    },
    Item: {
        type: ItemSchema,
        autoform: {
            type: "select",
            options: function() {
                return Items.find().map(function(c) {
                    return {
                        label: c.Name,
                        value: c._id
                    }
                });
            }
        }
    },
    IsLive: {
        type: Boolean,
        defaultValue: false
    },
    HasBeenWon: {
        type: Boolean,
        defaultValue: false
    },
    CreationComplete: {
        type: Boolean,
    },
    StartTimeOfCompetition: {
        type: Date
    },
    EndTimeOfCompetition: {
        type: Date
    },
    SlotCount: {
        type: Number,
        defaultValue: 0
    },
    SlotPrice: {
        type: Number,
        defaultValue: 0
    },
    Progress: {
        type: Number,
        defaultValue: 0,
        decimal: true
    },
    EntrantCount: {
        type: Number,
        defaultValue: 0
    },
    Cancelled: {
        type: Boolean,
        optional: true
    },
    WinnersCalculated: {
        type: Boolean,
        defaultValue: false
    },
    Winners: {
        type: [WinnersSchema],
        optional: true
    },
    NumberOfWinners: {
        type: Number,
        optional: true
    },
    "1-100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1; i <= 100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // },
        blackbox: true
    },
    "1-100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "101-200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 101; i <= 200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "101-200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "201-300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 201; i <= 300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "201-300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "301-400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 301; i <= 400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "301-400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "401-500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 401; i <= 500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "401-500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "501-600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 501; i <= 600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "501-600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "601-700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 601; i <= 700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "601-700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "701-800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 701; i <= 800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "701-800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "801-900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 801; i <= 900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "801-900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "901-1000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 901; i <= 1000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "901-1000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1001-1100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1001; i <= 1100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1001-1100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1101-1200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1101; i <= 1200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1101-1200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1201-1300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1201; i <= 1300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1201-1300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1301-1400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1301; i <= 1400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1301-1400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1401-1500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1401; i <= 1500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1401-1500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1501-1600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1501; i <= 1600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1501-1600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1601-1700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1601; i <= 1700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1601-1700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1701-1800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1701; i <= 1800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1701-1800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1801-1900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1801; i <= 1900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1801-1900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "1901-2000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 1901; i <= 2000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "1901-2000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2001-2100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2001; i <= 2100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2001-2100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2101-2200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2101; i <= 2200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2101-2200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2201-2300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2201; i <= 2300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2201-2300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2301-2400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2301; i <= 2400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2301-2400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2401-2500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2401; i <= 2500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2401-2500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2501-2600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2501; i <= 2600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2501-2600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2601-2700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2601; i <= 2700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2601-2700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2701-2800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2701; i <= 2800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2701-2800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2801-2900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2801; i <= 2900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2801-2900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "2901-3000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 2901; i <= 3000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "2901-3000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3001-3100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3001; i <= 3100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3001-3100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3101-3200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3101; i <= 3200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3101-3200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3201-3300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3201; i <= 3300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3201-3300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3301-3400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3301; i <= 3400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3301-3400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3401-3500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3401; i <= 3500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3401-3500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3501-3600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3501; i <= 3600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3501-3600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3601-3700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3601; i <= 3700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3601-3700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3701-3800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3701; i <= 3800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3701-3800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3801-3900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3801; i <= 3900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3801-3900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "3901-4000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 3901; i <= 4000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "3901-4000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4001-4100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4001; i <= 4100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4001-4100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4101-4200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4101; i <= 4200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4101-4200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4201-4300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4201; i <= 4300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4201-4300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4301-4400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4301; i <= 4400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4301-4400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4401-4500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4401; i <= 4500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4401-4500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4501-4600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4501; i <= 4600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4501-4600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4601-4700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4601; i <= 4700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4601-4700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4701-4800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4701; i <= 4800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4701-4800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4801-4900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4801; i <= 4900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4801-4900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "4901-5000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 4901; i <= 5000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "4901-5000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5001-5100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5001; i <= 5100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5001-5100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5101-5200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5101; i <= 5200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5101-5200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5201-5300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5201; i <= 5300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5201-5300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5301-5400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5301; i <= 5400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5301-5400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5401-5500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5401; i <= 5500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5401-5500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5501-5600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5501; i <= 5600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5501-5600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5601-5700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5601; i <= 5700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5601-5700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5701-5800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5701; i <= 5800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5701-5800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5801-5900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5801; i <= 5900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5801-5900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "5901-6000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 5901; i <= 6000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "5901-6000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6001-6100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6001; i <= 6100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6001-6100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6101-6200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6101; i <= 6200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6101-6200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6201-6300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6201; i <= 6300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6201-6300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6301-6400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6301; i <= 6400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6301-6400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6401-6500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6401; i <= 6500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6401-6500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6501-6600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6501; i <= 6600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6501-6600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6601-6700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6601; i <= 6700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6601-6700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6701-6800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6701; i <= 6800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6701-6800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6801-6900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6801; i <= 6900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6801-6900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "6901-7000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 6901; i <= 7000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "6901-7000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7001-7100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7001; i <= 7100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7001-7100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7101-7200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7101; i <= 7200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7101-7200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7201-7300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7201; i <= 7300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7201-7300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7301-7400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7301; i <= 7400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7301-7400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7401-7500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7401; i <= 7500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7401-7500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7501-7600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7501; i <= 7600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7501-7600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7601-7700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7601; i <= 7700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7601-7700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7701-7800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7701; i <= 7800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7701-7800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7801-7900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7801; i <= 7900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7801-7900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "7901-8000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 7901; i <= 8000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "7901-8000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8001-8100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8001; i <= 8100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8001-8100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8101-8200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8101; i <= 8200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8101-8200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8201-8300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8201; i <= 8300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8201-8300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8301-8400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8301; i <= 8400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8301-8400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8401-8500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8401; i <= 8500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8401-8500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8501-8600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8501; i <= 8600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8501-8600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8601-8700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8601; i <= 8700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8601-8700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8701-8800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8701; i <= 8800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8701-8800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8801-8900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8801; i <= 8900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8801-8900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "8901-9000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 8901; i <= 9000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "8901-9000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9001-9100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9001; i <= 9100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9001-9100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9101-9200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9101; i <= 9200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9101-9200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9201-9300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9201; i <= 9300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9201-9300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9301-9400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9301; i <= 9400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9301-9400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9401-9500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9401; i <= 9500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9401-9500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9501-9600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9501; i <= 9600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9501-9600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9601-9700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9601; i <= 9700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9601-9700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9701-9800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9701; i <= 9800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9701-9800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9801-9900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9801; i <= 9900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9801-9900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "9901-10000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 9901; i <= 10000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "9901-10000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10001-10100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10001; i <= 10100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10001-10100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10101-10200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10101; i <= 10200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10101-10200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10201-10300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10201; i <= 10300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10201-10300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10301-10400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10301; i <= 10400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10301-10400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10401-10500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10401; i <= 10500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10401-10500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10501-10600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10501; i <= 10600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10501-10600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10601-10700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10601; i <= 10700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10601-10700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10701-10800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10701; i <= 10800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10701-10800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10801-10900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10801; i <= 10900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10801-10900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "10901-11000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 10901; i <= 11000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "10901-11000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11001-11100": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11001; i <= 11100; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11001-11100.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11101-11200": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11101; i <= 11200; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11101-11200.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11201-11300": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11201; i <= 11300; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11201-11300.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11301-11400": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11301; i <= 11400; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11301-11400.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11401-11500": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11401; i <= 11500; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11401-11500.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11501-11600": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11501; i <= 11600; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11501-11600.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11601-11700": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11601; i <= 11700; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11601-11700.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11701-11800": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11701; i <= 11800; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11701-11800.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11801-11900": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11801; i <= 11900; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11801-11900.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
    "11901-12000": {
        type: [SlotSchema],
        optional: true,
        blackbox: true,
        // autoValue: function() {
        //     if (this.isInsert) {
        //         var array = [];
        //         for (var i = 11901; i <= 12000; i++) {
        //             array.push({
        //                 "Slot": i,
        //                 "User": null,
        //                 "Date": null
        //             });
        //         }
        //         return array;
        //     }
        // }
    },
    "11901-12000.$": {
        type: Object,
        optional: true,
        blackbox: true
    },
});


ItemCompetitions.attachSchema(ItemCompetitionSchema);

CompletedItemCompetitions = new Mongo.Collection('CompletedItemCompetitions');

CompletedItemCompetitionSchema = new SimpleSchema({
    Winners: {
        type: [WinnersSchema]
    },
    CompletionDate: {
        type: Date
    },
    ItemCompetition: {
        type: ItemCompetitionSchema
    }

});
CompletedItemCompetitions.attachSchema(CompletedItemCompetitionSchema);

CancelledItemCompetitions = new Mongo.Collection('CancelledItemCompetitions');


CancelledItemCompetitionSchema = new SimpleSchema({
    _id: {
        type: String
    },
    Competition: {
        type: ItemCompetitionSchema
    },
    CancelledDate: {
        type: Date
    },

});
CancelledItemCompetitions.attachSchema(CancelledItemCompetitionSchema);
