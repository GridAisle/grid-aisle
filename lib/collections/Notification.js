Notification = new Mongo.Collection('Notification');
NotificationSchema = new SimpleSchema({
    Text: {
        type: String,
        max:50
    },
    Type: {
        type: String,
        allowedValues: ['Information', 'Warning', 'Addition'],
        autoform: {
            options: [{
                label: "Information",
                value: "Information"
            }, {
                label: "Warning",
                value: "Warning"
            }, {
                label: "Addition",
                value: "Addition"
            }]
        }
    },
    Url: {
        type: String,
        optional: true,
        max:50
    }
});
Notification.attachSchema(NotificationSchema);
