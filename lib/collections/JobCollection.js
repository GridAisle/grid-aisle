if (Meteor.isServer) {
    ItemCompetitionData = JobCollection('ItemCompetitionData');
    ItemCompetitionWinnersExpiration = JobCollection('ItemCompetitionWinnersExpiration');
    ItemCompetitionUserDataClean = JobCollection('ItemCompetitionUserDataClean');
    ItemData = JobCollection('ItemData');
    OrderData = JobCollection('OrderData');
    QuestsData = JobCollection('QuestsData');
    ItemCompetitionData.allow({
        // Grant full permission to any authenticated user
        admin: function(userId, method, params) {
            return (userId ? true : false);
        }
    });
    QuestsData.allow({
        // Grant full permission to any authenticated user
        admin: function(userId, method, params) {
            return (userId ? true : false);
        }
    });
    ItemCompetitionWinnersExpiration.allow({
        // Grant full permission to any authenticated user
        admin: function(userId, method, params) {
            return (userId ? true : false);
        }
    });
    ItemCompetitionUserDataClean.allow({
        // Grant full permission to any authenticated user
        admin: function(userId, method, params) {
            return (userId ? true : false);
        }
    });
    ItemData.allow({
        // Grant full permission to any authenticated user
        admin: function(userId, method, params) {
            return (userId ? true : false);
        }
    });
    OrderData.allow({
        // Grant full permission to any authenticated user
        admin: function(userId, method, params) {
            return (userId ? true : false);
        }
    });

    Meteor.startup(function() {
        ItemCompetitionData.startJobServer();
        ItemCompetitionWinnersExpiration.startJobServer();
        ItemCompetitionUserDataClean.startJobServer();
        ItemData.startJobServer();
        OrderData.startJobServer();
        QuestsData.startJobServer();

    });
}
