CartItemSchema = new SimpleSchema({
    _id: {
        type: String,
        autoform: {
            omit: true
        },
        defaultValue: new Meteor.Collection.ObjectID().valueOf()
    },
    Item: {
        type: ItemSchema
    },
    Quantity: {
        type: Number,
        max: 10
    },
    WinningCompetitionId: {
        type: String,
        optional: true
    },
    WonFromCompetition: {
        type: Boolean,
        defaultValue: false
    },
    WonFromCompetitionExpirationDate: {
        type: Date,
        optional: true
    },
    Discount: {
        type: Number,
        defaultValue: 0,
        min: 0
    },
    CartOrReserve: {
        type: Boolean,
        defaultValue: true
    }
});
ReplacementRefundSchema = new SimpleSchema({
    _id: {
        type: String,
        autoform: {
            omit: true
        },
        defaultValue: new Meteor.Collection.ObjectID().valueOf()
    },
    RequestDate: {
        type: Date,
        defaultValue: new Date()
    },
    OrderId: {
        type: String,
        autoform: {
            type: "hidden"
        }
    },
    ItemId: {
        type: String,
        autoform: {
            type: "hidden"
        }
    },
    ItemQuantity: {
        type: Number,
        autoform: {
            type: "hidden"
        }
    },
    Quantity: {
        type: Number,
        min: 1
    },
    Price: {
        type: Number,
        autoform: {
            type: "hidden"
        }
    },
    RefundOrReplace: {
        type: Boolean,
        autoform: {
            type: "boolean-radios",
            trueLabel: "Refund",
            falseLabel: "Replace"
        }
    },
    InformationEmailSent:{
      type:Boolean,
      defaultValue:false
    },
    HasBeenRefundedReplaced: {
        type: Boolean,
        defaultValue: false
    },
    Reason: {
        type: String,
        allowedValues: ['Product damaged but shipping box is ok', 'Missing parts or accessories', 'Product and shipping box both damaged', "Item defective / doesn't work"],
        autoform: {
            options: [{
                label: "Product damaged but shipping box is ok",
                value: "Product damaged but shipping box is ok"
            }, {
                label: "Missing parts or accessories",
                value: "Missing parts or accessories"
            }, {
                label: "Product and shipping box both damaged",
                value: "Product and shipping box both damaged"
            }, {
                label: "Item defective / doesn't work",
                value: "Item defective / doesn't work"
            }],
            label: false
        }
    },
    Comments: {
        type: String,
        label: "COMMENTS (REQUIRED) (256 CHARACTERS)",
        max: 256
    },
    CompletionDate: {
        type: Date,
        optional: true
    }
});

TwitterQuestSchema = new SimpleSchema({
    Active: {
        type: Boolean,
        defaultValue: true
    },
    ActiveRenewalDate: {
        type: Date,
        optional: true
    }
});
FacebookQuestSchema = new SimpleSchema({
    Active: {
        type: Boolean,
        defaultValue: true
    },
    Submitted:{
      type:Boolean,
      defaultValue:false
    },
    Username:{
      type:String,
      optional:true
    },
    ActiveRenewalDate: {
        type: Date,
        optional: true
    },
    PreviousTokenReward:{
      type:Number,
      optional:true
    }
});
QuestsSchema = new SimpleSchema({
    TwitterQuest: {
        type: TwitterQuestSchema,
    },
    FacebookQuest: {
        type: FacebookQuestSchema
    }
});

CompetitionWinningsSchema = new SimpleSchema({
    CompetitionId: {
        type: String
    },
    WinPercentage: {
        type: Number,
        decimal: true
    },
    Item: {
        type: ItemSchema
    }
});
AddressSchema = new SimpleSchema({
    _id: {
        type: String,
        autoform: {
            omit: true
        }
    },
    FirstName: {
        type: String,
        label: "First Name"
    },
    LastName: {
        type: String,
        label: "Last Name"
    },
    Address: {
        type: String,
        label: "Address"
    },
    ApartmentFloorUnit: {
        type: String,
        optional: true,
        label: "Apartment / Floor Unit (Optional)"
    },
    City: {
        type: String,
        label: "City"
    },
    State: {
        type: String,
        label: "State"
    },
    Zipcode: {
        type: Number,
        label: "Zipcode",
        regEx:SimpleSchema.RegEx.ZipCode,
        min:5
    },
    PhoneNumber: {
        type: String,
        label: "Phone Number",
        regEx:SimpleSchema.RegEx.Phone
    },
    IsDefault: {
        type: Boolean,
        optional: true,
        defaultValue: false
    }

});
OrderSchema = new SimpleSchema({
    _id: {
        type: String
    },
    OrderId: {
        type: String
    },
    HasBeenCompleted: {
        type: Boolean
    },
    ShippingStatus: {
        type: String
    },
    Cancelable: {
        type: Boolean
    },
    ShippingInformation: {
        type: AddressSchema
    },
    OrderPlacedDate: {
        type: Date
    },
    TrackingUrl:{
      type:String,
      optional:true
    },
    Item: {
        type: ItemSchema
    },
    Quantity: {
        type: Number,
        max: 10
    },
    WinningCompetitionId: {
        type: String,
        optional: true
    },
    WonFromCompetition: {
        type: Boolean,
        defaultValue: false
    },
    Discount: {
        type: Number,
        defaultValue: 0,
        min: 0
    },
});
CompetitionSettingsSchema = new SimpleSchema({
    FastSlots: {
        type: Boolean,
        defaultValue: false
    },
});
CompetitionSlotsSchema = new SimpleSchema({
    CompetitionId: {
        type: String
    },
    Slot: {
        type: Number
    },
    Price: {
        type: Number
    }
});
ComeptitionInformationSchema = new SimpleSchema({
    CompetitionWins: {
        type: Number,
        defaultValue: 0
    },
    CompetitionWinningsAmount: {
        type: Number,
        decimal: true,
        defaultValue: 0
    }
});
ResponseSchema = new SimpleSchema({
    Creator: {
        type: String,
        defaultValue: "Spargain Support"
    },
    createdAt: {
        type: Date,
    },
    Response: {
        type: String
    }
});
TicketSchema = new SimpleSchema({
    Ticket: {
        type: String
    },
    createdAt: {
        type: Date,
    },
    Status: {
        type: Boolean
    },
    Creator: {
        type: String
    },
    IsUser: {
        type: Boolean
    }
});
UserProfileSchema = new SimpleSchema({
    FirstName: {
        type: String,
        defaultValue: null,
        optional: true
    },
    LastName: {
        type: String,
        defaultValue: null,
        optional: true
    },
    Tokens: {
        type: Number,
        defaultValue: 0,
        min: 0
    },
});
ReferralSchema = new SimpleSchema({
  Referrer:{
    type:String,
    optional:true
  }
})
UserSchema = new SimpleSchema({
    profile: {
        type: UserProfileSchema,
        defaultValue: null,
        optional: true
    },
    Customer: {
        type: String,
        defaultValue: null,
        optional: true
    },
    Watchlist: {
        type: [String],
        defaultValue: []
    },
    CompetitionsEntered:{
      type:[String],
      defaultValue:[]
    },
    Cart: {
        type: [CartItemSchema],
        defaultValue: []
    },
    Tickets: {
        type: [TicketSchema],
        defaultValue: []
    },
    Addresses: {
        type: [AddressSchema],
        defaultValue: []
    },
    Orders: {
        type: [OrderSchema],
        defaultValue: []
    },
    ReplacementsRefunds: {
        type: [ReplacementRefundSchema],
        defaultValue: []
    },
    Quests: {
        type: QuestsSchema
    },
    CompetitionSlots: {
        type: [CompetitionSlotsSchema],
        defaultValue: []
    },
    CompetitionInformation: {
        type: ComeptitionInformationSchema
    },
    CompetitionSettings: {
        type: CompetitionSettingsSchema
    },
    Referral:{
      type:ReferralSchema,
      optional:true
    },
    emails: {
        type: Array,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date,
        autoValue: function() {
            return new Date()
        }
    },
    // Make sure this services field is in your schema if you're using any of the accounts packages
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    // Option 2: [String] type
    // If you are sure you will never need to use role groups, then
    // you can specify [String] as the type
    roles: {
        type: [String],
        optional: true
    },
    heartbeat: {
        type: Date,
        optional: true
    },
    status: {
        type: Object,
        optional: true,
        blackbox: true
    }
});

Meteor.users.attachSchema(UserSchema);
Meteor.users.deny({
    update: function() {
        return true;
    }
});
