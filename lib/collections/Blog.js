Blog = new Mongo.Collection('Blog');
BlogSchema = new SimpleSchema({
    _id: {
        type: String
    },
    Author:{
      type:String
    },
    Content: {
        type: String
    },
    Title:{
      type:String
    },
    Date:{
      type:Date
    },
    Main_Image:{
      type:String,
      optional:true

    }
});
Blog.attachSchema(BlogSchema);
