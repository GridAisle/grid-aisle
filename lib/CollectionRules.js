Meteor.users.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
ItemCompetitions.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
CancelledItemCompetitions.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
CompletedItemCompetitions.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
Blog.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
Notification.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
Items.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
ItemCompetitionExampleData.deny({
    update: function() {
        return true;
    },
    insert: function() {
        return true;
    },
    remove: function() {
        return true;
    },
});
