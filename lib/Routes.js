  FlowRouter.notFound = {
      action: function() {
          FlowRouter.go("/");
      }
  }

  function isLoggedIn(context, redirect) {
      if (Meteor.userId() || Meteor.loggingIn()) {
          route = context
      } else {
          redirect("/Register");
      }
  }
  FlowRouter.route("/", {
      name: "Main",
      action: function() {
          BlazeLayout.render("Main", {
              content: 'ItemCompetitionsPage',
              ItemCompetitionListOrWatchlist: 'ItemCompetitionsList'
          });
      }
  });
  FlowRouter.route("/Page/:Number", {
      name: "Main",
      action: function() {
          BlazeLayout.render("Main", {
              content: 'ItemCompetitionsPage',
              ItemCompetitionListOrWatchlist: 'ItemCompetitionsList'
          });
      }
  });
  FlowRouter.route("/Watchlist/Page/:Number", {
      name: "Watchlist",
      action: function() {
          BlazeLayout.render("Main", {
              content: 'ItemCompetitionsPage',
              ItemCompetitionListOrWatchlist: 'ItemCompetitionsListWatchList'
          });
      }
  });
  FlowRouter.route("/Watchlist", {
      name: "Watchlist",
      triggersEnter: [isLoggedIn],
      action: function() {
          BlazeLayout.render("Main", {
              content: 'ItemCompetitionsPage',
              ItemCompetitionListOrWatchlist: 'ItemCompetitionsListWatchList'
          });
      }
  });
  FlowRouter.route("/CompetitionsEntered/Page/:Number", {
      name: "CompetitionsEntered",
      action: function() {
          BlazeLayout.render("Main", {
              content: 'ItemCompetitionsPage',
              ItemCompetitionListOrWatchlist: 'ItemCompetitionsListEntered'
          });
      }
  });
  FlowRouter.route("/CompetitionsEntered", {
      name: "CompetitionsEntered",
      triggersEnter: [isLoggedIn],
      action: function() {
          BlazeLayout.render("Main", {
              content: 'ItemCompetitionsPage',
              ItemCompetitionListOrWatchlist: 'ItemCompetitionsListEntered'
          });
      }
  });
  FlowRouter.route("/HowSiteWorks", {
      name: "HowSiteWorks",
      action: function() {
          BlazeLayout.render("Main", {
              content: "HowSiteWorksFAQ",
              HowSiteWorksFAQTemplate: "HowSiteWorks"
          });
      }
  });
  FlowRouter.route("/FAQ", {
      name: "FAQ",
      action: function() {
          BlazeLayout.render("Main", {
              content: "HowSiteWorksFAQ",
              HowSiteWorksFAQTemplate: "FAQ"
          });
      }
  });
  FlowRouter.route("/Careers", {
      name: "Careers",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Careers"
          });
      }
  });
  FlowRouter.route("/Contact", {
      name: "Contact",
      triggersEnter: [isLoggedIn],
      action: function() {
          BlazeLayout.render("Main", {
              content: "Contact"
          });
      }
  });

  FlowRouter.route("/Blog", {
      name: "Blog",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Blog"
          });
      }
  });
  FlowRouter.route("/Terms", {
      name: "Terms",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Terms"
          });
      }
  });
  FlowRouter.route("/Privacy", {
      name: "Privacy",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Privacy"
          });
      }
  });
  FlowRouter.route("/Cart", {
      name: "Cart",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Cart"
          });
      }
  });
  FlowRouter.route("/Tokens", {
      name: "Tokens",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Tokens"
          });
      }
  });

  FlowRouter.route("/Admin", {
      name: "Admin",
      action: function() {
          if (Roles.userIsInRole(Meteor.userId(), 'Admin')) {
              BlazeLayout.render("Main", {
                  content: "Admin"
              });
          } else {
              FlowRouter.go("/");
          }
      }
  });
  var UserProfile = FlowRouter.group({
      prefix: "/UserProfile",
      name: "UserProfile",
      triggersEnter: [isLoggedIn]
  });
  UserProfile.route("/Profile", {
      name: "Profile",
      action: function() {
          BlazeLayout.render("Main", {
              content: "UserProfile",
              userprofile: "Profile"
          });
      }
  });
  UserProfile.route("/OrderHistory", {
      name: "OrderHistory",
      action: function() {
          BlazeLayout.render("Main", {
              content: "UserProfile",
              userprofile: "OrderHistory"
          });
      }
  });
  UserProfile.route("/Quests", {
      name: "Quests",
      action: function() {
          BlazeLayout.render("Main", {
              content: "UserProfile",
              userprofile: "Quests"
          });
      }
  });

  function CheckoutBreadcrumb(context, redirect) {
      if (Meteor.userId() || Meteor.loggingIn()) {
          Meteor.call("CheckoutLocation", function(error, result) {
              try {
                  if (result.hasOwnProperty("PaymentInformationCost")) {
                      Session.set("PaymentInformationCost", result.PaymentInformationCost);
                  }
                  if (context.oldRoute.group.name != ("PaymentInformation" || "ShippingInformation" || "Order")) {
                      Session.set("CheckoutBreadcrumb", result.CheckoutSession);
                  }
              } catch (e) {
                  Session.set("CheckoutBreadcrumb", result.CheckoutSession);
              }
              FlowRouter.redirect(result.Url);
          });
      } else {
          redirect("/");
      }
  }

  var Checkout = FlowRouter.group({
      prefix: "/Checkout",
      name: "Checkout",
      triggersEnter: [CheckoutBreadcrumb]
  });
  Checkout.route("/PaymentInformation", {
      name: "PaymentInformation",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Checkout",
              checkout: "PaymentInformation"
          });
      }
  });
  Checkout.route("/ShippingInformation", {
      name: "ShippingInformation",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Checkout",
              checkout: "ShippingInformation"
          });
      }
  });
  Checkout.route("/Order", {
      name: "Order",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Checkout",
              checkout: "Order"
          });
      }
  });
  FlowRouter.route("/ItemCompetition/:ItemCompetitionId/", {
      name: "ItemCompetition",
      action: function() {
          BlazeLayout.render("Main", {
              content: "ItemCompetition"
          });
      }
  });
  FlowRouter.route("/Item/:ItemId", {
      name: "Item",
      action: function() {
          BlazeLayout.render("Main", {
              content: "Item"
          });
      }
  });
  FlowRouter.route("/AddToCartResult", {
      name: "AddToCartResult",
      triggersEnter: [isLoggedIn],
      action: function() {
          BlazeLayout.render("Main", {
              content: "AddToCartResult"
          });
      }
  });
